<?php

/**
 * Register application modules
 */
$application->registerModules(array(
    'frontend' => array(
        'className' => 'DoGo\Frontend\Module',
        'path' => __DIR__ . '/../apps/frontend/Module.php'
    ),
    'backend' => array(
        'className' => 'DoGo\Backend\Module',
        'path' => __DIR__ . '/../apps/backend/Module.php'
    )
));
