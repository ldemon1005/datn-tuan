(function($) {
    "use strict";
    $(".add_to_cart").on('click',function () {
        var product_id = $(this).attr('product_id');
        var product_pricce = $(this).attr('product_price');
        var product_quantity = 1;
        $.ajax({
            url: '/shopping/add_cart',
            method: 'post',
            data: {
                product_id : product_id,
                product_price : product_pricce,
                product_quantity : product_quantity
            },
            success:function (result) {
                var res = JSON.parse(result);
                if(res.status == 1){
                    $("#snackbar").html(res.message);
                    $(".header-top__shopcart").html(res.cart_header);
                    $("#snackbar").addClass('show');
                    // After 3 seconds, remove the show class from DIV
                    setTimeout(function () {
                        $("#snackbar").removeClass('show');
                    }, 3000);
                }else {
                    $("#snackbar").css("background-color : red ");
                    $("#snackbar").html(res.message);
                    $("#snackbar").addClass('show');
                    // After 3 seconds, remove the show class from DIV
                    setTimeout(function () {
                        $("#snackbar").css('background-color : green');
                        $("#snackbar").removeClass('show');
                    }, 3000);
                }
            }
        });
    });

    $(".btn-plus").on('click',function () {
        var quantity_node = $(this).siblings('input');
        var quantity = quantity_node.val();
        quantity = parseInt(quantity) + 1;
        quantity_node.val(quantity);
    });

    $(".btn-minus").on('click',function () {
        var quantity_node = $(this).siblings('input');
        var quantity = quantity_node.val();
        quantity = parseInt(quantity) -1;
        if(quantity < 1) quantity = 1;
        quantity_node.val(quantity);
    });

    $(".delete_cart").on('click',function () {
        var id = $(this).attr('id_item');
        var node = $(this).closest(".view-cart__product");
        $.ajax({
            url : '/shopping/delete_cart',
            method : 'post',
            data : {id : id},
            success : function (res) {
                res = JSON.parse(res);
                if(res.status == 1){
                    node.hide();
                    $("#total_money").html(res.total_money);
                }
            }
        });
    });


    $("#vtp").on('click',function () {
        if($(this).is(':checked')){
            $(".vtp").show();
        }else $(".vtp").hide();
    });


    $(document).ready(function(){
        $(".calculate_vtp").on('click',function () {
            var province_id = $("#province_id").val();
            var district_id = $("#district_id").val();
            var store = $("#store").val();
            var service = $("#service").val();
            if(isNaN(province_id) || isNaN(district_id)){
                $("#snackbar").html("Thiếu thông tin địa chỉ");
                $("#snackbar").addClass('show');
                $("#snackbar").css("background-color : red");
                // After 3 seconds, remove the show class from DIV
                setTimeout(function () {
                    $("#snackbar").css('background-color : green');
                    $("#snackbar").removeClass('show');
                }, 3000);
            }
            if($("#vtp").is(':checked')){
                $.ajax({
                    url : '/shopping/calculate_fee_vtp' ,
                    method : 'post',
                    data : {
                        province_id : province_id,
                        district_id : district_id,
                        store : store,
                        service : service
                    },
                    success : function (res) {
                        res = JSON.parse(res);
                        if(res && res.status == 1){
                            $(".fee_transposter").html(res.fee.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") + "đ");
                            $("#fee_transporter_input").val(res.fee);
                            $("#total_money").html((parseInt($("#total_money").attr("total_money")) + parseInt(res.fee)).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") + "đ");
                        }else if(res && res.status == 0){
                            $("#message_error").html(res.message);

                        }
                    }
                });
            }
        });
        $("#ngan_luong").on('click',function () {
            if($(this).is(':checked')){
                $("#nlpayment").show();
            }else $("#nlpayment").hide();
        });

        $(".quantitty_number").on('change',function () {
            var quantity = $(this).val();
            var product_id = $(this).attr('product_id');
            var product_price = $(this).attr('product_price');
            var old_quantity = $(this).attr('old_quantity');
            var old_money = $(this).attr('old_money');
            var total_money = (parseInt(quantity) - parseInt(old_quantity))*parseInt(product_price) + parseInt(old_money);
            $("#total_money").html(total_money);
        });

        function updatePrice() {
            var quantity = $(".quantitty_number").val();
            var product_id = $('.quantitty_number').attr('product_id');
            var product_price = $('.quantitty_number').attr('product_price');
            var old_quantity = $('.quantitty_number').attr('old_quantity');
            var old_money = $('.quantitty_number').attr('old_money');
            var total_money = (parseInt(quantity) - parseInt(old_quantity))*parseInt(product_price) + parseInt(old_money);
            $("#total_money").html(total_money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") + "đ");
            console.log(quantity);
            $.ajax({
                url : "/shopping/add_cart",
                method :"post",
                data : {
                    product_id : product_id,
                    product_quantity : quantity,
                    update_cart : 1
                },
                success : function (res) {

                }
            })
        }
        $(document).on("click", ".btn-plus", updatePrice);
        $(document).on("click", ".btn-minus", updatePrice);
    });
})(jQuery);