(function($) {
    "use strict";
    if ($('.scroll-top').length) {
        var scrollTrigger = 100, // px
            backToTop = function() {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('.scroll-top').addClass('show');

                } else {
                    $('.scroll-top').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function() {
            backToTop();
            
        });
        $('.scroll-top').on('click', function(e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }
    $(window).load(function() {
        $('.slider').nivoSlider({
            effect: 'random',
            animSpeed: 500,
            pauseTime: 8000
        });
        $('.slider').on('swipeleft', function(e) {
            $('a.nivo-nextNav').trigger('click');
            e.stopPropagation();
            return false;
        });
        $('.slider').on('swiperight', function(e) {
            $('a.nivo-prevNav').trigger('click');
            e.stopPropagation();
            return false;
        });
    });

    $("#login-form").validate({
        rules: {
            'username': {required: true},
            'password': {required: true}
        },
        messages: {
            "username": {required: "Email hoặc Số điện thoại."},
            "password": {required: "Bạn vui lòng nhập mật khẩu."}
        }
    });

    $("#customer_info").validate({
        rules : {
            'user[fullname]' : {
                required : true
            },
            'user[email]' : {
                required : true
            },
            'user[phone]' : {
                required : true
            },
            'birth[day]' : {
                required : true
            },
            'birth[month]' : {
                required : true
            },
            'birth[year]' : {
                required : true
            }
        },
        messages : {
            'user[fullname]' : {
                required : 'Vui lòng nhập họ và tên.'
            },
            'user[email]' : {
                required : 'Vui lòng nhập email.'
            },
            'user[phone]' : {
                required : 'Vui lòng nhập số điện thoại'
            },
            'birth[day]' : {
                required : 'Thiếu ngày sinh'
            },
            'birth[month]' : {
                required : 'Thiếu tháng'
            },
            'birth[year]' : {
                required : 'Thiếu năm'
            }
        }
    })

    $("#contact_info").validate({
        rules : {
            'contact_customer[fullname]' : {
                required : true
            },
            'contact_customer[email]' : {
                required : true
            },
            'contact_customer[phone]' : {
                required : true
            },
            'contact_customer[content]' : {
                required : true
            }
        },
        messages : {
            'contact_customer[fullname]' : {
                required : 'Vui lòng nhập họ và tên.'
            },
            'contact_customer[email]' : {
                required : 'Vui lòng nhập email.'
            },
            'contact_customer[phone]' : {
                required : 'Vui lòng nhập số điện thoại'
            },
            'contact_customer[content]' : {
                required : 'Vui lòng nhập nội dung'
            }
        }
    })

    $("#register-form").validate({
        rules: {
            'register[password]': {minlength: 6, required: true},
            'register[password_again]': {equalTo: '#register_password'},
            'register[email]': {
                required: true,
            },
            'register[fullname]': {required: true},
            'register[phone]': {required: true},
            'register_has': {required: true}
        },
        messages: {
            'register[password]': {
                required: "Bạn vui lòng nhập mật khẩu.",
                minlength: "Mật khẩu tối thiểu phải có 6 ký tự."
            },
            'register[password_again]': {notEqual: "Nhập lại mật khẩu không đúng.",},
            'register[email]': {
                required: "Bạn vui lòng nhập email.",
                email: "Email chưa đúng định dạng."
            },
            'register[fullname]': {required: "Bạn vui lòng nhập họ và tên."},
            'register[phone]': {required: "Bạn vui lòng nhập số điện thoại",},
            'register_has': {required: "Bạn chưa đồng ý với điều khoản của website."}
        }
    });

    $("#register-form select").change(function () {
        if ($(this).valid()) {
            $(this).closest('.form-group .error').removeClass('error');
        } else {
            $(this).closest('.form-group').find('.form-control').addClass('error');
            $('.birthday-picker label.error').appendTo('.birthday-picker');
        }
    });
    $("#register-form .register_has").change(function (e) {
        if (!$("#register-form .register_has").valid()) {
            $("#register-form .register_has").parent().find('label.error').remove();
            $("#register-form .register_has").parent().addClass('error');
        } else {
            $("#register-form .register_has").parent().removeClass('error');
        }
    })
    $("#register-form").submit(function (e) {
        $(this).find('select').each(function () {
            if ($(this).valid()) {
                $(this).closest('.form-group .error').removeClass('error');
            } else {
                $(this).closest('.form-group').find('.form-control').addClass('error');
                $('.birthday-picker label.error').appendTo('.birthday-picker');
            }
        })
        if (!$("#register-form .register_has").valid()) {
            $("#register-form .register_has").parent().addClass('error');
        } else {
            $("#register-form .register_has").parent().removeClass('error');
        }
        if (!$("#register-form").valid()) {
            $("#register-form .register_has").parent().find('label.error').remove();
        }
    });

    $('.slider__product').slick({
        dots: false,
        arrows:false,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        lazyLoad: 'ondemand',
        responsive: [{
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            }, {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
    $('.slider-partner').slick({
        dots: false,
        arrows:false,
        infinite: true,
        speed: 300,
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 8,
        slidesToScroll: 8,
        lazyLoad: 'ondemand',
        responsive: [{
                breakpoint: 991,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 6,
                    infinite: true,
                    dots: true
                }
            }, {
                breakpoint: 768,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            }, {
                breakpoint: 320,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });

    $('.nav-slider__product .nav.prev').click(function(){
      $('.slider__product').slick('slickPrev');
    });

    $('.nav-slider__product .nav.next').click(function(){
      $('.slider__product').slick('slickNext');
    });
     $(".nav-toggle").click(function(){
        if($(".menu-category").hasClass("show-menu")){
            $(".menu-category").removeClass("show-menu");
            $(".common-overlay").removeClass("show");
        }else{
            $(".menu-category").addClass("show-menu");
            $(".common-overlay").addClass("show");
    

        }
    });
     $(".common-overlay").click(function(){
        if($(".common-overlay").hasClass("show")){
            $(".menu-category").removeClass("show-menu");
            $(".common-overlay").removeClass("show");
        }else{
            $(".menu-category").addClass("show-menu");
            $(".common-overlay").addClass("show");
    

        }
    });
    $(".menu-category .catalog__item").after().click(function(){
        $(this).toggleClass("active");
    });
    $('.product__view__image--thumb').slick({
        slidesToShow: 1,
        slidesToScroll: 2,
        verticalSwiping: true,
        speed: 300,
        autoplay: true,
        autoplaySpeed: 2000,
        vertical: true,
        arrows: true
    });
    $('.modal').on('shown.bs.modal', function (e) {
        $('.product__view__image--list,.product__view__image--thumb').resize();
    });
    if($("#slider__price,#lower-price,#upper-price").length){
        var slider__price = document.getElementById('slider__price');

        noUiSlider.create(slider__price, {
            connect: true,
            behaviour: 'tap',
            start: [ 500, 4000 ],
            range: {
                'min': [ 0 ],
                '10%': [ 500, 500 ],
                '50%': [ 4000, 1000 ],
                'max': [ 10000 ]
            }
        });
        var nodes = [
            document.getElementById('lower-price'), // 0
            document.getElementById('upper-price')  // 1
        ];
        slider__price.noUiSlider.on('update', function ( values, handle, unencoded, isTap, positions ) {
            nodes[handle].innerHTML = values[handle] ;
        });
    }
    if($("#elevate__zoom,#image-additional-carousel").length){
        if( $("body").width() > 400 ){
            var zoomCollection = '#elevate__zoom';
            $( zoomCollection ).elevateZoom({
                lensShape : "round",
                lensSize    : 50,
                easing:true,
                scrollZoom : true,
                gallery:'image-additional-carousel',
                cursor: 'pointer',
                galleryActiveClass: "active"
            });
        }   
        
    }
    if($(".fancybox").length){
        $('.fancybox').fancybox();
    }
    $(".modal__close").click(function(){
        $(".zoomContainer").remove();
    });
    $(document).ready(function(){
        $('[data-toggle="tooltip"],[data-toggle="modal"]').tooltip();   
    });
    // if($(".fancybox").length){
    //     $('.fancybox').fancybox();
    // }
})(jQuery);