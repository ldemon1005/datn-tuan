<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 7/12/2017
 * Time: 2:40 PM
 */
trait ObjectInstanceExt
{
    /**
     * @param object|array $arr
     *
     * @return static
     */
    public static function newInstance($arr = null)
    {
        if ($arr == null) {
            return new static();
        }
        if (!is_object($arr)) {
            $arr = (object)$arr;
        }
        $instance = ClassUtils::mapObject($arr, static::class);
        return $instance;
    }


    /**
     * map_object
     *
     * @param $arr mixed
     *
     * @return static
     * Set giá trị cho properties
     */
    public function mapObject($arr)
    {
        if (is_object($arr)) {
            $arr = get_object_vars($arr);
        }
        /*foreach ($this->toArray() as $key => $item) {
            if (isset($arr[$key])) {
                if (!empty($arr[$key])) {
                $arr[$key] = trim($arr[$key]);
                }
                $this->{$key} = $arr[$key];
            }
        }*/
        foreach ($arr as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
        return $this;
    }

}