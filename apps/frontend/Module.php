<?php
namespace DoGo\Frontend;

use Phalcon\DiInterface;
use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Config;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use DoGo\Frontend\Library\Helper;

class Module implements ModuleDefinitionInterface {
    /**
     * Registers an autoloader related to the module
     * @param DiInterface $di
     */
    public function registerAutoloaders(DiInterface $di = null) {
        $loader = new Loader();
        $loader->registerNamespaces([
            'DoGo\Frontend\Controllers' => __DIR__ . '/controllers/',
            'DoGo\Frontend\Models' => __DIR__ . '/models/',
            'DoGo\Frontend\Library' => __DIR__ . '/library/'
        ]);
        $loader->registerDirs([
            __DIR__.'/service',__DIR__.'/../library'
        ]);
        $loader->register();
    }

    /**
     * Registers services related to the module
     * @param DiInterface $di
     */
    public function registerServices(DiInterface $di)
    {
        /**
         * Read common configuration
         */
        $config = $di->has('config') ? $di->getShared('config') : null;

        /**
         * Try to load local configuration
         */
        if (file_exists(__DIR__ . '/config/config.php')) {
            $override = include __DIR__ . '/config/config.php';

            if ($config instanceof Config) {
                $config->merge($override);
            } else {
                $config = $override;
            }
        }

        $di->set('config', function () use ($config) {
            return $config;
        });

        /**
         * Setting up the view component
         */
        $di['view'] = function () use ($config) {
            $view = new View();
            $view->setViewsDir($config->get('application')->viewsDir);
            $view->registerEngines(array(
                '.volt' => function ($view, $di) use ($config) {

                    $volt = new VoltEngine($view, $di);

                    $volt->setOptions(array(
                        'compiledPath' => $config->application->cacheDir,
                        'compiledSeparator' => '_',
                        'stat' => true,
                        'compileAlways' => true
                    ));
                    //load function php
                    $compiler = $volt->getCompiler();
                    //define variable translate
                    $compiler->addFunction('in_array', 'in_array');
                    $compiler->addFunction('br2nl', 'br2nl');
                    $compiler->addFunction('number_format', function ($resolvedArgs, $exprArgs) use ($compiler) {
                        $firstArgument = $compiler->expression($exprArgs[0]['expr']);
                        return 'number_format('.$firstArgument.", 0, '.', ',')";
                    });
                    return $volt;
                },
                '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
            ));
            return $view;
        };

        /**
         * Database connection is created based in the parameters defined in the configuration file
         */
        $di['db'] = function () use ($config) {
            $config = $config->database->toArray();

            $dbAdapter = '\Phalcon\Db\Adapter\Pdo\\' . $config['adapter'];
            unset($config['adapter']);

            return new $dbAdapter($config);
        };
    }
}
