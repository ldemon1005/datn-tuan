<div class="sec sec__shopcart">

    <div class="container">
        <div class="nav-steps">
            <div class="nav-step__item waiting">
                <span class="number">1</span>
                <span class="txt">Giỏ hàng</span>
            </div>
            <div class="nav-step__item">
                <span class="number">2</span>
                <span class="txt">Nhập thông tin</span>
            </div>
            <div class="nav-step__item">
                <span class="number">3</span>
                <span class="txt">Xác nhận thông tin</span>
            </div>
            <div class="nav-step__item">
                <span class="number">4</span>
                <span class="txt">Hoàn thành</span>
            </div>
        </div>
        <div class="sub-name-step">Giỏ hàng của tôi</div>
        <div class="row">
            <div class="sec__shopcart__info col-md-8 col-sm-12 col-xs-12">
                <div class="box-user__info panel">
                    <div class="content-info-check">
                        <div class="table-product">
                            <div class="title">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <span class=""> 2 Sản phẩm</span>
                                    </div>
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <span class="textr">Đơn giá</span>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <span class="">Số lượng</span>
                                    </div>
                                </div>
                            </div>
                            <div class="content ">
                                <?php foreach ($list_product as $item) { ?>
                                    <div class="view-cart__product">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="img-product col-md-6 col-sm-6">
                                                    <img style="height: 100px;width: auto" src="<?= $item['avatar'] ?>" alt="Name your Product ">
                                                </div>
                                                <div class="sub__product col-md-6 col-sm-6">
                                                    <p class="product__name">
                                                        <?= $item['name'] ?>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-3 col-xs-3 hidden-mb">
                                                <p class="dongia"> <?= $item['price_disconut'] ?>₫</p>
                                            </div>
                                            <div class="col-md-3 col-sm-2 col-xs-2">
                                                <div class="product__quantity">
                                                    <button class="btn-minus btn">
                                                        -
                                                    </button>
                                                    <input class="input-number quantitty_number"
                                                           product_id="<?= $item['id'] ?>"
                                                           product_price="<?= $item['price_disconut'] ?>"
                                                           old_money="<?= $total_money ?>"
                                                           old_quantity="<?= $item['quantity'] ?>"
                                                           value="<?= $item['quantity'] ?>" type="text">
                                                    <button class="btn-plus btn">
                                                        +
                                                    </button>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-1 col-sm-1 col-xs-1">
                                                <a class="btn-delete delete_cart pointer" id_item="<?= $item['id'] ?>"><span class="icon -ap icon-trash2"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 col-price-total">
                <div class="name-top__step">Thông tin đơn hàng</div>
                <div class="">
                    <div class="price-total">
                        <div class="pull-left">Thành tiền :</div>
                        <div class="pull-right" id="total_money"><?= $total_money ?>₫</div>
                    </div>
                </div>
                <div class="list-button">
                    <a href="/shopping/shop_cart/<?= $step ?>" class="btn btn__next">Tiến hành thanh toán</a>
                </div>
            </div>
        </div>
    </div>
</div>