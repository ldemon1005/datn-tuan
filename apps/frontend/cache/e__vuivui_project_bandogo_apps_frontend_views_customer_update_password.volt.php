<header class="header">
    <div class="banner">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="logo">
                        <img src="/frontend_res/assets/uploads/logo.png" alt="logo"/>
                    </div>
                </div>
                <!--<div class="col-md-5 col-sm-5 col-xs-12">-->
                <!--<div class="center-bn">-->
                <!--<a><span class="red">Hotline :</span> (+84) 0123465789</a>-->
                <!--|-->
                <!--<a href="#">Tìm cửa hàng trên toàn quốc</a>-->
                <!--</div>-->
                <!--</div>-->
                <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                <!--<div class="box-search">-->
                <!--<div class="form-input">-->
                <!--<input type="text" class="form-control" placeholder="Search...">-->
                <!--<button class="btn btn__search"><span class="fa fa-search"></span></button>-->
                <!--</div>-->
                <!--</div>-->
                <!--</div>-->
                <div class="col-md-9 col-sm-9 col-xs-12 col-right-headtop">
                    <div class="pull-left">
                        <div class="box-search">
                            <div class="form-input">
                                <input type="text" class="form-control" placeholder="Tìm kiếm...">
                                <button class="btn btn__search"><span class="fa fa-search"></span></button>
                            </div>
                        </div>
                    </div>
                    <div class="menu-top pull-right">
                        <div class="col-menu-top">
                            <div class="hotline"><span class="-ap icon-phone-handset"></span> Hotline: <?= $contact[0]['hotline'] ?></div>
                            <div class="header-top__shopcart">
                                <div class="icon">
                                    <span class="-ap icon-cart"></span>
                                </div>
                                <div class="headline">Giỏ hàng <span class="number-cart"><?= $total_product ?></span></div>
                                <div class="dropdown__shop__cart">
                                    <div class="dropdown__head">
                                        <span class="number"><?= $total_product ?></span>
                                        <span class="txt">Sản phẩm trong giỏ hàng</span>
                                    </div>
                                    <div class="product__shopcart" id="cart_product">
                                        <div class="product__list" >
                                            <?php foreach ($cart_product as $item) { ?>
                                                <div class="product__list__item">
                                                    <div class="product__img">
                                                        <img src="<?= $item['avatar'] ?>" alt="Name your Product ">
                                                    </div>
                                                    <div class="product__name">
                                                        <?= $item['name'] ?>
                                                        <a class="product__delete">
                                                            <span class="icon -ap icon-trash2"></span>
                                                        </a>
                                                    </div>

                                                    <div class="product__price">
                                                        <?php if ($item['type_discount'] == 1) { ?>
                                                            <div class="product__price__old"><?= $item['price'] ?><span class="unit">đ</span></div>
                                                            <div class="product__price__regular"><?= $item['price'] - $item['discount'] ?> <span class="unit">đ</span></div>
                                                        <?php } else { ?>
                                                            <div class="product__price__old"><?= $item['price'] ?><span class="unit">đ</span></div>
                                                            <div class="product__price__regular"><?= $item['price'] * (100 - $item['discount']) / 100 ?> <span class="unit">đ</span></div>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <form method="post">
                                        <div class="dropdown__bottom">
                                            
                                            <div class="total">Tổng cộng: <span class="product__price number" id="total_money_cart"><?= $total_money ?> <span class="unit"></span>đ</span></div>
                                            <a href="/shopping/shop_cart" class="btn__checkout">Tiến hành đặt hàng</a>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <?php if ($auth) { ?>
                                <div class="header-top__login">
                                    <div class="acc-mobi">
                                        <div class="title"><?= ($auth['fullname'] ? $auth['fullname'] : $auth['email']) ?></div>
                                        <ul class="menu-icons__sub">
                                            <li class="menu-icons__sub__item"><a class="menu-icon__sub-link" href="/customer" title=""><span class="fa fa-user"></span> Thông tin tài khoản</a></li>
                                            <li class="menu-icons__sub__item">
                                                <a href="/auth/logout" class="menu-icon__sub-link" title="">
                                                    <span class="fa fa-unlock-alt"></span> Thoát
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="header-top__login">
                                    <div class="acc-mobi">
                                        <div class="title">Tài khoản</div>
                                        <ul class="menu-icons__sub">
                                            <li class="menu-icons__sub__item"><a class="menu-icon__sub-link" data-toggle="modal" data-target="#login" data-original-title="" title=""><span class="icon icon-lock_outline -ap"></span> Đăng nhập</a></li>
                                            <li class="menu-icons__sub__item"><a class="menu-icon__sub-link" data-toggle="modal" data-target="#register" data-original-title="" title=""><span class="icon icon-lock_open -ap"></span> Đăng ký</a></li>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <!---->
                    </div>
                    <!---->
                </div>
            </div>
            <div class="nav-toggle">
                <span class="fa fa-bars"></span>
            </div>
        </div>
    </div>
    <div class="header-menu">
        <div class="container">
            <div class="menu-category">
                <div class="catalog__list">
                    <ul class="catalog__wrap">
                        <li class="catalog__item active">
                            <a href="/" class="catalog__link">Trang chủ</a>
                        </li>
                        <li class="catalog__item"><a href="/agency" class="catalog__link"> Giới thiệu</a></li>
                        <li class="catalog__item -sub">
                            <a href="/product"  class="catalog__link">Sản phẩm</a>
                            <div class="catalog__sub">
                                <div class="row">
                                    <div class="col-md-4 col-sm-5 col-xs-12">
                                        <ul class="list-cat__sub">
                                            <?php foreach ($category_sidebar as $item) { ?>
                                                <li class="catalog-sub__item"><a href="/product/product_category/<?= $item['id'] ?>"><?= $item['name'] ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="col-dm-8 col-sm-7 col-xs-12">
                                        <div class="catalog__title"><span>MỚI NHẤT HÔM NAY</span></div>
                                        <div class="catalog__product">
                                            <div class="product__image">
                                                <a href="#">
                                                    <img src="<?= $product_new['avatar'] ?>" alt="">
                                                </a>
                                                <div class="product__status -new">New </div>
                                            </div>
                                            <a href="/product/product_detail/<?= $product_new['id'] ?>" class="product__name"><?= $product_new['name'] ?></a>
                                            <div class="product__price">
                                                <?php if ($product_new['type_discount'] == 1) { ?>
                                                    <div class="product__price__old"><?= $product_new['price'] ?><span class="unit">đ</span></div>
                                                    <div class="product__price__regular"><?= $product_new['price'] - $product_new['discount'] ?> <span class="unit">đ</span></div>
                                                <?php } else { ?>
                                                    <div class="product__price__old"><?= $product_new['price'] ?><span class="unit">đ</span></div>
                                                    <div class="product__price__regular"><?= $product_new['price'] * (100 - $product_new['discount']) / 100 ?> <span class="unit">đ</span></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="catalog__item"><a href="/articel" class="catalog__link"> Tin tức</a></li>
                        <li class="catalog__item"><a href="/contact" class="catalog__link">Liên hệ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="breadcrumb">
    <div class="container">
        <a href="/" class="li">Trang chủ </a>
        <span class="li active">Thay đổi mật khẩu</span>
    </div>
</div>

<div class="user-profile__wrap sec">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="sidebar-profile">
    <div class="profiles">
        <div class="avatar" id="avatar_customer">
            <a href="#"><img src="<?= ($user['avatar'] ? $user['avatar'] : '//ssl.gstatic.com/accounts/ui/avatar_2x.png') ?>" alt="<?= $user['fullname'] ?>"></a>
        </div>
        <div class="headline">Tài khoản của</div>
        <div class="username"><?= $user['email'] ?></div>
    </div>
    <div class="user-profile__menu">
        <ul class="user-profile__menu__wrap">
            <li class="user-profile__item <?= ($this->router->getActionName() == null ? 'active' : '') ?>">
                <a href="/customer" class="user-profile__link"><span class="-ap icon-user6 icon"></span> <span class="txt">Thông tin tài khoản</span></a>
            </li>
            <li class="user-profile__item <?= ($this->router->getControllerName() == 'customer' && ($this->router->getActionName() == 'order' || $this->router->getActionName() == 'order_detail') ? 'active' : '') ?>">
                <a href="/customer/order" class="user-profile__link"><span class="-ap icon-file-text icon"></span> <span class="txt">Quản lý đơn hàng</span></a>
            </li>
            <li class="user-profile__item <?= ($this->router->getControllerName() == 'customer' && $this->router->getActionName() == 'update_password' ? 'active' : '') ?>">
                <a href="/customer/update_password" class="user-profile__link"><span class="fa fa-exchange icon"></span> <span class="txt">Thay đổi mật khẩu</span></a>
            </li>
        </ul>
    </div>
</div>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="wrap__profile__info">
                    <h2 class="title-profile">Thay đổi mật khẩu</h2>
                    <form method="post" id="change_pass" action="/auth/update_password" class="profile-content">
                        <div id="change_password_account">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12"><?= $this->flash->output() ?></div>
                                    <div class="input__label">
                                        <div class="label">Mật khẩu cũ</div>
                                    </div>
                                    <div class="input__wrap">
                                        <input name="password[current_password]" type="password" class="form-control" placeholder="Mật khẩu cũ" value="<?= ($current_password ? $current_password : '') ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="input__label">
                                        <div class="label">Mật khẩu mới</div>
                                    </div>
                                    <div class="input__wrap">
                                        <input name="password[password]" id="password" type="password" class="form-control" placeholder="Mật khẩu mới" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="input__label">
                                        <div class="label">Nhập lại mật khẩu</div>
                                    </div>
                                    <div class="input__wrap">
                                        <input name="password[password_again]" type="password" class="form-control" placeholder="Mật khẩu mới" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group login__action">
                            <button id="update" type="submit" class="btn btn-submit">Cập nhật</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <!-- End topfoter -->
    <!-- footer -->
    <div class="container">
        <div class="footer__box">
            <div class="row ">
                <div class="col-md-4  col-sm-4 col-xs-12">
                    <h3 class="footer__title ">Trang Bán Đồ Gỗ</h3>
                    <div class="list-menu-ft">
                        <ul>
                            <li><a href="/index"><i class="fa fa-check" aria-hidden="true"></i> Trang chủ</a></li>
                            <li><a href="/agence"><i class="fa fa-check" aria-hidden="true"></i> Giới thiệu</a></li>
                            <li><a href="/product"><i class="fa fa-check" aria-hidden="true"></i> Sản phẩm</a></li>
                            <li><a href="/articel"><i class="fa fa-check" aria-hidden="true"></i> Tin tức</a></li>
                        </ul>
                    </div>
                    
                    
                        
                        
                            
                            
                        
                    
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h3 class="footer__title ">Tin tức</h3>
                    <div class="list-news-ft">
                        <ul>
                            <?php foreach ($articel_footer as $item) { ?>
                                <li>
                                    <div class="avt">
                                        <a href=""><img src="<?= $item['avatar'] ?>" alt="<?= $item['name'] ?>"></a>
                                    </div>
                                    <div class="ct">
                                        <p class="name"><a href=""><?= $item['name'] ?></a></p>
                                        <p class="date"><i class="fa fa-calendar" aria-hidden="true"></i> <?= $item['datecreate'] ?></p>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4  col-sm-4 col-xs-12">
                    <h3 class="footer__title ">Liên hệ</h3>
                    <?php foreach ($contact as $item) { ?>
                        <div class="contact">
                            <p class="name-comp"><?= $item['name'] ?></p>
                            <p class="info ">
                                <span class="icon -ap icon-location3"></span>
                                <span class="name-header texU disb fow">Địa chỉ: </span>
                                <span class="disb"><?= $item['address'] ?></span>
                            </p>
                        </div>
                        <div class="contact">
                            <p class="info ">
                                <span class="icon -ap  icon-envelope2"></span>
                                <span class="name-header texU disb fow">Email:</span>
                                <span class="disb"><?= $item['email'] ?></span>
                            </p>
                        </div>
                        <div class="contact">
                            <p class="info ">
                                <span class="icon fa fa-phone"></span>
                                <span class="name-header texU disb fow"> Phone: </span>
                                <span class="disb"><?= $item['phone'] ?></span>
                            </p>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <!-- End footer -->
    <!-- Copyright -->
    <div class="bottom">
        <div class="container">
            <div class="pull-left">
                <div class="copyright">
                    Copyright ® 2017 <a href="" class="url">ABC</a>
                </div>
            </div>
        </div>
    </div>
    <!-- end copyright -->

</footer>

<script>
    function checkCheckbook(obj) {
        $("#change_password_account").toggle();
    }

    $(document).ready(function () {
        $("#change_pass").validate({
            rules : {
                'password[current_password]' : {
                    required : true
                },
                'password[password]' : {required:true},
                'password[password_again]' : {
                    equalTo : '#password'
                }
            },

            messages: {
                'password[current_password]' : {
                    required : "Bạn chưa nhập mật khẩu hiện tại."
                },
                'password[password]' : {
                    required : "Bạn chưa nhập mật khẩu mới."
                },
                'password[password_again]' : {
                    notEqual: "Nhập lại mật khẩu không đúng."
                }
            }
        });
    });
</script>