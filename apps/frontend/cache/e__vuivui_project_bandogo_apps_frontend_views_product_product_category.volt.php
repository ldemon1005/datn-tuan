<header class="header">
    <div class="banner">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="logo">
                        <img src="/frontend_res/assets/uploads/logo.png" alt="logo"/>
                    </div>
                </div>
                <!--<div class="col-md-5 col-sm-5 col-xs-12">-->
                <!--<div class="center-bn">-->
                <!--<a><span class="red">Hotline :</span> (+84) 0123465789</a>-->
                <!--|-->
                <!--<a href="#">Tìm cửa hàng trên toàn quốc</a>-->
                <!--</div>-->
                <!--</div>-->
                <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                <!--<div class="box-search">-->
                <!--<div class="form-input">-->
                <!--<input type="text" class="form-control" placeholder="Search...">-->
                <!--<button class="btn btn__search"><span class="fa fa-search"></span></button>-->
                <!--</div>-->
                <!--</div>-->
                <!--</div>-->
                <div class="col-md-9 col-sm-9 col-xs-12 col-right-headtop">
                    <div class="pull-left">
                        <div class="box-search">
                            <div class="form-input">
                                <form method="post" action="/product">
                                    <input type="text" name="text_search" class="form-control" placeholder="Tìm kiếm...">
                                    <button type="submit" class="btn btn__search"><span class="fa fa-search"></span></button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="menu-top pull-right">
                        <div class="col-menu-top">
                            <div class="hotline"><span class="-ap icon-phone-handset"></span> Hotline: <?= $contact[0]['hotline'] ?></div>
                            <div class="header-top__shopcart">
                                <div class="icon">
                                    <span class="-ap icon-cart"></span>
                                </div>
                                <div class="headline">Giỏ hàng <span class="number-cart"><?= $total_product ?></span></div>
                                <div class="dropdown__shop__cart">
                                    <div class="dropdown__head">
                                        <span class="number"><?= $total_product ?></span>
                                        <span class="txt">Sản phẩm trong giỏ hàng</span>
                                    </div>
                                    <div class="product__shopcart" id="cart_product">
                                        <div class="product__list" >
                                            <?php foreach ($cart_product as $item) { ?>
                                                <div class="product__list__item">
                                                    <div class="product__img">
                                                        <img style="width: 200px" src="<?= $item['avatar'] ?>" alt="Name your Product ">
                                                    </div>
                                                    <div class="product__name">
                                                        <?= $item['name'] ?>
                                                        <a class="product__delete">
                                                            <span class="icon -ap icon-trash2"></span>
                                                        </a>
                                                    </div>
                                                    <div class="product__price">
                                                        <?php if ($item['type_discount'] == 1) { ?>
                                                            <div class="product__price__old"><?= number_format($item['price'], 2, '.', ' ') ?><span class="unit">đ</span></div>
                                                            <div class="product__price__regular"><?= number_format($item['price'] - $item['discount'], 2, '.', ' ') ?> <span class="unit">đ</span></div>
                                                        <?php } else { ?>
                                                            <div class="product__price__old"><?= number_format($item['price'], 2, '.', ' ') ?><span class="unit">đ</span></div>
                                                            <div class="product__price__regular"><?= number_format($item['price'] * (100 - $item['discount']) / 100, 2, '.', ' ') ?> <span class="unit">đ</span></div>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <form method="post">
                                        <div class="dropdown__bottom">
                                            
                                            <div class="total">Tổng cộng: <span class="product__price number" id="total_money_cart"><?= number_format($total_money, 2, '.', ' ') ?> <span class="unit"></span>đ</span></div>
                                            <a href="/shopping/shop_cart" class="btn__checkout">Tiến hành đặt hàng</a>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <?php if ($auth) { ?>
                                <div class="header-top__login">
                                    <div class="acc-mobi">
                                        <div class="title"><?= ($auth['fullname'] ? $auth['fullname'] : $auth['email']) ?></div>
                                        <ul class="menu-icons__sub">
                                            <li class="menu-icons__sub__item"><a class="menu-icon__sub-link" href="/customer" title=""><span class="fa fa-user"></span> Thông tin tài khoản</a></li>
                                            <li class="menu-icons__sub__item">
                                                <a href="/auth/logout" class="menu-icon__sub-link" title="">
                                                    <span class="fa fa-unlock-alt"></span> Thoát
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="header-top__login">
                                    <div class="acc-mobi">
                                        <div class="title">Tài khoản</div>
                                        <ul class="menu-icons__sub">
                                            <li class="menu-icons__sub__item"><a class="menu-icon__sub-link" data-toggle="modal" data-target="#login" data-original-title="" title=""><span class="icon icon-lock_outline -ap"></span> Đăng nhập</a></li>
                                            <li class="menu-icons__sub__item"><a class="menu-icon__sub-link" data-toggle="modal" data-target="#register" data-original-title="" title=""><span class="icon icon-lock_open -ap"></span> Đăng ký</a></li>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <!---->
                    </div>
                    <!---->
                </div>
            </div>
            <div class="nav-toggle">
                <span class="fa fa-bars"></span>
            </div>
        </div>
    </div>
    <div class="header-menu">
        <div class="container">
            <div class="menu-category">
                <div class="catalog__list">
                    <ul class="catalog__wrap">
                        <li class="catalog__item active">
                            <a href="/" class="catalog__link">Trang chủ</a>
                        </li>
                        <li class="catalog__item"><a href="/agency" class="catalog__link"> Giới thiệu</a></li>
                        <li class="catalog__item -sub">
                            <a href="/product"  class="catalog__link">Sản phẩm</a>
                            <div class="catalog__sub">
                                <div class="row">
                                    <div class="col-md-4 col-sm-5 col-xs-12">
                                        <ul class="list-cat__sub">
                                            <?php foreach ($category_sidebar as $item) { ?>
                                                <li class="catalog-sub__item"><a href="/product/product_category/<?= $item['id'] ?>"><?= $item['name'] ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="col-dm-8 col-sm-7 col-xs-12">
                                        <div class="catalog__title"><span>MỚI NHẤT HÔM NAY</span></div>
                                        <div class="catalog__product">
                                            <div class="product__image">
                                                <a href="#">
                                                    <img src="<?= $product_new['avatar'] ?>" alt="">
                                                </a>
                                                <div class="product__status -new">New </div>
                                            </div>
                                            <a href="/product/product_detail/<?= $product_new['id'] ?>" class="product__name"><?= $product_new['name'] ?></a>
                                            <div class="product__price">
                                                <?php if ($product_new['type_discount'] == 1) { ?>
                                                    <div class="product__price__old"><?= number_format($product_new['price'], 2, '.', ' ') ?><span class="unit">đ</span></div>
                                                    <div class="product__price__regular"><?= number_format($product_new['price'] - $product_new['discount'], 2, '.', ' ') ?> <span class="unit">đ</span></div>
                                                <?php } else { ?>
                                                    <div class="product__price__old"><?= number_format($product_new['price'], 2, '.', ' ') ?><span class="unit">đ</span></div>
                                                    <div class="product__price__regular"><?= number_format($product_new['price'] * (100 - $product_new['discount']) / 100, 2, '.', ' ') ?> <span class="unit">đ</span></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="catalog__item"><a href="/articel" class="catalog__link"> Tin tức</a></li>
                        <li class="catalog__item"><a href="/contact" class="catalog__link">Liên hệ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<style>
    a.disabled {
        pointer-events: none;
        cursor: default;
        background: #dddddd;
    }
</style>
<!-- End menu -->
<div class="nav-anchor"></div>
<div class="hero-banner">
    <div class="container">
        <div class="slider-wrapper theme-default">
            <div class="slider apollo_slides nivoSlider">
                <a href="#"><img src="/frontend_res/assets/uploads/slider.jpg"/></a>
                <a href="#"><img src="/frontend_res/assets/uploads/slider2.png"/></a>
            </div>
        </div>
    </div>
</div>
<div class="sec list-product">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12 col-left">
                <div class="box category">
    <div class="title-name">
        Danh mục sản phẩm
    </div>
    <div class="list">
        <ul>
            <?php foreach ($category_sidebar as $item) { ?>
                <li><a href="/product/product_category/<?= $item['id'] ?>"><?= $item['name'] ?></a></li>
            <?php } ?>
        </ul>
    </div>
</div>
            </div>
            <!--/-->
            <div class="col-md-9 col-sm-9 col-xs-12 col-right">
                <!---->
                <div class="box-ct">
                    <div class="list-ct">
                        <div class="row">
                            <?php foreach ($list_product as $item) { ?>
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="product__item">
                                        <div class="w">
                                            <div class="product__image">
                                                <a href="#" class="product__link"><img style="max-height: 80px" src="<?= $item['avatar'] ?>" alt="name your product"></a>
                                                <div class="product-item__actions">
                                                    <a class="btn btn-detail" data-toggle="tooltip" data-placement="top" href="/product/product_detail/<?= $item['id'] ?>" title="Chi tiết"><span class="-ap  icon-search2"></span></a>
                                                    <a class="btn btn-quickview" id="quick_view" product="<?= $item['id'] ?>" title="Xem nhanh"><span class="fa fa-eye"></span></a>
                                                </div>
                                                <div class="product__overlay"></div>
                                                <?php if ($item['type_discount'] == 2) { ?>
                                                    <div class="product__sale"><?= number_format($item['discount'], 2, '.', ' ') ?>%</div>
                                                <?php } ?>
                                            </div>
                                            <h3 class="product__name"><a href="#"><?= $item['name'] ?></a></h3>
                                            <div class="product__price">
                                                <?php if ($item['type_discount'] == 1) { ?>
                                                    <div class="product__price__old"><?= number_format($item['price'], 2, '.', ' ') ?><span class="unit">đ</span></div>
                                                    <div class="product__price__regular"><?= number_format($item['price'] - $item['discount'], 2, '.', ' ') ?> <span class="unit">đ</span></div>
                                                <?php } else { ?>
                                                    <div class="product__price__old"><?= number_format($item['price'], 2, '.', ' ') ?><span class="unit">đ</span></div>
                                                    <div class="product__price__regular"><?= number_format($item['price'] * (100 - $item['discount']) / 100, 2, '.', ' ') ?> <span class="unit">đ</span></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($Pagination['total'] > 0 && $Pagination['total_pages'] > 1 && $Pagination['current'] >= 1 && $Pagination['current'] <= $Pagination['total_pages']) { ?>
    <nav aria-label="Page navigation" class="page-pagination">
        <ul class="pagination">
            <?php if ($Pagination['current'] > 1) { ?>
                <li>
                    <a href="<?= $Pagination['current_link'] ?>p=<?= ($Pagination['current'] - 1) ?>" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
            <?php } ?>
            <?php foreach (range(1, $Pagination['total_pages']) as $i) { ?>
                <li><a href="<?= $Pagination['current_link'] ?>p=<?= $i ?>" class="<?= ($i == $Pagination['current'] ? 'active' : '') ?>"><?= $i ?></a></li>
            <?php } ?>
            <?php if ($Pagination['current'] < $Pagination['total_pages']) { ?>
                <li>
                    <a href="<?= $Pagination['current_link'] ?>p=<?= ($Pagination['current'] + 1) ?>" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </nav>
<?php } ?>
<?php if ($Pagination['total'] == 0) { ?>
    <p class="text-success text-center" style="font-size: 15px;padding-top: 10px">Không tìm thấy kết quả nào ở trang này</p>
<?php } ?>
                        </div>
                    </div>
                </div>
                <!---->
                <div class="box-ct">
                    <div class="title-name">
                        Tin mới
                    </div>
                    <div class="list-ct-news">
                        <div class="row">
                            <div class="col-md-7 col-sm-7 col-xs-12 col-left">
                                <div class="avt">
                                    <img src="<?= $articel_new['avatar'] ?>" alt="<?= $articel_new['name'] ?>">
                                </div>
                                <div class="ct">
                                    <div class="name">
                                        <a href="/articel/detail/<?= $articel_new['id'] ?>"><?= $articel_new['name'] ?></a>
                                    </div>
                                    <div class="date">
                                        <span><?= $articel_new['datecreate'] ?></span>
                                    </div>
                                    <div class="txt">
                                        <?= $articel_new['title'] ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-12 col-right">
                                <div class="list-n">
                                    <ul>
                                        <?php foreach ($list_articel as $item) { ?>
                                            <li><a href="/articel/detail/<?= $item['id'] ?>"><?= $item['name'] ?></a></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                            <!---->
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<!-- footer -->
<footer class="footer">
    <!-- End topfoter -->
    <!-- footer -->
    <div class="container">
        <div class="footer__box">
            <div class="row ">
                <div class="col-md-4  col-sm-4 col-xs-12">
                    <h3 class="footer__title ">Trang Bán Đồ Gỗ</h3>
                    <div class="list-menu-ft">
                        <ul>
                            <li><a href="/index"><i class="fa fa-check" aria-hidden="true"></i> Trang chủ</a></li>
                            <li><a href="/agence"><i class="fa fa-check" aria-hidden="true"></i> Giới thiệu</a></li>
                            <li><a href="/product"><i class="fa fa-check" aria-hidden="true"></i> Sản phẩm</a></li>
                            <li><a href="/articel"><i class="fa fa-check" aria-hidden="true"></i> Tin tức</a></li>
                        </ul>
                    </div>
                    
                    
                        
                        
                            
                            
                        
                    
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h3 class="footer__title ">Tin tức</h3>
                    <div class="list-news-ft">
                        <ul>
                            <?php foreach ($articel_footer as $item) { ?>
                                <li>
                                    <div class="avt">
                                        <a href="/articel/detail/<?= $item['id'] ?>"><img src="<?= $item['avatar'] ?>" alt="<?= $item['name'] ?>"></a>
                                    </div>
                                    <div class="ct">
                                        <p class="name"><a href="/articel/detail/<?= $item['id'] ?>"><?= $item['name'] ?></a></p>
                                        <p class="date"><i class="fa fa-calendar" aria-hidden="true"></i> <?= $item['datecreate'] ?></p>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4  col-sm-4 col-xs-12">
                    <h3 class="footer__title ">Liên hệ</h3>
                    <?php foreach ($contact as $item) { ?>
                        <div class="contact">
                            <p class="name-comp"><?= $item['name'] ?></p>
                            <p class="info ">
                                <span class="icon -ap icon-location3"></span>
                                <span class="name-header texU disb fow">Địa chỉ: </span>
                                <span class="disb"><?= $item['address'] ?></span>
                            </p>
                        </div>
                        <div class="contact">
                            <p class="info ">
                                <span class="icon -ap  icon-envelope2"></span>
                                <span class="name-header texU disb fow">Email:</span>
                                <span class="disb"><?= $item['email'] ?></span>
                            </p>
                        </div>
                        <div class="contact">
                            <p class="info ">
                                <span class="icon fa fa-phone"></span>
                                <span class="name-header texU disb fow"> Phone: </span>
                                <span class="disb"><?= $item['phone'] ?></span>
                            </p>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <!-- End footer -->
    <!-- Copyright -->
    <div class="bottom">
        <div class="container">
            <div class="pull-left">
                <div class="copyright">
                    Copyright ® 2017 <a href="" class="url">ABC</a>
                </div>
            </div>
        </div>
    </div>
    <!-- end copyright -->

</footer>