<header class="header">
    <div class="banner">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="logo">
                        <img src="/frontend_res/assets/uploads/logo.png" alt="logo"/>
                    </div>
                </div>
                <!--<div class="col-md-5 col-sm-5 col-xs-12">-->
                <!--<div class="center-bn">-->
                <!--<a><span class="red">Hotline :</span> (+84) 0123465789</a>-->
                <!--|-->
                <!--<a href="#">Tìm cửa hàng trên toàn quốc</a>-->
                <!--</div>-->
                <!--</div>-->
                <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                <!--<div class="box-search">-->
                <!--<div class="form-input">-->
                <!--<input type="text" class="form-control" placeholder="Search...">-->
                <!--<button class="btn btn__search"><span class="fa fa-search"></span></button>-->
                <!--</div>-->
                <!--</div>-->
                <!--</div>-->
                <div class="col-md-9 col-sm-9 col-xs-12 col-right-headtop">
                    <div class="pull-left">
                        <div class="box-search">
                            <div class="form-input">
                                <input type="text" class="form-control" placeholder="Tìm kiếm...">
                                <button class="btn btn__search"><span class="fa fa-search"></span></button>
                            </div>
                        </div>
                    </div>
                    <div class="menu-top pull-right">
                        <div class="col-menu-top">
                            <div class="hotline"><span class="-ap icon-phone-handset"></span> Hotline: <?= $contact[0]['hotline'] ?></div>
                            <div class="header-top__shopcart">
                                <div class="icon">
                                    <span class="-ap icon-cart"></span>
                                </div>
                                <div class="headline">Giỏ hàng <span class="number-cart"><?= $total_product ?></span></div>
                                <div class="dropdown__shop__cart">
                                    <div class="dropdown__head">
                                        <span class="number"><?= $total_product ?></span>
                                        <span class="txt">Sản phẩm trong giỏ hàng</span>
                                    </div>
                                    <div class="product__shopcart" id="cart_product">
                                        <div class="product__list" >
                                            <?php foreach ($cart_product as $item) { ?>
                                                <div class="product__list__item">
                                                    <div class="product__img">
                                                        <img src="<?= $item['avatar'] ?>" alt="Name your Product ">
                                                    </div>
                                                    <div class="product__name">
                                                        <?= $item['name'] ?>
                                                        <a class="product__delete">
                                                            <span class="icon -ap icon-trash2"></span>
                                                        </a>
                                                    </div>

                                                    <div class="product__price">
                                                        <?php if ($item['type_discount'] == 1) { ?>
                                                            <div class="product__price__old"><?= $item['price'] ?><span class="unit">đ</span></div>
                                                            <div class="product__price__regular"><?= $item['price'] - $item['discount'] ?> <span class="unit">đ</span></div>
                                                        <?php } else { ?>
                                                            <div class="product__price__old"><?= $item['price'] ?><span class="unit">đ</span></div>
                                                            <div class="product__price__regular"><?= $item['price'] * (100 - $item['discount']) / 100 ?> <span class="unit">đ</span></div>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <form method="post">
                                        <div class="dropdown__bottom">
                                            
                                            <div class="total">Tổng cộng: <span class="product__price number" id="total_money_cart"><?= $total_money ?> <span class="unit"></span>đ</span></div>
                                            <a href="/shopping/shop_cart" class="btn__checkout">Tiến hành đặt hàng</a>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <?php if ($auth) { ?>
                                <div class="header-top__login">
                                    <div class="acc-mobi">
                                        <div class="title"><?= ($auth['fullname'] ? $auth['fullname'] : $auth['email']) ?></div>
                                        <ul class="menu-icons__sub">
                                            <li class="menu-icons__sub__item"><a class="menu-icon__sub-link" href="/customer" title=""><span class="fa fa-user"></span> Thông tin tài khoản</a></li>
                                            <li class="menu-icons__sub__item">
                                                <a href="/auth/logout" class="menu-icon__sub-link" title="">
                                                    <span class="fa fa-unlock-alt"></span> Thoát
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="header-top__login">
                                    <div class="acc-mobi">
                                        <div class="title">Tài khoản</div>
                                        <ul class="menu-icons__sub">
                                            <li class="menu-icons__sub__item"><a class="menu-icon__sub-link" data-toggle="modal" data-target="#login" data-original-title="" title=""><span class="icon icon-lock_outline -ap"></span> Đăng nhập</a></li>
                                            <li class="menu-icons__sub__item"><a class="menu-icon__sub-link" data-toggle="modal" data-target="#register" data-original-title="" title=""><span class="icon icon-lock_open -ap"></span> Đăng ký</a></li>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <!---->
                    </div>
                    <!---->
                </div>
            </div>
            <div class="nav-toggle">
                <span class="fa fa-bars"></span>
            </div>
        </div>
    </div>
    <div class="header-menu">
        <div class="container">
            <div class="menu-category">
                <div class="catalog__list">
                    <ul class="catalog__wrap">
                        <li class="catalog__item active">
                            <a href="/" class="catalog__link">Trang chủ</a>
                        </li>
                        <li class="catalog__item"><a href="#" class="catalog__link"> Giới thiệu</a></li>
                        <li class="catalog__item -sub">
                            <a href="#"  class="catalog__link">Sản phẩm</a>
                            <div class="catalog__sub">
                                <div class="row">
                                    <div class="col-md-4 col-sm-5 col-xs-12">
                                        <ul class="list-cat__sub">
                                            <?php foreach ($category_sidebar as $item) { ?>
                                                <li class="catalog-sub__item"><a href="/product/product_category/<?= $item['id'] ?>"><?= $item['name'] ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="col-dm-8 col-sm-7 col-xs-12">
                                        <div class="catalog__title"><span>MỚI NHẤT HÔM NAY</span></div>
                                        <div class="catalog__product">
                                            <div class="product__image">
                                                <a href="#">
                                                    <img src="<?= $product_new['avatar'] ?>" alt="">
                                                </a>
                                                <div class="product__status -new">New </div>
                                            </div>
                                            <a href="/product/product_detail/<?= $product_new['id'] ?>" class="product__name"><?= $product_new['name'] ?></a>
                                            <div class="product__price">
                                                <?php if ($product_new['type_discount'] == 1) { ?>
                                                    <div class="product__price__old"><?= $product_new['price'] ?><span class="unit">đ</span></div>
                                                    <div class="product__price__regular"><?= $product_new['price'] - $product_new['discount'] ?> <span class="unit">đ</span></div>
                                                <?php } else { ?>
                                                    <div class="product__price__old"><?= $product_new['price'] ?><span class="unit">đ</span></div>
                                                    <div class="product__price__regular"><?= $product_new['price'] * (100 - $product_new['discount']) / 100 ?> <span class="unit">đ</span></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="catalog__item"><a href="#" class="catalog__link"> Tin tức</a></li>
                        <li class="catalog__item"><a href="/contact" class="catalog__link">Liên hệ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- End menu -->

<?php if (($step == 1)) { ?>
    <div class="sec sec__shopcart">

    <div class="container">
        <div class="nav-steps">
            <div class="nav-step__item waiting">
                <span class="number">1</span>
                <span class="txt">Giỏ hàng</span>
            </div>
            <div class="nav-step__item">
                <span class="number">2</span>
                <span class="txt">Nhập thông tin</span>
            </div>
            <div class="nav-step__item">
                <span class="number">3</span>
                <span class="txt">Xác nhận thông tin</span>
            </div>
            <div class="nav-step__item">
                <span class="number">4</span>
                <span class="txt">Hoàn thành</span>
            </div>
        </div>
        <div class="sub-name-step">Giỏ hàng của tôi</div>
        <div class="row">
            <div class="sec__shopcart__info col-md-8 col-sm-12 col-xs-12">
                <div class="box-user__info panel">
                    <div class="content-info-check">
                        <div class="table-product">
                            <div class="title">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <span class=""> 2 Sản phẩm</span>
                                    </div>
                                    <div class="col-md-2 col-sm-3 col-xs-3">
                                        <span class="textr">Đơn giá</span>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <span class="">Số lượng</span>
                                    </div>
                                </div>
                            </div>
                            <div class="content ">
                                <?php foreach ($list_product as $item) { ?>
                                    <div class="view-cart__product">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="img-product col-md-6 col-sm-6">
                                                    <img style="height: 100px;width: auto" src="<?= $item['avatar'] ?>" alt="Name your Product ">
                                                </div>
                                                <div class="sub__product col-md-6 col-sm-6">
                                                    <p class="product__name">
                                                        <?= $item['name'] ?>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-3 col-xs-3 hidden-mb">
                                                <p class="dongia"> <?= $item['price_disconut'] ?>₫</p>
                                            </div>
                                            <div class="col-md-3 col-sm-2 col-xs-2">
                                                <div class="product__quantity">
                                                    <button class="btn-minus btn">
                                                        -
                                                    </button>
                                                    <input class="input-number quantitty_number"
                                                           product_id="<?= $item['id'] ?>"
                                                           product_price="<?= $item['price_disconut'] ?>"
                                                           old_money="<?= $total_money ?>"
                                                           old_quantity="<?= $item['quantity'] ?>"
                                                           value="<?= $item['quantity'] ?>" type="text">
                                                    <button class="btn-plus btn">
                                                        +
                                                    </button>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-1 col-sm-1 col-xs-1">
                                                <a class="btn-delete delete_cart pointer" id_item="<?= $item['id'] ?>"><span class="icon -ap icon-trash2"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 col-price-total">
                <div class="name-top__step">Thông tin đơn hàng</div>
                <div class="">
                    <div class="price-total">
                        <div class="pull-left">Thành tiền :</div>
                        <div class="pull-right" id="total_money"><?= $total_money ?>₫</div>
                    </div>
                </div>
                <div class="list-button">
                    <a href="/shopping/shop_cart/<?= $step ?>" class="btn btn__next">Tiến hành thanh toán</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } elseif ($step == 2) { ?>
    <div class="sec sec__shopcart" xmlns="http://www.w3.org/1999/html">

    <div class="container">
        <div class="nav-steps">
            <div class="nav-step__item active">
                <span class="number">1</span>
                <span class="txt">Giỏ hàng</span>
            </div>
            <div class="nav-step__item waiting">
                <span class="number">2</span>
                <span class="txt">Nhập thông tin</span>
            </div>
            <div class="nav-step__item">
                <span class="number">3</span>
                <span class="txt">Xác nhận thông tin</span>
            </div>
            <div class="nav-step__item">
                <span class="number">4</span>
                <span class="txt">Hoàn thành</span>
            </div>
        </div>
        <div class="row">
            <form id="cart_2" action="/shopping/shop_cart/<?= $step ?>" method="post">
                <div class="sec__shopcart__info col-md-7 col-sm-7 col-xs-12">
                    <div class="box-user__info panel">
                        <div class="title">Thông tin thanh toán và nhận hàng</div>
                        <div class="content">
                            <div class="row">
                                <div class="col-md-6 col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <div class="label">Số điện thoại:</div>
                                        <input value="<?= $auth['phone'] ?>" name="order[phone]" type="text" class="form-control" placeholder="SĐT" required>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <div class="label">Họ tên:</div>
                                        <input value="<?= $auth['fullname'] ?>" name="order[fullname]" type="text" class="form-control" placeholder="Tên" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="label">Email</div>
                                <input value="<?= $auth['email'] ?>" type="email" name="order[email]" class="form-control" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <div class="label">Địa chỉ:</div>
                                <input type="text" class="form-control" name="order[address]" placeholder="Địa chỉ">
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <select name="order[province_id]" id="province_id" class="form-control selectpicker" data-live-search="true" style="width: 100%;" required>
                                        <option value="0" selected="selected">--Tỉnh/thành--</option>
                                        <?php foreach ($province as $item) { ?>
                                            <option value="<?= $item->city_id ?>"><?= $item->name ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="order[district_id]" id="district_id" class="form-control selectpicker" data-live-search="true" style="width: 100%;" required>
                                        <option value="0" selected="selected">--Quận/huyện--</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="order[ward_id]" id="ward_id" class="form-control selectpicker" data-live-search="true" style="width: 100%;" required>
                                        <option value="0" selected="selected" class="text-center">--Xã/phường--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="label">Ghi chú:</div>
                                <textarea name="order[note]" id="" cols="30" rows="10" class="form-control" placeholder="Nội dung"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                    <div class="title">Giỏ hàng</div>
                    <div class="content">
                        <div class="product__shopcart">
                            <div class="product__list">
                                <?php foreach ($list_product as $item) { ?>
                                    <div class="product__list__item">
                                        <div class="product__img">
                                            <img src="<?= $item['avatar'] ?>" alt="<?= $item['name'] ?>">
                                        </div>
                                        <div class="product__name">
                                            <a href="#"><?= $item['name'] ?></a>
                                            <a class="product__delete">
                                                <span class="icon -ap icon-trash2"></span>
                                            </a>
                                        </div>

                                        <div class="product__price">
                                            <?php if ($item['type_discount'] == 1) { ?>
                                                <div class="product__price__old"><?= $item['price'] ?><span class="unit">đ</span></div>
                                                <div class="product__price__regular"><?= $item['price'] - $item['discount'] ?> <span class="unit">đ</span></div>
                                            <?php } else { ?>
                                                <div class="product__price__old"><?= $item['price'] ?><span class="unit">đ</span></div>
                                                <div class="product__price__regular"><?= $item['price'] * (100 - $item['discount']) / 100 ?> <span class="unit">đ</span></div>
                                            <?php } ?>
                                            <div class="product__quantity">
                                                <button class="btn-minus btn">
                                                    -
                                                </button>
                                                <input class="input-number quantitty_number"
                                                       product_id="<?= $item['id'] ?>"
                                                       product_price="<?= $item['price_disconut'] ?>"
                                                       old_money="<?= $total_money ?>"
                                                       old_quantity="<?= $item['quantity'] ?>"
                                                       value="<?= $item['quantity'] ?>" type="text">
                                                <button class="btn-plus btn">
                                                    +
                                                </button>
                                            </div>

                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-price-total">
                        <div class="">
                            <div class="price-provi">
                                <div class="pull-left"><label class="checkbox-inline"><input id="vtp" step="<?= $step - 1 ?>" type="checkbox" value="1">Vận chuyển Viettelpost</label></div>
                            </div>
                            <div class="price-provi form-group vtp" style="display: none" >
                                <div class="col-md-4">
                                    Cửa hàng
                                </div>
                                <div class="col-md-8">
                                    <select id="store" name="order[store_id]" class="form-control">
                                        <?php foreach ($contact as $item) { ?>
                                            <option value="<?= $item['id'] ?>">
                                                <?= $item['name'] ?>--<?= $item['address'] ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="price-provi form-group vtp" style="display: none">
                                <div class="col-md-4">
                                    Dịch vụ chuyển phát
                                </div>
                                <div class="col-md-8">
                                    <select id="service" name="order[vtp_service]" class="form-control">
                                        <?php foreach ($vtp as $item) { ?>
                                            <option value="<?= $item['SERVICE_CODE'] ?>">
                                                <?= $item['SERVICE_NAME'] ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="price-provi form-group vtp" style="display: none" >
                                <div class="pull-left">
                                    <button class="btn btn-primary calculate_vtp" type="button">Tính phí vận chuyển</button>
                                    <input id="fee_transporter_input" class="hidden" name="order[vtp_fee]" value="0">
                                </div>
                                <div class="pull-right fee_transposter">₫</div>
                            </div>
                            <div class="price-total">
                                <div class="pull-left">Thành tiền :</div>
                                <div class="pull-right" id="total_money" total_money="<?= $total_money ?>"><?= $total_money ?>₫</div>
                            </div>
                        </div>
                    </div>
                    <div class="list-button">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <a class="btn btn__back">Quay lại</a>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <button type="submit" class="btn btn__next">Tiếp tục thanh toán</button>
                            </div>

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>

<script>
    $(document).ready(function () {
        jQuery.validator.addMethod("notEqualTo", function(v, e, p) {
            return this.optional(e) || v != p;
        }, "Thiếu thông tin địa chỉ");

        $("#cart_2").validate({
            rules : {
                'order[fullname]' : {
                    required : true
                },
                'order[phone]' : {
                    required : true,
                    digits: !0,
                    maxlength: 13,
                    minlength: 8
                },
                'order[email]':{
                    required:true
                },
                'order[province_id]' : {
                    required : true,
                    notEqualTo : '0'
                },
                'order[district_id]' : {
                    required : true,
                    notEqualTo : '0'
                },
                'order[ward_id]' : {
                    required : true,
                    notEqualTo : '0'
                }
            },
            messages : {
                'order[fullname]' : {
                    required : "Bạn vui lòng nhập họ tên."
                },
                'order[phone]' : {
                    required : "Bạn vui lòng nhập số điện thoại.",
                    digits: "Số điện thoại phải là số.",
                    minlength: "Số điện thoại tối thiểu phải có 8 số.",
                    maxlength: "Số điện thoại chỉ được 8 đến 14 số."
                },
                'order[email]':{
                    required:"Bạn vui lòng nhập email.",
                }
                ,
                'order[province_id]':{
                    required:"Bạn vui lòng chọn tỉnh thành.",
                    notEqualTo : 'Bạn vui lòng chọn tỉnh thành.'
                }
                ,
                'order[district_id]':{
                    required:"Bạn vui lòng chọn quận/huyện.",
                    notEqualTo : 'Bạn vui lòng chọn quận/huyện.'
                }
                ,
                'order[ward_id]':{
                    required:"Bạn vui lòng chọn xã/phường.",
                    notEqualTo : 'Bạn vui lòng chọn xã/phường.'
                }
            }
        })
    });
</script>
<?php } elseif ($step == 3) { ?>
    <style>

    ul.bankList {
        clear: both;
        height: 202px;
        width: 636px;
    }
    ul.bankList li {
        list-style-position: outside;
        list-style-type: none;
        cursor: pointer;
        float: left;
        margin-right: 0;
        padding: 5px 2px;
        text-align: center;
        width: 90px;
    }
    .list-content li {
        list-style: none outside none;
        margin: 0 0 10px;
    }

    .list-content li .boxContent {
        display: none;
        width: auto;
        border:1px solid #cccccc;
        padding:10px;
    }
    .list-content li.active .boxContent {
        display: block;
    }
    .list-content li .boxContent ul {
        height:auto;
    }

    i.VISA, i.MASTE, i.AMREX, i.JCB, i.VCB, i.TCB, i.MB, i.VIB, i.ICB, i.EXB, i.ACB, i.HDB, i.MSB, i.NVB, i.DAB, i.SHB, i.OJB, i.SEA, i.TPB, i.PGB, i.BIDV, i.AGB, i.SCB, i.VPB, i.VAB, i.GPB, i.SGB,i.NAB,i.BAB
    { width:80px; height:30px; display:block; background:url(https://www.nganluong.vn/webskins/skins/nganluong/checkout/version3/images/bank_logo.png) no-repeat;}
    i.MASTE { background-position:0px -31px}
    i.AMREX { background-position:0px -62px}
    i.JCB { background-position:0px -93px;}
    i.VCB { background-position:0px -124px;}
    i.TCB { background-position:0px -155px;}
    i.MB { background-position:0px -186px;}
    i.VIB { background-position:0px -217px;}
    i.ICB { background-position:0px -248px;}
    i.EXB { background-position:0px -279px;}
    i.ACB { background-position:0px -310px;}
    i.HDB { background-position:0px -341px;}
    i.MSB { background-position:0px -372px;}
    i.NVB { background-position:0px -403px;}
    i.DAB { background-position:0px -434px;}
    i.SHB { background-position:0px -465px;}
    i.OJB { background-position:0px -496px;}
    i.SEA { background-position:0px -527px;}
    i.TPB { background-position:0px -558px;}
    i.PGB { background-position:0px -589px;}
    i.BIDV { background-position:0px -620px;}
    i.AGB { background-position:0px -651px;}
    i.SCB { background-position:0px -682px;}
    i.VPB { background-position:0px -713px;}
    i.VAB { background-position:0px -744px;}
    i.GPB { background-position:0px -775px;}
    i.SGB { background-position:0px -806px;}
    i.NAB { background-position:0px -837px;}
    i.BAB { background-position:0px -868px;}

    ul.cardList li {
        cursor: pointer;
        float: left;
        margin-right: 0;
        padding: 5px 4px;
        text-align: center;
        width: 90px;
    }
</style>
<div class="sec sec__shopcart">
    <div class="container">
        <div class="nav-steps">
            <div class="nav-step__item active">
                <span class="number">1</span>
                <span class="txt">Giỏ hàng</span>
            </div>
            <div class="nav-step__item active">
                <span class="number">2</span>
                <span class="txt">Nhập thông tin</span>
            </div>
            <div class="nav-step__item waiting">
                <span class="number">3</span>
                <span class="txt">Xác nhận thông tin</span>
            </div>
            <div class="nav-step__item">
                <span class="number">4</span>
                <span class="txt">Hoàn thành</span>
            </div>
        </div>
        <div class="row">
            <form action="/shopping/shop_cart/<?= $step ?>" method="post">
                <div class="sec__shopcart__info col-md-7 col-sm-7 col-xs-12">
                    <div class="box-user__info panel">
                        <div class="title">Thông tin cá nhân</div>
                        <div class="content">
                            <div class="row form-group">
                                <label class="control-label col-md-3 col-xs-3 col-sm-3">Họ tên: </label>
                                <div class="name col-md-9 col-xs-9 col-sm-9"><?= $order_info['fullname'] ?></div>
                            </div>
                            <div class="row form-group">
                                <label class="control-label col-md-3 col-xs-3 col-sm-3">Số điện thoại: </label>
                                <div class="name col-md-9 col-xs-9 col-sm-9"><?= $order_info['phone'] ?></div>
                            </div>
                            <div class="row form-group">
                                <label class="control-label col-md-3 col-xs-3 col-sm-3">Email: </label>
                                <div class="name col-md-9 col-xs-9 col-sm-9"><?= $order_info['email'] ?></div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-3 col-sm-3">Địa chỉ:</label>
                                <div class="name col-md-9 col-xs-9 col-sm-9"><?= $order_info['address_info'] ?></div>
                            </div>
                        </div>
                    </div>
                    <div class="box-user__info panel">
                        <div class="title">Hình thức thanh toán</div>
                        <div class="content">
                            <div class="list-payment">
                                <div>
                                    <label class="checkbox-inline"><input id="ngan_luong" type="checkbox" value="">
                                        Thanh toán qua cổng thanh toán trực tiếp ngân lượng</label>
                                </div>
                                <div style="display: none;margin-left: 10px" id="nlpayment">
                                    <ul class="list-content">
                                        <li class="active">
                                            <label><input type="radio" value="NL" name="option_payment" selected="true">Thanh
                                                toán bằng Ví điện tử NgânLượng</label>
                                            <div class="boxContent">
                                                <p>
                                                    Thanh toán trực tuyến AN TOÀN và ĐƯỢC BẢO VỆ, sử dụng thẻ ngân hàng
                                                    trong và ngoài nước hoặc nhiều hình thức tiện lợi khác.
                                                    Được bảo hộ & cấp phép bởi NGÂN HÀNG NHÀ NƯỚC, ví điện tử duy nhất
                                                    được cộng đồng ƯA THÍCH NHẤT 2 năm liên tiếp, Bộ Thông tin Truyền
                                                    thông trao giải thưởng Sao Khuê
                                                    <br/>Giao dịch. Đăng ký ví NgânLượng.vn miễn phí <a
                                                            href="https://www.nganluong.vn/?portal=nganluong&amp;page=user_register"
                                                            target="_blank">tại đây</a></p>
                                            </div>
                                        </li>
                                        <li>
                                            <label><input type="radio" value="ATM_ONLINE" name="option_payment">Thanh
                                                toán online bằng thẻ ngân hàng nội địa</label>
                                            <div class="boxContent">
                                                <p><i>
                                                        <span style="color:#ff5a00;font-weight:bold;text-decoration:underline;">Lưu ý</span>:
                                                        Bạn cần đăng ký Internet-Banking hoặc dịch vụ thanh toán trực
                                                        tuyến tại ngân hàng trước khi thực hiện.</i></p>

                                                <ul class="cardList clearfix">
                                                    <li class="bank-online-methods ">
                                                        <label for="vcb_ck_on">
                                                            <i class="BIDV"
                                                               title="Ngân hàng TMCP Đầu tư &amp; Phát triển Việt Nam"></i>
                                                            <input type="radio" value="BIDV" name="bankcode">

                                                        </label></li>
                                                    <li class="bank-online-methods ">
                                                        <label for="vcb_ck_on">
                                                            <i class="VCB"
                                                               title="Ngân hàng TMCP Ngoại Thương Việt Nam"></i>
                                                            <input type="radio" value="VCB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="vnbc_ck_on">
                                                            <i class="DAB" title="Ngân hàng Đông Á"></i>
                                                            <input type="radio" value="DAB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="tcb_ck_on">
                                                            <i class="TCB" title="Ngân hàng Kỹ Thương"></i>
                                                            <input type="radio" value="TCB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_mb_ck_on">
                                                            <i class="MB" title="Ngân hàng Quân Đội"></i>
                                                            <input type="radio" value="MB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_vib_ck_on">
                                                            <i class="VIB" title="Ngân hàng Quốc tế"></i>
                                                            <input type="radio" value="VIB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_vtb_ck_on">
                                                            <i class="ICB" title="Ngân hàng Công Thương Việt Nam"></i>
                                                            <input type="radio" value="ICB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_exb_ck_on">
                                                            <i class="EXB" title="Ngân hàng Xuất Nhập Khẩu"></i>
                                                            <input type="radio" value="EXB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_acb_ck_on">
                                                            <i class="ACB" title="Ngân hàng Á Châu"></i>
                                                            <input type="radio" value="ACB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_hdb_ck_on">
                                                            <i class="HDB" title="Ngân hàng Phát triển Nhà TPHCM"></i>
                                                            <input type="radio" value="HDB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_msb_ck_on">
                                                            <i class="MSB" title="Ngân hàng Hàng Hải"></i>
                                                            <input type="radio" value="MSB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_nvb_ck_on">
                                                            <i class="NVB" title="Ngân hàng Nam Việt"></i>
                                                            <input type="radio" value="NVB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_vab_ck_on">
                                                            <i class="VAB" title="Ngân hàng Việt Á"></i>
                                                            <input type="radio" value="VAB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_vpb_ck_on">
                                                            <i class="VPB" title="Ngân Hàng Việt Nam Thịnh Vượng"></i>
                                                            <input type="radio" value="VPB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_scb_ck_on">
                                                            <i class="SCB" title="Ngân hàng Sài Gòn Thương tín"></i>
                                                            <input type="radio" value="SCB" name="bankcode">

                                                        </label></li>


                                                    <li class="bank-online-methods ">
                                                        <label for="bnt_atm_pgb_ck_on">
                                                            <i class="PGB" title="Ngân hàng Xăng dầu Petrolimex"></i>
                                                            <input type="radio" value="PGB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="bnt_atm_gpb_ck_on">
                                                            <i class="GPB" title="Ngân hàng TMCP Dầu khí Toàn Cầu"></i>
                                                            <input type="radio" value="GPB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="bnt_atm_agb_ck_on">
                                                            <i class="AGB"
                                                               title="Ngân hàng Nông nghiệp &amp; Phát triển nông thôn"></i>
                                                            <input type="radio" value="AGB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="bnt_atm_sgb_ck_on">
                                                            <i class="SGB" title="Ngân hàng Sài Gòn Công Thương"></i>
                                                            <input type="radio" value="SGB" name="bankcode">

                                                        </label></li>
                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_bab_ck_on">
                                                            <i class="BAB" title="Ngân hàng Bắc Á"></i>
                                                            <input type="radio" value="BAB" name="bankcode">

                                                        </label></li>
                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_bab_ck_on">
                                                            <i class="TPB" title="Tền phong bank"></i>
                                                            <input type="radio" value="TPB" name="bankcode">

                                                        </label></li>
                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_bab_ck_on">
                                                            <i class="NAB" title="Ngân hàng Nam Á"></i>
                                                            <input type="radio" value="NAB" name="bankcode">

                                                        </label></li>
                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_bab_ck_on">
                                                            <i class="SHB"
                                                               title="Ngân hàng TMCP Sài Gòn - Hà Nội (SHB)"></i>
                                                            <input type="radio" value="SHB" name="bankcode">

                                                        </label></li>
                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_bab_ck_on">
                                                            <i class="OJB"
                                                               title="Ngân hàng TMCP Đại Dương (OceanBank)"></i>
                                                            <input type="radio" value="OJB" name="bankcode">

                                                        </label></li>


                                                </ul>

                                            </div>
                                        </li>
                                        <li>
                                            <label><input type="radio" value="IB_ONLINE" name="option_payment">Thanh
                                                toán bằng IB</label>
                                            <div class="boxContent">
                                                <p><i>
                                                        <span style="color:#ff5a00;font-weight:bold;text-decoration:underline;">Lưu ý</span>:
                                                        Bạn cần đăng ký Internet-Banking hoặc dịch vụ thanh toán trực
                                                        tuyến tại ngân hàng trước khi thực hiện.</i></p>

                                                <ul class="cardList clearfix">
                                                    <li class="bank-online-methods ">
                                                        <label for="vcb_ck_on">
                                                            <i class="BIDV"
                                                               title="Ngân hàng TMCP Đầu tư &amp; Phát triển Việt Nam"></i>
                                                            <input type="radio" value="BIDV" name="bankcode">

                                                        </label></li>
                                                    <li class="bank-online-methods ">
                                                        <label for="vcb_ck_on">
                                                            <i class="VCB"
                                                               title="Ngân hàng TMCP Ngoại Thương Việt Nam"></i>
                                                            <input type="radio" value="VCB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="vnbc_ck_on">
                                                            <i class="DAB" title="Ngân hàng Đông Á"></i>
                                                            <input type="radio" value="DAB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="tcb_ck_on">
                                                            <i class="TCB" title="Ngân hàng Kỹ Thương"></i>
                                                            <input type="radio" value="TCB" name="bankcode">

                                                        </label></li>


                                                </ul>

                                            </div>
                                        </li>
                                        <li>
                                            <label><input type="radio" value="ATM_OFFLINE" name="option_payment">Thanh
                                                toán atm offline</label>
                                            <div class="boxContent">

                                                <ul class="cardList clearfix">
                                                    <li class="bank-online-methods ">
                                                        <label for="vcb_ck_on">
                                                            <i class="BIDV"
                                                               title="Ngân hàng TMCP Đầu tư &amp; Phát triển Việt Nam"></i>
                                                            <input type="radio" value="BIDV" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="vcb_ck_on">
                                                            <i class="VCB"
                                                               title="Ngân hàng TMCP Ngoại Thương Việt Nam"></i>
                                                            <input type="radio" value="VCB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="vnbc_ck_on">
                                                            <i class="DAB" title="Ngân hàng Đông Á"></i>
                                                            <input type="radio" value="DAB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="tcb_ck_on">
                                                            <i class="TCB" title="Ngân hàng Kỹ Thương"></i>
                                                            <input type="radio" value="TCB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_mb_ck_on">
                                                            <i class="MB" title="Ngân hàng Quân Đội"></i>
                                                            <input type="radio" value="MB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_vtb_ck_on">
                                                            <i class="ICB" title="Ngân hàng Công Thương Việt Nam"></i>
                                                            <input type="radio" value="ICB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_acb_ck_on">
                                                            <i class="ACB" title="Ngân hàng Á Châu"></i>
                                                            <input type="radio" value="ACB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_msb_ck_on">
                                                            <i class="MSB" title="Ngân hàng Hàng Hải"></i>
                                                            <input type="radio" value="MSB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_scb_ck_on">
                                                            <i class="SCB" title="Ngân hàng Sài Gòn Thương tín"></i>
                                                            <input type="radio" value="SCB" name="bankcode">

                                                        </label></li>
                                                    <li class="bank-online-methods ">
                                                        <label for="bnt_atm_pgb_ck_on">
                                                            <i class="PGB" title="Ngân hàng Xăng dầu Petrolimex"></i>
                                                            <input type="radio" value="PGB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="bnt_atm_agb_ck_on">
                                                            <i class="AGB"
                                                               title="Ngân hàng Nông nghiệp &amp; Phát triển nông thôn"></i>
                                                            <input type="radio" value="AGB" name="bankcode">

                                                        </label></li>
                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_bab_ck_on">
                                                            <i class="SHB"
                                                               title="Ngân hàng TMCP Sài Gòn - Hà Nội (SHB)"></i>
                                                            <input type="radio" value="SHB" name="bankcode">

                                                        </label></li>


                                                </ul>

                                            </div>
                                        </li>
                                        <li>
                                            <label><input type="radio" value="NH_OFFLINE" name="option_payment">Thanh
                                                toán tại văn phòng ngân hàng</label>
                                            <div class="boxContent">

                                                <ul class="cardList clearfix">
                                                    <li class="bank-online-methods ">
                                                        <label for="vcb_ck_on">
                                                            <i class="BIDV"
                                                               title="Ngân hàng TMCP Đầu tư &amp; Phát triển Việt Nam"></i>
                                                            <input type="radio" value="BIDV" name="bankcode">

                                                        </label></li>
                                                    <li class="bank-online-methods ">
                                                        <label for="vcb_ck_on">
                                                            <i class="VCB"
                                                               title="Ngân hàng TMCP Ngoại Thương Việt Nam"></i>
                                                            <input type="radio" value="VCB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="vnbc_ck_on">
                                                            <i class="DAB" title="Ngân hàng Đông Á"></i>
                                                            <input type="radio" value="DAB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="tcb_ck_on">
                                                            <i class="TCB" title="Ngân hàng Kỹ Thương"></i>
                                                            <input type="radio" value="TCB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_mb_ck_on">
                                                            <i class="MB" title="Ngân hàng Quân Đội"></i>
                                                            <input type="radio" value="MB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_vib_ck_on">
                                                            <i class="VIB" title="Ngân hàng Quốc tế"></i>
                                                            <input type="radio" value="VIB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_vtb_ck_on">
                                                            <i class="ICB" title="Ngân hàng Công Thương Việt Nam"></i>
                                                            <input type="radio" value="ICB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_acb_ck_on">
                                                            <i class="ACB" title="Ngân hàng Á Châu"></i>
                                                            <input type="radio" value="ACB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_msb_ck_on">
                                                            <i class="MSB" title="Ngân hàng Hàng Hải"></i>
                                                            <input type="radio" value="MSB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_scb_ck_on">
                                                            <i class="SCB" title="Ngân hàng Sài Gòn Thương tín"></i>
                                                            <input type="radio" value="SCB" name="bankcode">

                                                        </label></li>


                                                    <li class="bank-online-methods ">
                                                        <label for="bnt_atm_pgb_ck_on">
                                                            <i class="PGB" title="Ngân hàng Xăng dầu Petrolimex"></i>
                                                            <input type="radio" value="PGB" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="bnt_atm_agb_ck_on">
                                                            <i class="AGB"
                                                               title="Ngân hàng Nông nghiệp &amp; Phát triển nông thôn"></i>
                                                            <input type="radio" value="AGB" name="bankcode">

                                                        </label></li>
                                                    <li class="bank-online-methods ">
                                                        <label for="sml_atm_bab_ck_on">
                                                            <i class="TPB" title="Tền phong bank"></i>
                                                            <input type="radio" value="TPB" name="bankcode">

                                                        </label></li>


                                                </ul>

                                            </div>
                                        </li>
                                        <li>
                                            <label><input type="radio" value="VISA" name="option_payment"
                                                          selected="true">Thanh toán bằng thẻ Visa hoặc
                                                MasterCard</label>
                                            <div class="boxContent">
                                                <p>
                                                    <span style="color:#ff5a00;font-weight:bold;text-decoration:underline;">Lưu ý</span>:Visa
                                                    hoặc MasterCard.</p>
                                                <ul class="cardList clearfix">
                                                    <li class="bank-online-methods ">
                                                        <label for="vcb_ck_on">
                                                            Visa:
                                                            <input type="radio" value="VISA" name="bankcode">

                                                        </label></li>

                                                    <li class="bank-online-methods ">
                                                        <label for="vnbc_ck_on">
                                                            Master:<input type="radio" value="MASTER" name="bankcode">
                                                        </label></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li>
                                            <label><input type="radio" value="CREDIT_CARD_PREPAID" name="option_payment"
                                                          selected="true">Thanh toán bằng thẻ Visa hoặc MasterCard trả
                                                trước</label>

                                        </li>
                                    </ul>

                                    <table style="clear:both;width:500px;padding-left:46px;">

                                        <tr>
                                            <td></td>
                                            <td>
                                                <input type="submit" name="nlpayment" value="thanh toán"/>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                    <div class="title">Giỏ hàng</div>
                    <div class="content">
                        <div class="product__shopcart">
                            <div class="product__list">
                                <?php foreach ($list_product as $item) { ?>
                                    <div class="product__list__item">
                                        <div class="product__img">
                                            <img src="<?= $item['avatar'] ?>" alt="<?= $item['name'] ?> ">
                                        </div>
                                        <div class="product__name">
                                            <?= $item['name'] ?>
                                        </div>

                                        <div class="product__price">
                                            <?php if ($item['type_discount'] == 1) { ?>
                                                <div class="product__price__old"><?= $item['price'] ?><span
                                                            class="unit">đ</span></div>
                                                <div class="product__price__regular"><?= $item['price'] - $item['discount'] ?>
                                                    <span class="unit">đ</span></div>
                                            <?php } else { ?>
                                                <div class="product__price__old"><?= $item['price'] ?><span
                                                            class="unit">đ</span></div>
                                                <div class="product__price__regular"><?= $item['price'] * (100 - $item['discount']) / 100 ?>
                                                    <span class="unit">đ</span></div>
                                            <?php } ?>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="list-button">
                        <button class="btn btn__next" type="submit">Hoàn thành</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="https://www.nganluong.vn/webskins/javascripts/jquery_min.js" type="text/javascript"></script>
<script language="javascript">
    $('input[name="option_payment"]').bind('click', function() {
        $('.list-content li').removeClass('active');
        $(this).parent().parent('li').addClass('active');
    });
</script>
<?php } else { ?>
    <div class="sec sec__shopcart">

    <div class="container">
        <div class="nav-steps">
            <div class="nav-step__item active">
                <span class="number">1</span>
                <span class="txt">Giỏ hàng</span>
            </div>
            <div class="nav-step__item active">
                <span class="number">2</span>
                <span class="txt">Nhập thông tin</span>
            </div>
            <div class="nav-step__item active">
                <span class="number">3</span>
                <span class="txt">Xác nhận thông tin</span>
            </div>
            <div class="nav-step__item">
                <span class="number active">4</span>
                <span class="txt text-success">Hoàn thành -- Mã đơn hàng : <?= $order['order_code'] ?></span>
            </div>
        </div>

        <div class="alert alert-success">
            <span class="-ap icon-info-with-circle"></span>
            Link thanh toán Ngân lượng
            <a href="<?= $order['url_nl'] ?>" class="alert-link" target="_blank"><?= $order['url_nl'] ?></a>
        </div>
        <div class="alert alert-success">
            <span class="-ap icon-info-with-circle"></span>
            Bạn đã mua hàng thành công
            <a href="/index" class="alert-link">Quay lại trang chủ để tiếp tục mua hàng</a>
        </div>
    </div>
</div>
<?php } ?>

<footer class="footer">
    <!-- End topfoter -->
    <!-- footer -->
    <div class="container">
        <div class="footer__box">
            <div class="row ">
                <div class="col-md-4  col-sm-4 col-xs-12">
                    <h3 class="footer__title ">Trang Bán Đồ Gỗ</h3>
                    <div class="list-menu-ft">
                        <ul>
                            <li><a href=""><i class="fa fa-check" aria-hidden="true"></i> Trang chủ</a></li>
                            <li><a href=""><i class="fa fa-check" aria-hidden="true"></i> Giới thiệu</a></li>
                            <li><a href=""><i class="fa fa-check" aria-hidden="true"></i> Sản phẩm</a></li>
                            <li><a href=""><i class="fa fa-check" aria-hidden="true"></i> Tin tức</a></li>
                        </ul>
                    </div>
                    <h3 class="footer__title ">Đăng ký nhận tin tức</h3>
                    <div class="list-menu-ft">
                        <p class="note-sbmail">Theo dõi bản tin của chungst ôi với những tin tức mới nhất và chác chương trình khuyến mại</p>
                        <div class="sub frm-regemail">
                            <input type="text" class="form-control" placeholder="Nhập email của bạn">
                            <button class="btn btn-regemail">Đăng ký</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h3 class="footer__title ">Tin tức</h3>
                    <div class="list-news-ft">
                        <ul>
                            <li>
                                <div class="avt">
                                    <a href=""><img src="/frontend_res/assets/uploads/news.jpg" alt="name your product"></a>
                                </div>
                                <div class="ct">
                                    <p class="name"><a href="">Các ý tưởng thiết kế ban công</a></p>
                                    <p class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 12/7/2017</p>
                                </div>
                            </li>
                            <li>
                                <div class="avt">
                                    <a href=""><img src="/frontend_res/assets/uploads/news.jpg" alt="name your product"></a>
                                </div>
                                <div class="ct">
                                    <p class="name"><a href="">Các ý tưởng thiết kế ban công</a></p>
                                    <p class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 12/7/2017</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4  col-sm-4 col-xs-12">
                    <h3 class="footer__title ">Liên hệ</h3>
                    <?php foreach ($contact as $item) { ?>
                        <div class="contact">
                            <p class="name-comp"><?= $item['name'] ?></p>
                            <p class="info ">
                                <span class="icon -ap icon-location3"></span>
                                <span class="name-header texU disb fow">Địa chỉ: </span>
                                <span class="disb"><?= $item['address'] ?></span>
                            </p>
                        </div>
                        <div class="contact">
                            <p class="info ">
                                <span class="icon -ap  icon-envelope2"></span>
                                <span class="name-header texU disb fow">Email:</span>
                                <span class="disb"><?= $item['email'] ?></span>
                            </p>
                        </div>
                        <div class="contact">
                            <p class="info ">
                                <span class="icon fa fa-phone"></span>
                                <span class="name-header texU disb fow"> Phone: </span>
                                <span class="disb"><?= $item['phone'] ?></span>
                            </p>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <!-- End footer -->
    <!-- Copyright -->
    <div class="bottom">
        <div class="container">
            <div class="pull-left">
                <div class="copyright">
                    Copyright ® 2017 <a href="" class="url">ABC</a>
                </div>
            </div>
        </div>
    </div>
    <!-- end copyright -->

</footer>