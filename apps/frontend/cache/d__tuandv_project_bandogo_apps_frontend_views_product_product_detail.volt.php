<header class="header">
    <div class="banner">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="logo">
                        <img src="/frontend_res/assets/uploads/logo.png" alt="logo"/>
                    </div>
                </div>
                <!--<div class="col-md-5 col-sm-5 col-xs-12">-->
                <!--<div class="center-bn">-->
                <!--<a><span class="red">Hotline :</span> (+84) 0123465789</a>-->
                <!--|-->
                <!--<a href="#">Tìm cửa hàng trên toàn quốc</a>-->
                <!--</div>-->
                <!--</div>-->
                <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                <!--<div class="box-search">-->
                <!--<div class="form-input">-->
                <!--<input type="text" class="form-control" placeholder="Search...">-->
                <!--<button class="btn btn__search"><span class="fa fa-search"></span></button>-->
                <!--</div>-->
                <!--</div>-->
                <!--</div>-->
                <div class="col-md-9 col-sm-9 col-xs-12 col-right-headtop">
                    <div class="pull-left">
                        <div class="box-search">
                            <div class="form-input">
                                <input type="text" class="form-control" placeholder="Tìm kiếm...">
                                <button class="btn btn__search"><span class="fa fa-search"></span></button>
                            </div>
                        </div>
                    </div>
                    <div class="menu-top pull-right">
                        <div class="col-menu-top">
                            <div class="hotline"><span class="-ap icon-phone-handset"></span> Hotline: <?= $contact[0]['hotline'] ?></div>
                            <div class="header-top__shopcart">
                                <div class="icon">
                                    <span class="-ap icon-cart"></span>
                                </div>
                                <div class="headline">Giỏ hàng <span class="number-cart"><?= $total_product ?></span></div>
                                <div class="dropdown__shop__cart">
                                    <div class="dropdown__head">
                                        <span class="number"><?= $total_product ?></span>
                                        <span class="txt">Sản phẩm trong giỏ hàng</span>
                                    </div>
                                    <div class="product__shopcart">
                                        <div class="product__list">
                                            <?php foreach ($cart_product as $item) { ?>
                                                <div class="product__list__item">
                                                    <div class="product__img">
                                                        <img src="<?= $item['avatar'] ?>" alt="Name your Product ">
                                                    </div>
                                                    <div class="product__name">
                                                        <?= $item['name'] ?>
                                                        <a class="product__delete">
                                                            <span class="icon -ap icon-trash2"></span>
                                                        </a>
                                                    </div>

                                                    <div class="product__price">
                                                        <?php if ($item['type_discount'] == 1) { ?>
                                                            <div class="product__price__old"><?= $item['price'] ?><span class="unit">đ</span></div>
                                                            <div class="product__price__regular"><?= $item['price'] - $item['discount'] ?> <span class="unit">đ</span></div>
                                                        <?php } else { ?>
                                                            <div class="product__price__old"><?= $item['price'] ?><span class="unit">đ</span></div>
                                                            <div class="product__price__regular"><?= $item['price'] * (100 - $item['discount']) / 100 ?> <span class="unit">đ</span></div>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <form method="post">
                                        <div class="dropdown__bottom">
                                            
                                            <div class="total">Tổng cộng: <span class="product__price number"><?= $total_money ?> <span class="unit"></span>đ</span></div>
                                            <a href="/shopping/shop_cart" class="btn__checkout">Tiến hành đặt hàng</a>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <?php if ($auth) { ?>
                                <div class="header-top__login">
                                    <div class="acc-mobi">
                                        <div class="title"><?= ($auth['fullname'] ? $auth['fullname'] : $auth['email']) ?></div>
                                        <ul class="menu-icons__sub">
                                            <li class="menu-icons__sub__item"><a class="menu-icon__sub-link" data-toggle="modal" data-target="#login" data-original-title="" title=""><span class="fa fa-user"></span> Thông tin tài khoản</a></li>
                                            <li class="menu-icons__sub__item">
                                                <a href="/" class="menu-icon__sub-link" title="">
                                                    <span class="fa fa-unlock-alt"></span> Thoát
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="header-top__login">
                                    <div class="acc-mobi">
                                        <div class="title">Tài khoản</div>
                                        <ul class="menu-icons__sub">
                                            <li class="menu-icons__sub__item"><a class="menu-icon__sub-link" data-toggle="modal" data-target="#login" data-original-title="" title=""><span class="icon icon-lock_outline -ap"></span> Đăng nhập</a></li>
                                            <li class="menu-icons__sub__item"><a class="menu-icon__sub-link" data-toggle="modal" data-target="#register" data-original-title="" title=""><span class="icon icon-lock_open -ap"></span> Đăng ký</a></li>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <!---->
                    </div>
                    <!---->
                </div>
            </div>
            <div class="nav-toggle">
                <span class="fa fa-bars"></span>
            </div>
        </div>
    </div>
    <div class="header-menu">
        <div class="container">
            <div class="menu-category">
                <div class="catalog__list">
                    <ul class="catalog__wrap">
                        <li class="catalog__item active">
                            <a href="/" class="catalog__link">Trang chủ</a>
                        </li>
                        <li class="catalog__item"><a href="#" class="catalog__link"> Giới thiệu</a></li>
                        <li class="catalog__item -sub">
                            <a href="#"  class="catalog__link">Sản phẩm</a>
                            <div class="catalog__sub">
                                <div class="row">
                                    <div class="col-md-4 col-sm-5 col-xs-12">
                                        <ul class="list-cat__sub">
                                            <?php foreach ($category_sidebar as $item) { ?>
                                                <li class="catalog-sub__item"><a href="/product/product_category/<?= $item['id'] ?>"><?= $item['name'] ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="col-dm-8 col-sm-7 col-xs-12">
                                        <div class="catalog__title"><span>MỚI NHẤT HÔM NAY</span></div>
                                        <div class="catalog__product">
                                            <div class="product__image">
                                                <a href="#">
                                                    <img src="<?= $product_new['avatar'] ?>" alt="">
                                                </a>
                                                <div class="product__status -new">New </div>
                                            </div>
                                            <a href="/product/product_detail/<?= $product_new['id'] ?>" class="product__name"><?= $product_new['name'] ?></a>
                                            <div class="product__price">
                                                <?php if ($product_new['type_discount'] == 1) { ?>
                                                    <div class="product__price__old"><?= $product_new['price'] ?><span class="unit">đ</span></div>
                                                    <div class="product__price__regular"><?= $product_new['price'] - $product_new['discount'] ?> <span class="unit">đ</span></div>
                                                <?php } else { ?>
                                                    <div class="product__price__old"><?= $product_new['price'] ?><span class="unit">đ</span></div>
                                                    <div class="product__price__regular"><?= $product_new['price'] * (100 - $product_new['discount']) / 100 ?> <span class="unit">đ</span></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="catalog__item"><a href="#" class="catalog__link"> Tin tức</a></li>
                        <li class="catalog__item"><a href="/contact" class="catalog__link">Liên hệ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- End menu -->
<div class="nav-anchor"></div>
<div class="hero-banner">
    <div class="container">
        <div class="slider-wrapper theme-default">
            <div class="slider apollo_slides nivoSlider">
                <a href="#"><img src="/frontend_res/assets/uploads/slide.jpg"/></a>
                <a href="#"><img src="/frontend_res/assets/uploads/slider2.png"/></a>
            </div>
        </div>
    </div>
</div>
<div class="sec list-product">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12 col-left">
                <div class="box category">
    <div class="title-name">
        Danh mục sản phẩm
    </div>
    <div class="list">
        <ul>
            <?php foreach ($category_sidebar as $item) { ?>
                <li><a href="/product/product_category/<?= $item['id'] ?>"><?= $item['name'] ?></a></li>
            <?php } ?>
        </ul>
    </div>
</div>
            </div>
            <!--/-->
            <div class="col-md-9 col-sm-9 col-xs-12 col-right">
                <!---->
                <div class="box-category">
                    <div class="ct-detail-pr">
                        <div class="row ct-detail-top">
                            <div class="product__view__image col-md-6 col-sm-6 col-xs-12">
                                <div class="row">
                                    <div class="col-sm-9 col-xs-12">
                                        <div class="product__view__image--list">
                                            <a href="<?= $product['avatar'] ?>" data-fancybox="gallery" class="fancybox">
                                                <img src="<?= $product['avatar'] ?>" class="elevate__zoom" data-zoom-image="<?= $product['avatar'] ?>" id="elevate__zoom"  title="product-2">
                                            </a>
                                            <div class="hidden">
                                                <?php foreach ($product['slide_show'] as $item) { ?>
                                                    <a href="<?= $item ?>" data-fancybox="gallery" class="fancybox">
                                                        <img src="<?= $item ?>" alt="">
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="product__view__image--thumb" id="image-additional-carousel">
                                            <div class="item">
                                                <?php foreach ($product['slide_show'] as $item) { ?>
                                                    <a href="<?= $item ?>" data-fancybox="gallery" class="fancybox">
                                                        <img src="<?= $item ?>" alt="">
                                                    </a>
                                                <?php } ?>
                                            </div>
                                            <div class="item">
                                                <?php foreach ($product['slide_show'] as $item) { ?>
                                                    <a href="<?= $item ?>" data-fancybox="gallery" class="fancybox">
                                                        <img src="<?= $item ?>" alt="">
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!---->
                            <div class="col-md-6 col-sm-6 col-xs-12 product__view__content">
                                <div class="name text-primary"><?= $product['name'] ?></div>
                                <div class="product__price product__price__view">
                                    <span class="product__price--txt text-primary">Giá bán</span>
                                    <?php if ($product['type_discount'] == 1) { ?>
                                        <div class="product__price__old"><?= $product['price'] ?><span class="unit">đ</span></div>
                                        <div class="product__price__regular"><?= $product['price'] - $product['discount'] ?> <span class="unit">đ</span></div>
                                    <?php } else { ?>
                                        <div class="product__price__old"><?= $product['price'] ?><span class="unit">đ</span></div>
                                        <div class="product__price__regular"><?= $product['price'] * (100 - $product['discount']) / 100 ?> <span class="unit">đ</span></div>
                                    <?php } ?>
                                </div>
                                <div class="product__view__info">
                                    <p class="info text-primary">Tình trạng: <span class="name"><?= ($product['quantity'] > 0 ? 'Còn hàng' : 'Liên hệ') ?></span></p>
                                </div>
                                <div class="product__view__info">
                                    <p class="info text-primary">Mô tả: <span><?= $product['desc'] ?></span></p>
                                </div>
                                <div class="product__view__des">
                                    <a id="add_cart" product_id="<?= $product['id'] ?>"
                                       product_price="<?= ($product['type_discount'] == 1 ? $product['price'] - $product['discount'] : $product['price'] * (100 - $product['discount']) / 100) ?>"
                                       class="product__view__button__cart pointer add_to_cart"><span
                                                class="icon icon fa fa-shopping-basket"></span>Thêm vào giỏ</a>
                                    <a class="product__view__button__cart pointer"><span class="icon fa fa-shopping-bag"></span>Mua ngay</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product__detail__content">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#home">Tổng quan</a></li>
                        </ul>

                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <article class="article">
                                    <div class="std">
                                        Cây Ngọc Ngân thủy sinh có tên khoa học Dieffenbachia picta, thuộc họ thực vật Araceae (Ráy) hay còn có tên gọi khác là cây Valentine. Cây Ngọc Ngân có lá khá nổi bật, vì tính tương phản giữa màu xanh thẫm của lá và màu trắng phần giữa lá, nó khiến người xem bị thu hút và thích thú ngay từ cái nhìn đầu tiên. Đặc biệt hơn nữa cây thủy sinh với chiếc bình thủy tinh giúp người xem thấy rõ bộ rễ khiến cây càng trở nên lộng lẫy. Cây Ngọc Ngân thủy sinh phù hợp để là cây cảnh nội thất, trang trí quán cà phê, văn phòng, bàn lễ tân…


                                        <p><img src="<?= $product['avatar'] ?>"></p>

                                        <p><strong>Ý NGHĨA PHONG THỦY CỦA CÂY NGỌC NGÂN THỦY SINH</strong></p>

                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                    <!---->
                </div>
                <!---->
                <div class="box-ct">
                    <div class="title-name">
                        Sản phẩm ưa chuộng
                    </div>
                    <div class="list-ct">
                        <div class="row">
                            <?php foreach ($product_popularity as $item) { ?>
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="product__item">
                                        <div class="w">
                                            <div class="product__image">
                                                <a href="#" class="product__link"><img src="<?= $item['avatar'] ?>" alt="name your product"></a>
                                                <div class="product-item__actions">
                                                    <a class="btn btn-detail" data-toggle="tooltip" data-placement="top" href="/product/product_detail/<?= $item['id'] ?>" title="Chi tiết"><span class="-ap  icon-search2"></span></a>
                                                    <a class="btn btn-quickview" id="quick_view" product="<?= $item['id'] ?>" data-toggle="modal" data-target="#QuickView" data-placement="top" title="Xem nhanh"><span class="fa fa-eye"></span></a>
                                                </div>
                                                <div class="product__overlay"></div>
                                                <?php if ($item['type_discount'] == 2) { ?>
                                                    <div class="product__sale"><?= $item['discount'] ?>%</div>
                                                <?php } ?>
                                            </div>
                                            <h3 class="product__name"><a href="#"><?= $item['name'] ?></a></h3>
                                            <div class="product__price">
                                                <?php if ($item['type_discount'] == 1) { ?>
                                                    <div class="product__price__old"><?= $item['price'] ?><span class="unit">đ</span></div>
                                                    <div class="product__price__regular"><?= $item['price'] - $item['discount'] ?> <span class="unit">đ</span></div>
                                                <?php } else { ?>
                                                    <div class="product__price__old"><?= $item['price'] ?><span class="unit">đ</span></div>
                                                    <div class="product__price__regular"><?= $item['price'] * (100 - $item['discount']) / 100 ?> <span class="unit">đ</span></div>
                                                <?php } ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- footer -->
<footer class="footer">
    <!-- End topfoter -->
    <!-- footer -->
    <div class="container">
        <div class="footer__box">
            <div class="row ">
                <div class="col-md-4  col-sm-4 col-xs-12">
                    <h3 class="footer__title ">Trang Bán Đồ Gỗ</h3>
                    <div class="list-menu-ft">
                        <ul>
                            <li><a href=""><i class="fa fa-check" aria-hidden="true"></i> Trang chủ</a></li>
                            <li><a href=""><i class="fa fa-check" aria-hidden="true"></i> Giới thiệu</a></li>
                            <li><a href=""><i class="fa fa-check" aria-hidden="true"></i> Sản phẩm</a></li>
                            <li><a href=""><i class="fa fa-check" aria-hidden="true"></i> Tin tức</a></li>
                        </ul>
                    </div>
                    <h3 class="footer__title ">Đăng ký nhận tin tức</h3>
                    <div class="list-menu-ft">
                        <p class="note-sbmail">Theo dõi bản tin của chungst ôi với những tin tức mới nhất và chác chương trình khuyến mại</p>
                        <div class="sub frm-regemail">
                            <input type="text" class="form-control" placeholder="Nhập email của bạn">
                            <button class="btn btn-regemail">Đăng ký</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h3 class="footer__title ">Tin tức</h3>
                    <div class="list-news-ft">
                        <ul>
                            <li>
                                <div class="avt">
                                    <a href=""><img src="/frontend_res/assets/uploads/news.jpg" alt="name your product"></a>
                                </div>
                                <div class="ct">
                                    <p class="name"><a href="">Các ý tưởng thiết kế ban công</a></p>
                                    <p class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 12/7/2017</p>
                                </div>
                            </li>
                            <li>
                                <div class="avt">
                                    <a href=""><img src="/frontend_res/assets/uploads/news.jpg" alt="name your product"></a>
                                </div>
                                <div class="ct">
                                    <p class="name"><a href="">Các ý tưởng thiết kế ban công</a></p>
                                    <p class="date"><i class="fa fa-calendar" aria-hidden="true"></i> 12/7/2017</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4  col-sm-4 col-xs-12">
                    <h3 class="footer__title ">Liên hệ</h3>
                    <?php foreach ($contact as $item) { ?>
                        <div class="contact">
                            <p class="name-comp"><?= $item['name'] ?></p>
                            <p class="info ">
                                <span class="icon -ap icon-location3"></span>
                                <span class="name-header texU disb fow">Địa chỉ: </span>
                                <span class="disb"><?= $item['address'] ?></span>
                            </p>
                        </div>
                        <div class="contact">
                            <p class="info ">
                                <span class="icon -ap  icon-envelope2"></span>
                                <span class="name-header texU disb fow">Email:</span>
                                <span class="disb"><?= $item['email'] ?></span>
                            </p>
                        </div>
                        <div class="contact">
                            <p class="info ">
                                <span class="icon fa fa-phone"></span>
                                <span class="name-header texU disb fow"> Phone: </span>
                                <span class="disb"><?= $item['phone'] ?></span>
                            </p>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <!-- End footer -->
    <!-- Copyright -->
    <div class="bottom">
        <div class="container">
            <div class="pull-left">
                <div class="copyright">
                    Copyright ® 2017 <a href="" class="url">ABC</a>
                </div>
            </div>
        </div>
    </div>
    <!-- end copyright -->

</footer>