<header class="header">
    <div class="banner">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="logo">
                        <img src="/frontend_res/assets/uploads/logo.png" alt="logo"/>
                    </div>
                </div>
                <!--<div class="col-md-5 col-sm-5 col-xs-12">-->
                <!--<div class="center-bn">-->
                <!--<a><span class="red">Hotline :</span> (+84) 0123465789</a>-->
                <!--|-->
                <!--<a href="#">Tìm cửa hàng trên toàn quốc</a>-->
                <!--</div>-->
                <!--</div>-->
                <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                <!--<div class="box-search">-->
                <!--<div class="form-input">-->
                <!--<input type="text" class="form-control" placeholder="Search...">-->
                <!--<button class="btn btn__search"><span class="fa fa-search"></span></button>-->
                <!--</div>-->
                <!--</div>-->
                <!--</div>-->
                <div class="col-md-9 col-sm-9 col-xs-12 col-right-headtop">
                    <div class="pull-left">
                        <div class="box-search">
                            <div class="form-input">
                                <input type="text" class="form-control" placeholder="Tìm kiếm...">
                                <button class="btn btn__search"><span class="fa fa-search"></span></button>
                            </div>
                        </div>
                    </div>
                    <div class="menu-top pull-right">
                        <div class="col-menu-top">
                            <div class="hotline"><span class="-ap icon-phone-handset"></span> Hotline: <?= $contact[0]['hotline'] ?></div>
                            <div class="header-top__shopcart">
                                <div class="icon">
                                    <span class="-ap icon-cart"></span>
                                </div>
                                <div class="headline">Giỏ hàng <span class="number-cart"><?= $total_product ?></span></div>
                                <div class="dropdown__shop__cart">
                                    <div class="dropdown__head">
                                        <span class="number"><?= $total_product ?></span>
                                        <span class="txt">Sản phẩm trong giỏ hàng</span>
                                    </div>
                                    <div class="product__shopcart" id="cart_product">
                                        <div class="product__list" >
                                            <?php foreach ($cart_product as $item) { ?>
                                                <div class="product__list__item">
                                                    <div class="product__img">
                                                        <img src="<?= $item['avatar'] ?>" alt="Name your Product ">
                                                    </div>
                                                    <div class="product__name">
                                                        <?= $item['name'] ?>
                                                        <a class="product__delete">
                                                            <span class="icon -ap icon-trash2"></span>
                                                        </a>
                                                    </div>

                                                    <div class="product__price">
                                                        <?php if ($item['type_discount'] == 1) { ?>
                                                            <div class="product__price__old"><?= $item['price'] ?><span class="unit">đ</span></div>
                                                            <div class="product__price__regular"><?= $item['price'] - $item['discount'] ?> <span class="unit">đ</span></div>
                                                        <?php } else { ?>
                                                            <div class="product__price__old"><?= $item['price'] ?><span class="unit">đ</span></div>
                                                            <div class="product__price__regular"><?= $item['price'] * (100 - $item['discount']) / 100 ?> <span class="unit">đ</span></div>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <form method="post">
                                        <div class="dropdown__bottom">
                                            
                                            <div class="total">Tổng cộng: <span class="product__price number" id="total_money_cart"><?= $total_money ?> <span class="unit"></span>đ</span></div>
                                            <a href="/shopping/shop_cart" class="btn__checkout">Tiến hành đặt hàng</a>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <?php if ($auth) { ?>
                                <div class="header-top__login">
                                    <div class="acc-mobi">
                                        <div class="title"><?= ($auth['fullname'] ? $auth['fullname'] : $auth['email']) ?></div>
                                        <ul class="menu-icons__sub">
                                            <li class="menu-icons__sub__item"><a class="menu-icon__sub-link" href="/customer" title=""><span class="fa fa-user"></span> Thông tin tài khoản</a></li>
                                            <li class="menu-icons__sub__item">
                                                <a href="/auth/logout" class="menu-icon__sub-link" title="">
                                                    <span class="fa fa-unlock-alt"></span> Thoát
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="header-top__login">
                                    <div class="acc-mobi">
                                        <div class="title">Tài khoản</div>
                                        <ul class="menu-icons__sub">
                                            <li class="menu-icons__sub__item"><a class="menu-icon__sub-link" data-toggle="modal" data-target="#login" data-original-title="" title=""><span class="icon icon-lock_outline -ap"></span> Đăng nhập</a></li>
                                            <li class="menu-icons__sub__item"><a class="menu-icon__sub-link" data-toggle="modal" data-target="#register" data-original-title="" title=""><span class="icon icon-lock_open -ap"></span> Đăng ký</a></li>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <!---->
                    </div>
                    <!---->
                </div>
            </div>
            <div class="nav-toggle">
                <span class="fa fa-bars"></span>
            </div>
        </div>
    </div>
    <div class="header-menu">
        <div class="container">
            <div class="menu-category">
                <div class="catalog__list">
                    <ul class="catalog__wrap">
                        <li class="catalog__item active">
                            <a href="/" class="catalog__link">Trang chủ</a>
                        </li>
                        <li class="catalog__item"><a href="/agency" class="catalog__link"> Giới thiệu</a></li>
                        <li class="catalog__item -sub">
                            <a href="/product"  class="catalog__link">Sản phẩm</a>
                            <div class="catalog__sub">
                                <div class="row">
                                    <div class="col-md-4 col-sm-5 col-xs-12">
                                        <ul class="list-cat__sub">
                                            <?php foreach ($category_sidebar as $item) { ?>
                                                <li class="catalog-sub__item"><a href="/product/product_category/<?= $item['id'] ?>"><?= $item['name'] ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="col-dm-8 col-sm-7 col-xs-12">
                                        <div class="catalog__title"><span>MỚI NHẤT HÔM NAY</span></div>
                                        <div class="catalog__product">
                                            <div class="product__image">
                                                <a href="#">
                                                    <img src="<?= $product_new['avatar'] ?>" alt="">
                                                </a>
                                                <div class="product__status -new">New </div>
                                            </div>
                                            <a href="/product/product_detail/<?= $product_new['id'] ?>" class="product__name"><?= $product_new['name'] ?></a>
                                            <div class="product__price">
                                                <?php if ($product_new['type_discount'] == 1) { ?>
                                                    <div class="product__price__old"><?= $product_new['price'] ?><span class="unit">đ</span></div>
                                                    <div class="product__price__regular"><?= $product_new['price'] - $product_new['discount'] ?> <span class="unit">đ</span></div>
                                                <?php } else { ?>
                                                    <div class="product__price__old"><?= $product_new['price'] ?><span class="unit">đ</span></div>
                                                    <div class="product__price__regular"><?= $product_new['price'] * (100 - $product_new['discount']) / 100 ?> <span class="unit">đ</span></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="catalog__item"><a href="/articel" class="catalog__link"> Tin tức</a></li>
                        <li class="catalog__item"><a href="/contact" class="catalog__link">Liên hệ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<div id="page-register" role="dialog">
    <form id="form-register" action="/auth/register" method="post">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Đăng ký tài khoản</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-7 col-xs-12 col-sm-7 account__input">
                            <div class="form-group">
                                <div class="row">
                                    <?= $this->flash->output() ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="input__label">
                                        <div class="label">Email:</div>
                                    </div>
                                    <div class="input__wrap">
                                        <input name="register[email]" autofocus type="email" class="form-control" placeholder="Email..." maxlength="40" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="input__label">
                                        <div class="label">Mật khẩu:</div>
                                    </div>
                                    <div class="input__wrap">
                                        <input type="password" id="pageRegister_password" name="register[password]" class="form-control" placeholder="Mật khẩu từ 6 đến 32 ký tự" maxlength="32" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="input__label">
                                        <div class="label">Nhập lại mật khẩu:</div>
                                    </div>
                                    <div class="input__wrap">
                                        <input type="password" name="register[password_again]" class="form-control" maxlength="32" placeholder="*******">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="input__label">
                                        <div class="label">Họ tên:</div>
                                    </div>
                                    <div class="input__wrap">
                                        <input name="register[fullname]" type="text"  class="form-control" placeholder="Nhập họ tên" maxlength="40" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="input__label">
                                        <div class="label">Giới tính</div>
                                    </div>
                                    <div class="input__wrap">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <div class="check__action -radio">
                                                    <input type="radio" checked="checked" value="1" class="checkbox" name="register[gender]" required>
                                                    <span class="icon"></span>
                                                    Nam
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="check__action -radio">
                                                    <input type="radio" class="checkbox" value="2" name="register[gender]" required>
                                                    <span class="icon"></span>
                                                    Nữ
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="input__label">
                                        <div class="label">Số điện thoại:</div>
                                    </div>
                                    <div class="input__wrap">
                                        <input name="register[phone]" type="text"  class="form-control" placeholder="Nhập số điện thoại" maxlength="14" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="input__label">
                                        <div class="label">Ngày Sinh:</div>
                                    </div>
                                    <div class="input__wrap pageBirthday-picker">
                                        <select class="birth-day form-control" name="register_birth[day]">
                                            <option value="">Ngày</option>
                                            <?php foreach (range(1, 31) as $i) { ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>
                                            <?php } ?>
                                        </select>
                                        <select class="birth-month form-control" name="register_birth[month]">
                                            <option value="">Tháng</option>
                                            <?php foreach (range(1, 12) as $i) { ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>
                                            <?php } ?>
                                        </select>
                                        <select class="birth-year form-control" name="register_birth[year]">
                                            <option value="">Năm</option>
                                            <?php $yearCurrent = date('Y'); ?>
                                            <?php $maxYear = $yearCurrent - 14; ?>
                                            <?php foreach (range(1900, $maxYear) as $i) { ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>
                                            <?php } ?>
                                        </select>
                                        <div style="display: none" id="register_notification_date" class="help-block">Ngày sinh không hợp lệ</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="check__action">
                                    <input type="checkbox" name="register_has" class="checkbox register_has">
                                    <span class="icon"></span>
                                    Tôi đồng ý mọi điều khoản của Website
                                </div>
                            </div>
                            <div class="form-group login__action">
                                <button type="submit" class="btn btn-submit">Đăng Ký</button>
                            </div>
                            <div class="login__note">
                                Bạn đã có tài khoản? <a href="/auth/login" >Đăng nhập</a>
                            </div>
                        </div>
                        <div class="account_login__social col-md-5 col-xs-12 col-sm-5">
                            <div class="title"><span>Đăng nhập với </span></div>
                            <div class="list__buttons">
                                <a href="#" class="btn btn-facebook">
                                    
                                    <span class="-ap icon-facebook icon"></span>
                                    Đăng nhập với Facebook
                                </a>
                                <a href="#" class="btn btn-google">
                                    
                                    <span class="-ap icon-google icon"></span>
                                    Đăng nhập với Google
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>
</div>
<footer class="footer">
    <!-- End topfoter -->
    <!-- footer -->
    <div class="container">
        <div class="footer__box">
            <div class="row ">
                <div class="col-md-4  col-sm-4 col-xs-12">
                    <h3 class="footer__title ">Trang Bán Đồ Gỗ</h3>
                    <div class="list-menu-ft">
                        <ul>
                            <li><a href="/index"><i class="fa fa-check" aria-hidden="true"></i> Trang chủ</a></li>
                            <li><a href="/agence"><i class="fa fa-check" aria-hidden="true"></i> Giới thiệu</a></li>
                            <li><a href="/product"><i class="fa fa-check" aria-hidden="true"></i> Sản phẩm</a></li>
                            <li><a href="/articel"><i class="fa fa-check" aria-hidden="true"></i> Tin tức</a></li>
                        </ul>
                    </div>
                    
                    
                        
                        
                            
                            
                        
                    
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h3 class="footer__title ">Tin tức</h3>
                    <div class="list-news-ft">
                        <ul>
                            <?php foreach ($articel_footer as $item) { ?>
                                <li>
                                    <div class="avt">
                                        <a href=""><img src="<?= $item['avatar'] ?>" alt="<?= $item['name'] ?>"></a>
                                    </div>
                                    <div class="ct">
                                        <p class="name"><a href=""><?= $item['name'] ?></a></p>
                                        <p class="date"><i class="fa fa-calendar" aria-hidden="true"></i> <?= $item['datecreate'] ?></p>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4  col-sm-4 col-xs-12">
                    <h3 class="footer__title ">Liên hệ</h3>
                    <?php foreach ($contact as $item) { ?>
                        <div class="contact">
                            <p class="name-comp"><?= $item['name'] ?></p>
                            <p class="info ">
                                <span class="icon -ap icon-location3"></span>
                                <span class="name-header texU disb fow">Địa chỉ: </span>
                                <span class="disb"><?= $item['address'] ?></span>
                            </p>
                        </div>
                        <div class="contact">
                            <p class="info ">
                                <span class="icon -ap  icon-envelope2"></span>
                                <span class="name-header texU disb fow">Email:</span>
                                <span class="disb"><?= $item['email'] ?></span>
                            </p>
                        </div>
                        <div class="contact">
                            <p class="info ">
                                <span class="icon fa fa-phone"></span>
                                <span class="name-header texU disb fow"> Phone: </span>
                                <span class="disb"><?= $item['phone'] ?></span>
                            </p>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <!-- End footer -->
    <!-- Copyright -->
    <div class="bottom">
        <div class="container">
            <div class="pull-left">
                <div class="copyright">
                    Copyright ® 2017 <a href="" class="url">ABC</a>
                </div>
            </div>
        </div>
    </div>
    <!-- end copyright -->

</footer>