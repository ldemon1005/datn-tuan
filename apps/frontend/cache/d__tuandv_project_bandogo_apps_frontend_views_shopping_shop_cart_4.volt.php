<div class="sec sec__shopcart">

    <div class="container">
        <div class="nav-steps">
            <div class="nav-step__item active">
                <span class="number">1</span>
                <span class="txt">Giỏ hàng</span>
            </div>
            <div class="nav-step__item active">
                <span class="number">2</span>
                <span class="txt">Nhập thông tin</span>
            </div>
            <div class="nav-step__item active">
                <span class="number">3</span>
                <span class="txt">Xác nhận thông tin</span>
            </div>
            <div class="nav-step__item">
                <span class="number active">4</span>
                <span class="txt text-success">Hoàn thành -- Mã đơn hàng : <?= $order['order_code'] ?></span>
            </div>
        </div>

        <div class="alert alert-success">
            <span class="-ap icon-info-with-circle"></span>
            Link thanh toán Ngân lượng
            <a href="<?= $order['url_nl'] ?>" class="alert-link" target="_blank"><?= $order['url_nl'] ?></a>
        </div>
        <div class="alert alert-success">
            <span class="-ap icon-info-with-circle"></span>
            Bạn đã mua hàng thành công
            <a href="/index" class="alert-link">Quay lại trang chủ để tiếp tục mua hàng</a>
        </div>
    </div>
</div>