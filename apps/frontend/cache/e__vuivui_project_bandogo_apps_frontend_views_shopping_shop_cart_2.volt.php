<div class="sec sec__shopcart" xmlns="http://www.w3.org/1999/html">

    <div class="container">
        <div class="nav-steps">
            <div class="nav-step__item active">
                <span class="number">1</span>
                <span class="txt">Giỏ hàng</span>
            </div>
            <div class="nav-step__item waiting">
                <span class="number">2</span>
                <span class="txt">Nhập thông tin</span>
            </div>
            <div class="nav-step__item">
                <span class="number">3</span>
                <span class="txt">Xác nhận thông tin</span>
            </div>
            <div class="nav-step__item">
                <span class="number">4</span>
                <span class="txt">Hoàn thành</span>
            </div>
        </div>
        <div class="row">
            <form id="cart_2" action="/shopping/shop_cart/<?= $step ?>" method="post">
                <div class="sec__shopcart__info col-md-7 col-sm-7 col-xs-12">
                    <div class="box-user__info panel">
                        <div class="title">Thông tin thanh toán và nhận hàng</div>
                        <div class="content">
                            <div class="row">
                                <div class="col-md-6 col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <div class="label">Số điện thoại:</div>
                                        <input value="<?= $auth['phone'] ?>" name="order[phone]" type="text" class="form-control" placeholder="SĐT" required>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <div class="label">Họ tên:</div>
                                        <input value="<?= $auth['fullname'] ?>" name="order[fullname]" type="text" class="form-control" placeholder="Tên" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="label">Email</div>
                                <input value="<?= $auth['email'] ?>" type="email" name="order[email]" class="form-control" placeholder="Email" required>
                            </div>
                            <div class="form-group">
                                <div class="label">Địa chỉ:</div>
                                <input type="text" class="form-control" name="order[address]" placeholder="Địa chỉ">
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <select name="order[province_id]" id="province_id" class="form-control selectpicker" data-live-search="true" style="width: 100%;" required>
                                        <option value="0" selected="selected">--Tỉnh/thành--</option>
                                        <?php foreach ($province as $item) { ?>
                                            <option value="<?= $item->city_id ?>"><?= $item->name ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="order[district_id]" id="district_id" class="form-control selectpicker" data-live-search="true" style="width: 100%;" required>
                                        <option value="0" selected="selected">--Quận/huyện--</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="order[ward_id]" id="ward_id" class="form-control selectpicker" data-live-search="true" style="width: 100%;" required>
                                        <option value="0" selected="selected" class="text-center">--Xã/phường--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="label">Ghi chú:</div>
                                <textarea name="order[note]" id="" cols="30" rows="10" class="form-control" placeholder="Nội dung"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-12">
                    <div class="title">Giỏ hàng</div>
                    <div class="content">
                        <div class="product__shopcart">
                            <div class="product__list">
                                <?php foreach ($list_product as $item) { ?>
                                    <div class="product__list__item">
                                        <div class="product__img">
                                            <img src="<?= $item['avatar'] ?>" alt="<?= $item['name'] ?>">
                                        </div>
                                        <div class="product__name">
                                            <a href="#"><?= $item['name'] ?></a>
                                            <a class="product__delete">
                                                <span class="icon -ap icon-trash2"></span>
                                            </a>
                                        </div>

                                        <div class="product__price">
                                            <?php if ($item['type_discount'] == 1) { ?>
                                                <div class="product__price__old"><?= number_format($item['price'], 0, '.', ',') ?><span class="unit">đ</span></div>
                                                <div class="product__price__regular"><?= number_format($item['price'] - $item['discount'], 0, '.', ',') ?> <span class="unit">đ</span></div>
                                            <?php } else { ?>
                                                <div class="product__price__old"><?= number_format($item['price'], 0, '.', ',') ?><span class="unit">đ</span></div>
                                                <div class="product__price__regular"><?= number_format($item['price'] * (100 - $item['discount']) / 100, 0, '.', ',') ?> <span class="unit">đ</span></div>
                                            <?php } ?>
                                            <div class="product__quantity">
                                                <button class="btn-minus btn">
                                                    -
                                                </button>
                                                <input class="input-number quantitty_number"
                                                       product_id="<?= $item['id'] ?>"
                                                       product_price="<?= $item['price_disconut'] ?>"
                                                       old_money="<?= $total_money ?>"
                                                       old_quantity="<?= $item['quantity'] ?>"
                                                       value="<?= $item['quantity'] ?>" type="text">
                                                <button class="btn-plus btn">
                                                    +
                                                </button>
                                            </div>

                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-price-total">
                        <div class="">
                            <div class="price-provi">
                                <div class="pull-left"><label class="checkbox-inline"><input id="vtp" step="<?= $step - 1 ?>" type="checkbox" value="1">Vận chuyển Viettelpost</label></div>
                            </div>
                            <div class="price-provi form-group vtp" style="display: none" >
                                <div class="col-md-4">
                                    Cửa hàng
                                </div>
                                <div class="col-md-8">
                                    <select id="store" name="order[store_id]" class="form-control">
                                        <?php foreach ($contact as $item) { ?>
                                            <option value="<?= $item['id'] ?>">
                                                <?= $item['name'] ?>--<?= $item['address'] ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="price-provi form-group vtp" style="display: none">
                                <div class="col-md-4">
                                    Dịch vụ chuyển phát
                                </div>
                                <div class="col-md-8">
                                    <select id="service" name="order[vtp_service]" class="form-control">
                                        <?php foreach ($vtp as $item) { ?>
                                            <option value="<?= $item['SERVICE_CODE'] ?>">
                                                <?= $item['SERVICE_NAME'] ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="price-provi form-group vtp" style="display: none" >
                                <div class="pull-left">
                                    <button class="btn btn-primary calculate_vtp" type="button">Tính phí vận chuyển</button>
                                    <input id="fee_transporter_input" class="hidden" name="order[vtp_fee]" value="0">
                                </div>
                                <div class="pull-right fee_transposter">₫</div>
                            </div>
                            <div class="row form-group">
                                <span class="text-danger" id="message_error"></span>
                            </div>
                            <div class="price-total">
                                <div class="pull-left">Thành tiền :</div>
                                <div class="pull-right" id="total_money" total_money="<?= $total_money ?>"><?= number_format($total_money, 0, '.', ',') ?>₫</div>
                            </div>
                        </div>
                    </div>
                    <div class="list-button">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <?= $this->flash->output() ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <a class="btn btn__back" href="/shopping/shop_cart/<?= $step - 2 ?>">Quay lại</a>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <button type="submit" class="btn btn__next">Tiếp tục thanh toán</button>
                            </div>

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>

<script>
    $(document).ready(function () {
        jQuery.validator.addMethod("notEqualTo", function(v, e, p) {
            return this.optional(e) || v != p;
        }, "Thiếu thông tin địa chỉ");

        $("#cart_2").validate({
            rules : {
                'order[fullname]' : {
                    required : true
                },
                'order[phone]' : {
                    required : true,
                    digits: !0,
                    maxlength: 13,
                    minlength: 8
                },
                'order[email]':{
                    required:true
                },
                'order[province_id]' : {
                    required : true,
                    notEqualTo : '0'
                },
                'order[district_id]' : {
                    required : true,
                    notEqualTo : '0'
                },
                'order[ward_id]' : {
                    required : true,
                    notEqualTo : '0'
                }
            },
            messages : {
                'order[fullname]' : {
                    required : "Bạn vui lòng nhập họ tên."
                },
                'order[phone]' : {
                    required : "Bạn vui lòng nhập số điện thoại.",
                    digits: "Số điện thoại phải là số.",
                    minlength: "Số điện thoại tối thiểu phải có 8 số.",
                    maxlength: "Số điện thoại chỉ được 8 đến 14 số."
                },
                'order[email]':{
                    required:"Bạn vui lòng nhập email.",
                }
                ,
                'order[province_id]':{
                    required:"Bạn vui lòng chọn tỉnh thành.",
                    notEqualTo : 'Bạn vui lòng chọn tỉnh thành.'
                }
                ,
                'order[district_id]':{
                    required:"Bạn vui lòng chọn quận/huyện.",
                    notEqualTo : 'Bạn vui lòng chọn quận/huyện.'
                }
                ,
                'order[ward_id]':{
                    required:"Bạn vui lòng chọn xã/phường.",
                    notEqualTo : 'Bạn vui lòng chọn xã/phường.'
                }
            }
        })
    });
</script>