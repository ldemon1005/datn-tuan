<header class="header">
    <div class="banner">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="logo">
                        <img src="/frontend_res/assets/uploads/logo.png" alt="logo"/>
                    </div>
                </div>
                <!--<div class="col-md-5 col-sm-5 col-xs-12">-->
                <!--<div class="center-bn">-->
                <!--<a><span class="red">Hotline :</span> (+84) 0123465789</a>-->
                <!--|-->
                <!--<a href="#">Tìm cửa hàng trên toàn quốc</a>-->
                <!--</div>-->
                <!--</div>-->
                <!--<div class="col-md-4 col-sm-4 col-xs-12">-->
                <!--<div class="box-search">-->
                <!--<div class="form-input">-->
                <!--<input type="text" class="form-control" placeholder="Search...">-->
                <!--<button class="btn btn__search"><span class="fa fa-search"></span></button>-->
                <!--</div>-->
                <!--</div>-->
                <!--</div>-->
                <div class="col-md-9 col-sm-9 col-xs-12 col-right-headtop">
                    <div class="pull-left">
                        <div class="box-search">
                            <div class="form-input">
                                <form method="post" action="/product">
                                    <input type="text" name="text_search" class="form-control" placeholder="Tìm kiếm...">
                                    <button type="submit" class="btn btn__search"><span class="fa fa-search"></span></button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="menu-top pull-right">
                        <div class="col-menu-top">
                            <div class="hotline"><span class="-ap icon-phone-handset"></span> Hotline: <?= $contact[0]['hotline'] ?></div>
                            <div class="header-top__shopcart">
                                <div class="icon">
                                    <span class="-ap icon-cart"></span>
                                </div>
                                <div class="headline">Giỏ hàng <span class="number-cart"><?= $total_product ?></span></div>
                                <div class="dropdown__shop__cart">
                                    <div class="dropdown__head">
                                        <span class="number"><?= $total_product ?></span>
                                        <span class="txt">Sản phẩm trong giỏ hàng</span>
                                    </div>
                                    <div class="product__shopcart" id="cart_product">
                                        <div class="product__list" >
                                            <?php foreach ($cart_product as $item) { ?>
                                                <div class="product__list__item">
                                                    <div class="product__img">
                                                        <img src="<?= $item['avatar'] ?>" alt="Name your Product ">
                                                    </div>
                                                    <div class="product__name">
                                                        <?= $item['name'] ?>
                                                        <a class="product__delete">
                                                            <span class="icon -ap icon-trash2"></span>
                                                        </a>
                                                    </div>

                                                    <div class="product__price">
                                                        <?php if ($item['type_discount'] == 1) { ?>
                                                            <div class="product__price__old"><?= number_format($item['price'], 2, '.', ' ') ?><span class="unit">đ</span></div>
                                                            <div class="product__price__regular"><?= number_format($item['price'] - $item['discount'], 2, '.', ' ') ?> <span class="unit">đ</span></div>
                                                        <?php } else { ?>
                                                            <div class="product__price__old"><?= number_format($item['price'], 2, '.', ' ') ?><span class="unit">đ</span></div>
                                                            <div class="product__price__regular"><?= number_format($item['price'] * (100 - $item['discount']) / 100, 2, '.', ' ') ?> <span class="unit">đ</span></div>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <form method="post">
                                        <div class="dropdown__bottom">
                                            
                                            <div class="total">Tổng cộng: <span class="product__price number" id="total_money_cart"><?= number_format($total_money, 2, '.', ' ') ?> <span class="unit"></span>đ</span></div>
                                            <a href="/shopping/shop_cart" class="btn__checkout">Tiến hành đặt hàng</a>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <?php if ($auth) { ?>
                                <div class="header-top__login">
                                    <div class="acc-mobi">
                                        <div class="title"><?= ($auth['fullname'] ? $auth['fullname'] : $auth['email']) ?></div>
                                        <ul class="menu-icons__sub">
                                            <li class="menu-icons__sub__item"><a class="menu-icon__sub-link" href="/customer" title=""><span class="fa fa-user"></span> Thông tin tài khoản</a></li>
                                            <li class="menu-icons__sub__item">
                                                <a href="/auth/logout" class="menu-icon__sub-link" title="">
                                                    <span class="fa fa-unlock-alt"></span> Thoát
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="header-top__login">
                                    <div class="acc-mobi">
                                        <div class="title">Tài khoản</div>
                                        <ul class="menu-icons__sub">
                                            <li class="menu-icons__sub__item"><a class="menu-icon__sub-link" data-toggle="modal" data-target="#login" data-original-title="" title=""><span class="icon icon-lock_outline -ap"></span> Đăng nhập</a></li>
                                            <li class="menu-icons__sub__item"><a class="menu-icon__sub-link" data-toggle="modal" data-target="#register" data-original-title="" title=""><span class="icon icon-lock_open -ap"></span> Đăng ký</a></li>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <!---->
                    </div>
                    <!---->
                </div>
            </div>
            <div class="nav-toggle">
                <span class="fa fa-bars"></span>
            </div>
        </div>
    </div>
    <div class="header-menu">
        <div class="container">
            <div class="menu-category">
                <div class="catalog__list">
                    <ul class="catalog__wrap">
                        <li class="catalog__item active">
                            <a href="/" class="catalog__link">Trang chủ</a>
                        </li>
                        <li class="catalog__item"><a href="/agency" class="catalog__link"> Giới thiệu</a></li>
                        <li class="catalog__item -sub">
                            <a href="/product"  class="catalog__link">Sản phẩm</a>
                            <div class="catalog__sub">
                                <div class="row">
                                    <div class="col-md-4 col-sm-5 col-xs-12">
                                        <ul class="list-cat__sub">
                                            <?php foreach ($category_sidebar as $item) { ?>
                                                <li class="catalog-sub__item"><a href="/product/product_category/<?= $item['id'] ?>"><?= $item['name'] ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                    <div class="col-dm-8 col-sm-7 col-xs-12">
                                        <div class="catalog__title"><span>MỚI NHẤT HÔM NAY</span></div>
                                        <div class="catalog__product">
                                            <div class="product__image">
                                                <a href="#">
                                                    <img src="<?= $product_new['avatar'] ?>" alt="">
                                                </a>
                                                <div class="product__status -new">New </div>
                                            </div>
                                            <a href="/product/product_detail/<?= $product_new['id'] ?>" class="product__name"><?= $product_new['name'] ?></a>
                                            <div class="product__price">
                                                <?php if ($product_new['type_discount'] == 1) { ?>
                                                    <div class="product__price__old"><?= $product_new['price'] ?><span class="unit">đ</span></div>
                                                    <div class="product__price__regular"><?= $product_new['price'] - $product_new['discount'] ?> <span class="unit">đ</span></div>
                                                <?php } else { ?>
                                                    <div class="product__price__old"><?= $product_new['price'] ?><span class="unit">đ</span></div>
                                                    <div class="product__price__regular"><?= $product_new['price'] * (100 - $product_new['discount']) / 100 ?> <span class="unit">đ</span></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="catalog__item"><a href="/articel" class="catalog__link"> Tin tức</a></li>
                        <li class="catalog__item"><a href="/contact" class="catalog__link">Liên hệ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<style>
    a.disabled {
        pointer-events: none;
        cursor: default;
        background: #dddddd;
    }
</style>

<div class="sec sec__contact">
    <div class="container">
        <div class="contact-list__head">
            <h2 class="contact-list__title">
                Liên hệ
            </h2>
        </div>
        <div class="row">
            <div class="list-contact col-md-5 col-sm-6 col-xs-12">
                <?php foreach ($list_contact as $item) { ?>
                    <div class="item">
                        <div class="title-comp">
                            <?= $item['name'] ?>
                        </div>
                        <div class="contact">
                            <p class="info ">
                                <span class="icon -ap icon-old-phone"></span>
                                <span class="name-header">Điện thoại: </span>
                                <span class="disb"><?= $item['phone'] ?></span>
                            </p>
                        </div>
                        <div class="contact">
                            <p class="info ">
                                <span class="icon -ap icon-location3"></span>
                                <span class="name-header texU disb fow">Địa chỉ: </span>
                                <span class="disb"><?= $item['address'] ?></span>
                            </p>
                        </div>
                        <div class="contact">
                            <p class="info ">
                                <span class="icon -ap  icon-envelope2"></span>
                                <span class="name-header texU disb fow">Email:</span>
                                <span class="disb"><?= $item['email'] ?></span>
                            </p>
                        </div>
                        <div class="contact">

                            <p class="info ">
                                <span class="icon -ap icon-access_time"></span>
                                <span class="name-header texU disb fow">Thời gian mở cửa: </span>
                                <span class="disb">Mon - Sat : 9:00 am - 22:00 pm</span>
                            </p>
                        </div>
                    </div>
                <?php } ?>
                <div class="item">
                    <div class="link-map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8858.811083050541!2d105.84461184784595!3d21.005811569826868!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76ccab6dd7%3A0x55e92a5b07a97d03!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBCw6FjaCBraG9hIEjDoCBO4buZaQ!5e0!3m2!1svi!2s!4v1521979280392" width="400" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="form-contact col-md-7 col-sm-6 col-xs-12">
                <form id="contact_info" action="/contact/create" method="post">
                    <div class="form-group">
                        <?= $this->flash->output() ?>
                    </div>
                    <div class="form-group">
                        <div class="label">Họ và tên</div>
                        <input name="contact_customer[fullname]" type="text" class="form-control" placeholder="Họ và tên...">
                    </div>
                    <div class="form-group">
                        <div class="label">Email</div>
                        <input name="contact_customer[email]" type="email" class="form-control" placeholder="Email...">
                    </div>
                    <div class="form-group">
                        <div class="label">Số điện thoại</div>
                        <input name="contact_customer[phone]" type="text" class="form-control" placeholder="Số điện thoại...">
                    </div>
                    <div class="form-group">
                        <div class="label">Nội dung:</div>
                        <textarea  name="contact_customer[content]" id="" cols="30" rows="10" class="form-control" placeholder="Nhập nội dung"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-submit pull-right">Gửi</button>
                    </div>
                </form>

            </div>
        </div>

    </div>
</div>