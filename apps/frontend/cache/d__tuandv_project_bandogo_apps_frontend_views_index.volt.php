<!DOCTYPE html>
<html lang="vi">

<head>
    <title>tranggarden</title>
    <meta charset="utf-8" />
    <meta name="keywords" content="It, it solution,solution, techonogy,internet" />
    <link href="https://plus.google.com/114644939574462223089" rel="author">
    <meta content="IE=edge" http-equiv="X-UA-Compatible"></meta>
    <meta content="width=device-width, initial-scale=1" name="viewport" id="viewport"></meta>
    <meta content="tranggarden" name="description">
    <meta content="IE=edge" http-equiv="X-UA-Compatible"></meta>

    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" id="viewport"></meta>
    <meta content="index, follow" name="ROBOTS">
    <meta name="format-detection" content="telephone=no">
    <!-- General style -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700&amp;subset=vietnamese" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/frontend_res/assets/vendor/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/frontend_res/assets/vendor/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/frontend_res/assets/vendor/css/slick.css">
    <link rel="stylesheet" type="text/css" href="/frontend_res/assets/vendor/css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="/frontend_res/assets/vendor/css/jquery.fancybox.min.css">
    <link rel="stylesheet" type="text/css" href="/frontend_res/assets/vendor/css/ap8.css">
    <link rel="stylesheet" type="text/css" href="/frontend_res/assets/css/style.css">
    <link rel="stylesheet/less" type="text/css" href="/frontend_res/assets/css/shopcart.less">


    <script type="text/javascript" src="/frontend_res/assets/vendor/js/jquery.2.1.1.min.js"></script>
    <script type="text/javascript" src="/frontend_res/assets/js/jquery.validate.min.js"></script>

</head>

<body>
<div class="common-overlay"></div>
<?= $this->getContent() ?>
<div id="snackbar"></div>
<div id="snackbar_danger"></div>
<div id="QuickView" class="modal fade" role="dialog"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">

</div>
<div id="login" class="modal__account modal fade" role="dialog">
    <form id="login-form" method="post" action="/auth/login">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span class="-ap icon-close"></span></button>
                    <h4 class="modal-title">Đăng nhập</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-12 col-sm-6 account__input">
                            <div class="form-group">
                                <div class="label">Email hoặc Số điện thoại:</div>
                                <div class="input">
                                    <input name="username" autofocus type="text" class="form-control" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="label">Mật khẩu:</div>
                                <input name="password" type="password" class="form-control" placeholder="Mật khẩu từ 6 đến 32 ký tự">
                            </div>
                            <div class="form-group login__action">
                                <button type="submit" class="btn btn-submit">Đăng nhập</button>
                            </div>
                            <div class="forgot_password">
                                <a href="/auth/forgot_password"><span style="color: #1976D2">Quên mật khẩu?</span></a>
                            </div>
                            <div class="login__note">
                                Bạn chưa có tài khoản? <a href="javascript:;" data-dismiss="modal" data-toggle="modal" data-target="#register">Đăng ký ngay</a>
                            </div>
                        </div>
                        <div class="account_login__social col-md-6 col-xs-12 col-sm-6">
                            <div class="title"><span>Đăng nhập với </span></div>
                            <div class="list__buttons">
                                <a href="#" class="btn btn-facebook">
                                    
                                    <span class="-ap icon-facebook icon"></span>
                                    Đăng nhập với Facebook
                                </a>
                                <a href="#" class="btn btn-google">
                                    
                                    <span class="-ap icon-google icon"></span>
                                    Đăng nhập với Google
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="register" class="modal__account modal fade" role="dialog">
    <form id="register-form" method="post" action="/auth/register">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span class="-ap icon-close"></span>
                    </button>
                    <h4 class="modal-title">Đăng ký tài khoản</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-7 col-xs-12 col-sm-7 account__input">
                            <div class="form-group">
                                <div class="row">
                                    <div class="input__label">
                                        <div class="label">Email:</div>
                                    </div>
                                    <div class="input__wrap">
                                        <input name="register[email]" autofocus type="email" class="form-control" placeholder="Email..." maxlength="40" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="input__label">
                                        <div class="label">Mật khẩu:</div>
                                    </div>
                                    <div class="input__wrap">
                                        <input type="password" id="register_password" name="register[password]" class="form-control" placeholder="Mật khẩu từ 6 đến 32 ký tự" maxlength="32" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="input__label">
                                        <div class="label">Nhập lại mật khẩu:</div>
                                    </div>
                                    <div class="input__wrap">
                                        <input type="password" name="register[password_again]" class="form-control" maxlength="32" placeholder="*******">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="input__label">
                                        <div class="label">Họ tên:</div>
                                    </div>
                                    <div class="input__wrap">
                                        <input name="register[fullname]" type="text" class="form-control" placeholder="Nhập họ tên" maxlength="40" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="input__label">
                                        <div class="label">Giới tính</div>
                                    </div>
                                    <div class="input__wrap">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <div class="check__action -radio">
                                                    <input type="radio" checked="checked" value="1" class="checkbox" name="register[gender]" required>
                                                    <span class="icon"></span>
                                                    Nam
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="check__action -radio">
                                                    <input type="radio" class="checkbox" value="2" name="register[gender]" required>
                                                    <span class="icon"></span>
                                                    Nữ
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="input__label">
                                        <div class="label">Số điện thoại:</div>
                                    </div>
                                    <div class="input__wrap">
                                        <input name="register[phone]" type="text" class="form-control" placeholder="Nhập số điện thoại" maxlength="11" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="input__label">
                                        <div class="label">Ngày Sinh:</div>
                                    </div>
                                    <div class="input__wrap birthday-picker">
                                        <select class="birth-day form-control" name="register_birth[day]">
                                            <option value="">Ngày</option>
                                            <?php foreach (range(1, 31) as $i) { ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>
                                            <?php } ?>
                                        </select>
                                        <select class="birth-month form-control" name="register_birth[month]">
                                            <option value="">Tháng</option>
                                            <?php foreach (range(1, 12) as $i) { ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>
                                            <?php } ?>
                                        </select>
                                        <select class="birth-year form-control" name="register_birth[year]">
                                            <option value="">Năm</option>
                                            <?php $yearCurrent = date('Y'); ?>
                                            <?php $maxYear = $yearCurrent - 14; ?>
                                            <?php foreach (range(1900, $maxYear) as $i) { ?>
                                                <option value="<?= $i ?>"><?= $i ?></option>
                                            <?php } ?>
                                        </select>
                                        <div style="display: none" id="register_notification_date" class="help-block">Ngày sinh không hợp lệ</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="check__action">
                                    <input type="checkbox" name="register_has" class="checkbox register_has">
                                    <span class="icon"></span>
                                    Tôi đồng ý mọi điều khoản của Website
                                </div>
                            </div>
                            <div class="form-group login__action">
                                <button id="register_btn" class="btn btn-submit">Đăng Ký</button>
                            </div>
                            <div class="login__note">
                                Bạn đã có tài khoản? <a href="javascript:;" data-dismiss="modal" data-toggle="modal" data-target="#login">Đăng nhập</a>
                            </div>
                        </div>
                        <div class="account_login__social col-md-5 col-xs-12 col-sm-5">
                            <div class="title"><span>Đăng nhập với </span></div>
                            <div class="list__buttons">
                                <a href="<?= $facebook_login ?>/auth/website_facebook?domain=http://<?= $domain ?>/auth/facebook" class="btn btn-facebook">
                                    
                                    <span class="-ap icon-facebook icon"></span>
                                    Đăng nhập với Facebook
                                </a>
                                <a href="<?= $facebook_login ?>/auth/website_google?domain=http://<?= $domain ?>/auth/google" class="btn btn-google">
                                    
                                    <span class="-ap icon-google icon"></span>
                                    Đăng nhập với Google
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>


<a class="scroll-top"><span class="fa fa-angle-up"></span></a>
<!-- end footer -->
<script type="text/javascript" src="/frontend_res/assets/vendor/js/jquery.2.1.1.min.js"></script>
<script type="text/javascript" src="/frontend_res/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/frontend_res/assets/vendor/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/frontend_res/assets/vendor/js/slick.min.js"></script>
<script type="text/javascript" src="/frontend_res/assets/vendor/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="/frontend_res/assets/vendor/js/jquery.nivo.slider.pack.js"></script>
<script type="text/javascript" src="/frontend_res/assets/vendor/js/jquery.elevateZoom-3.0.8.min.js"></script>
<script type="text/javascript" src="/frontend_res/assets/vendor/js/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="/frontend_res/assets/vendor/js/nouislider.min.js"></script>
<script type="text/javascript" src="/frontend_res/assets/js/custom.js"></script>
<script type="text/javascript" src="/frontend_res/assets/js/shoping.js"></script>



<script type="text/javascript">

    $(document).ready(function () {
        $(document).on('change', '#province_id', function (e) {
            e.preventDefault();
            $.ajax({
                url: '/user/district',
                method: 'post',
                data: { id: $(this).val() },
                success:function (result) {
                    var option = "<option selected=\"selected\">--Quận/huyện--</option>";
                    var data = JSON.parse(result);
                    data.forEach(function (item) {
                        option = option + "<option value=\"" + item.district_id + "\">" + item.name +"</option>";
                    });
                    var html_option = $.parseHTML(option);
                    $("#district_id").html(html_option);
                    $('#district_id').selectpicker('refresh');

                }
            })
        });
        $(document).on('change', '#district_id', function (e) {
            e.preventDefault();
            $.ajax({
                url: '/user/ward',
                method: 'post',
                data: { id: $(this).val() },
                success:function (result) {
                    var option = "<option selected=\"selected\">--Quận/huyện--</option>";
                    var data = JSON.parse(result);
                    data.forEach(function (item) {
                        option = option + "<option value=\"" + item.ward_id + "\">" + item.name +"</option>";
                    });
                    var html_option = $.parseHTML(option);
                    $("#ward_id").html(html_option);
                    $('#ward_id').selectpicker('refresh');

                }
            })
        });

        $(document).on('click', '#quick_view', function (e) {
            console.log($(this).attr('product'));
            e.preventDefault();
            $.ajax({
                url: '/index/quick_view',
                method: 'post',
                data: { id: $(this).attr('product') },
                success:function (result) {
                    var res = JSON.parse(result);
                    $("#QuickView").html(res.content);
                    $("#QuickView").modal('show');
                    setTimeout(function () {
                        $('<link rel="stylesheet" type="text/css" href="'+"/frontend_res/assets/vendor/css/slick.css"+'" />').appendTo("head");
                        jQuery.getScript("/frontend_res/assets/js/custom.js")
                            .done(function() {
                            })
                            .fail(function() {
                            });
                        jQuery.getScript("/frontend_res/assets/js/shoping.js")
                            .done(function() {
                            })
                            .fail(function() {
                            });
                    },1000);
                }
            })
        });
    });
</script>
</body>

</html>
