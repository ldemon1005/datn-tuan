<div class="box category">
    <div class="title-name">
        Danh mục sản phẩm
    </div>
    <div class="list">
        <ul>
            <?php foreach ($category_sidebar as $item) { ?>
                <li><a href="/product/product_category/<?= $item['id'] ?>"><?= $item['name'] ?></a></li>
            <?php } ?>
        </ul>
    </div>
</div>