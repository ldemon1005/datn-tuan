<div class="icon">
    <span class="-ap icon-cart"></span>
</div>
<div class="headline">Giỏ hàng <span class="number-cart"><?= $data['total_product'] ?></span></div>
<div class="dropdown__shop__cart">
    <div class="dropdown__head">
        <span class="number"><?= $data['total_product'] ?></span>
        <span class="txt">Sản phẩm trong giỏ hàng</span>
    </div>
    <div class="product__shopcart" id="cart_product">
        <div class="product__list" >
            <?php foreach ($data['cart_product'] as $item) { ?>
                <div class="product__list__item">
                    <div class="product__img">
                        <img src="<?= $item['avatar'] ?>" alt="Name your Product ">
                    </div>
                    <div class="product__name">
                        <?= $item['name'] ?>
                        <a class="product__delete">
                            <span class="icon -ap icon-trash2"></span>
                        </a>
                    </div>

                    <div class="product__price">
                        <?php if ($item['type_discount'] == 1) { ?>
                            <div class="product__price__old"><?= $item['price'] ?><span class="unit">đ</span></div>
                            <div class="product__price__regular"><?= $item['price'] - $item['discount'] ?> <span class="unit">đ</span></div>
                        <?php } else { ?>
                            <div class="product__price__old"><?= $item['price'] ?><span class="unit">đ</span></div>
                            <div class="product__price__regular"><?= $item['price'] * (100 - $item['discount']) / 100 ?> <span class="unit">đ</span></div>
                        <?php } ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            <?php } ?>
        </div>
    </div>
    <form method="post">
        <div class="dropdown__bottom">
            
            <div class="total">Tổng cộng: <span class="product__price number" id="total_money_cart"><?= $data['total_money'] ?> <span class="unit"></span>đ</span></div>
            <a href="/shopping/shop_cart" class="btn__checkout">Tiến hành đặt hàng</a>
        </div>
    </form>
</div>