{% include "layouts/header.volt" %}
<!-- End menu -->
{% include "layouts/banner.volt" %}
<div class="sec list-product">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12 col-left">
                {% include "layouts/sidebar.volt" %}
            </div>
            <!--/-->
            <div class="col-md-9 col-sm-9 col-xs-12 col-right">
                <div class="box-ct">
                    <div class="title-name">
                        Sản phẩm ưa chuộng
                    </div>
                    <div class="list-ct">
                        <div class="row">
                            <div class="row">
                                {% for item in product_popularity %}
                                    <div class="col-md-3 col-sm-3 col-xs-6">
                                        <div class="product__item">
                                            <div class="w">
                                                <div class="product__image">
                                                    <a href="#" class="product__link"><img style="max-height: 100px" src="{{ item['avatar'] }}" alt="name your product"></a>
                                                    <div class="product-item__actions">
                                                        <a class="btn btn-detail" data-toggle="tooltip" data-placement="top" href="/product/product_detail/{{ item['id'] }}" title="Chi tiết"><span class="-ap  icon-search2"></span></a>
                                                        <a class="btn btn-quickview" id="quick_view" product="{{ item['id'] }}" data-toggle="modal" data-target="#QuickView" data-placement="top" title="Xem nhanh"><span class="fa fa-eye"></span></a>
                                                    </div>
                                                    <div class="product__overlay"></div>
                                                    {% if item['type_discount'] == 2 %}
                                                        <div class="product__sale">{{ item['discount'] }}%</div>
                                                    {% endif %}
                                                </div>
                                                <h3 class="product__name"><a href="#">{{ item['name'] }}</a></h3>
                                                <div class="product__price">
                                                    {% if item['type_discount'] == 1  %}
                                                        <div class="product__price__old">{{ number_format(item['price']) }}<span class="unit">đ</span></div>
                                                        <div class="product__price__regular">{{ number_format(item['price'] - item['discount']) }} <span class="unit">đ</span></div>
                                                    {% else %}
                                                        <div class="product__price__old">{{ number_format(item['price']) }}<span class="unit">đ</span></div>
                                                        <div class="product__price__regular">{{ number_format(item['price']*(100-item['discount'])/100) }} <span class="unit">đ</span></div>
                                                    {% endif %}

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                {% endfor %}
                            </div>
                        </div>
                    </div>
                </div>
                <!---->
                <div class="box-ct">
                    <div class="title-name">
                        Sản phẩm
                    </div>
                    <div class="list-ct">
                        <div class="row">
                            {% for item in list_product %}
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="product__item">
                                        <div class="w">
                                            <div class="product__image">
                                                <a href="#" class="product__link"><img style="max-height: 100px" src="{{ item['avatar'] }}" alt="name your product"></a>
                                                <div class="product-item__actions">
                                                    <a class="btn btn-detail" data-toggle="tooltip" data-placement="top" href="/product/product_detail/{{ item['id'] }}" title="Chi tiết"><span class="-ap  icon-search2"></span></a>
                                                    <a class="btn btn-quickview" id="quick_view" product="{{ item['id'] }}" title="Xem nhanh"><span class="fa fa-eye"></span></a>
                                                </div>
                                                <div class="product__overlay"></div>
                                                {% if item['type_discount'] == 2 %}
                                                    <div class="product__sale">{{ item['discount'] }}%</div>
                                                {% endif %}
                                            </div>
                                            <h3 class="product__name"><a href="#">{{ item['name'] }}</a></h3>
                                            <div class="product__price">
                                                {% if item['type_discount'] == 1  %}
                                                    <div class="product__price__old">{{ number_format(item['price'])  }}<span class="unit">đ</span></div>
                                                    <div class="product__price__regular">{{ number_format(item['price'] - item['discount']) }} <span class="unit">đ</span></div>
                                                {% else %}
                                                    <div class="product__price__old">{{ number_format(item['price'])  }}<span class="unit">đ</span></div>
                                                    <div class="product__price__regular">{{ number_format(item['price']*(100-item['discount'])/100) }} <span class="unit">đ</span></div>
                                                {% endif %}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {% endfor %}
                            {% include "layouts/paging.volt" %}
                        </div>
                    </div>
                </div>
                <!---->
                <div class="box-ct">
                    <div class="title-name">
                        Tin mới
                    </div>
                    <div class="list-ct-news">
                        <div class="row">
                            <div class="col-md-7 col-sm-7 col-xs-12 col-left">
                                <div class="avt">
                                    <img src="{{ articel_new['avatar'] }}" alt="{{ articel_new['name'] }}">
                                </div>
                                <div class="ct">
                                    <div class="name">
                                        <a href="/articel/detail/{{ articel_new['id'] }}">{{ articel_new['name'] }}</a>
                                    </div>
                                    <div class="date">
                                        <span>{{ articel_new['datecreate'] }}</span>
                                    </div>
                                    <div class="txt">
                                        {{ articel_new['title'] }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-12 col-right">
                                <div class="list-n">
                                    <ul>
                                        {% for item in list_articel %}
                                            <li><a href="/articel/detail/{{ item['id'] }}">{{ item['name'] }}</a></li>
                                        {% endfor %}
                                    </ul>
                                </div>
                            </div>
                            <!---->
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<!-- footer -->
{% include "layouts/footer.volt" %}