{% set product = object['product'] %}
<div class="modal-dialog">
    <button type="button" class="modal__close" data-dismiss="modal"><span
                class="icon -ap icon-ion-android-close"></span></button>
    <div class="row">
        <div class="product__view__image col-md-5 col-sm-6 col-xs-12">
            <div class="row">
                <div class="col-sm-9 col-xs-12">
                    <div class="product__view__image--list">
                        <a href="{{ product['avatar'] }}" data-fancybox="gallery" class="fancybox">
                            <img src="{{ product['avatar'] }}" class="elevate__zoom"
                                 data-zoom-image="{{ product['avatar'] }}" id="elevate__zoom" title="product-2">
                        </a>
                        <div class="hidden">
                            {% for item in product['slide_show'] %}
                                <a href="{{ item }}"
                                   data-fancybox="gallery" class="fancybox">
                                    <img src="{{ item }}"
                                         alt="">
                                </a>
                            {% endfor %}
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div class="product__view__image--thumb" id="image-additional-carousel">
                        <div class="item">
                            {% for item in product['slide_show'] %}
                                <a href="{{ item }}"
                                   data-image="{{ item }}"
                                   class="elevate__zoom"><img
                                            src="{{ item }}"
                                            data-zoom-image="{{ item }}"
                                            title="product-2" alt="">
                                </a>
                            {% endfor %}
                        </div>
                        <div class="item">
                            {% for item in product['slide_show'] %}
                                <a href="{{ item }}"
                                   data-image="{{ item }}"
                                   class="elevate__zoom"><img
                                            src="{{ item }}"
                                            data-zoom-image="{{ item }}"
                                            title="product-2" alt="">
                                </a>
                            {% endfor %}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product__view__content col-md-7 col-sm-6 col-xs-12">
            <h2 class="product__view__name">{{ product['name'] }}</h2>
            <div class="product__price product__price__view">
                <span class="product__price--txt">Giá bán</span>
                {% if product['type_discount'] == 1 %}
                    <div class="product__price__old">{{ number_format(product['price']) }}<span class="unit">đ</span></div>
                    <div class="product__price__regular">{{ number_format(product['price'] - product['discount']) }} <span
                                class="unit">đ</span></div>
                {% else %}
                    <div class="product__price__old">{{ number_format(product['price']) }}<span class="unit">đ</span></div>
                    <div class="product__price__regular">{{ number_format(product['price']*(100-product['discount'])/100) }} <span
                                class="unit">đ</span></div>
                {% endif %}
            </div>
            <div class="product__view__info">
                <p class="info">Danh mục : <a href="#">{{ product['category']['name'] }}</a></p>
                <p class="info">Tình trạng: <span
                            class="name">{{ product['quantity'] >0 ? 'Còn hàng' : 'Liên hệ' }}</span></p>
            </div>
            <div class="product__view__desc">
                <div class="product__desc__title bold">Mô tả:</div>
                <div class="product__shortdesc__txt">
                    {{ product['desc'] }}
                </div>
            </div>
            <div class="product__view__color">
                <span class="name">Màu sắc :</span>
                <div class="item " style="background: #faebc7;">
                    <label>{{ product['properties']['color'] }}</label>
                </div>
            </div>
            <div class="product__quantity product__view__quantity">
                <div class="name">Số lượng:</div>
                <button class="btn-minus btn">
                    -
                </button>
                <input type="text" class="input-number" value="1">
                <button class="btn-plus btn">
                    +
                </button>
                <div class="clearfix"></div>
            </div>
            <div class="row form-group">
                <a id="add_cart" product_id="{{ product['id'] }}"
                   product_price="{{ product['type_discount'] == 1 ? product['price'] - product['discount'] : product['price']*(100-product['discount'])/100 }}"
                   class="product__view__button__cart pointer add_to_cart"><span
                            class="icon icon fa fa-shopping-basket"></span>Thêm vào giỏ</a>
            </div>

        </div>
    </div>
</div>