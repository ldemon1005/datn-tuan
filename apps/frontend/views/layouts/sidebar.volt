<div class="box category">
    <div class="title-name">
        Danh mục sản phẩm
    </div>
    <div class="list">
        <ul>
            {% for item in category_sidebar %}
                <li><a href="/product/product_category/{{ item['id'] }}">{{ item['name'] }}</a></li>
            {% endfor %}
        </ul>
    </div>
</div>