<footer class="footer">
    <!-- End topfoter -->
    <!-- footer -->
    <div class="container">
        <div class="footer__box">
            <div class="row ">
                <div class="col-md-4  col-sm-4 col-xs-12">
                    <h3 class="footer__title ">Trang Bán Đồ Gỗ</h3>
                    <div class="list-menu-ft">
                        <ul>
                            <li><a href="/index"><i class="fa fa-check" aria-hidden="true"></i> Trang chủ</a></li>
                            <li><a href="/agence"><i class="fa fa-check" aria-hidden="true"></i> Giới thiệu</a></li>
                            <li><a href="/product"><i class="fa fa-check" aria-hidden="true"></i> Sản phẩm</a></li>
                            <li><a href="/articel"><i class="fa fa-check" aria-hidden="true"></i> Tin tức</a></li>
                        </ul>
                    </div>
                    {#<h3 class="footer__title ">Đăng ký nhận tin tức</h3>#}
                    {#<div class="list-menu-ft">#}
                        {#<p class="note-sbmail">Theo dõi bản tin của chungst ôi với những tin tức mới nhất và chác chương trình khuyến mại</p>#}
                        {#<div class="sub frm-regemail">#}
                            {#<input type="text" class="form-control" placeholder="Nhập email của bạn">#}
                            {#<button class="btn btn-regemail">Đăng ký</button>#}
                        {#</div>#}
                    {#</div>#}
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <h3 class="footer__title ">Tin tức</h3>
                    <div class="list-news-ft">
                        <ul>
                            {% for item in articel_footer %}
                                <li>
                                    <div class="avt">
                                        <a href="/articel/detail/{{ item['id'] }}"><img src="{{ item['avatar'] }}" alt="{{ item['name'] }}"></a>
                                    </div>
                                    <div class="ct">
                                        <p class="name"><a href="/articel/detail/{{ item['id'] }}">{{ item['name'] }}</a></p>
                                        <p class="date"><i class="fa fa-calendar" aria-hidden="true"></i> {{ item['datecreate'] }}</p>
                                    </div>
                                </li>
                            {% endfor %}
                        </ul>
                    </div>
                </div>
                <div class="col-md-4  col-sm-4 col-xs-12">
                    <h3 class="footer__title ">Liên hệ</h3>
                    {% for item in contact %}
                        <div class="contact">
                            <p class="name-comp">{{ item['name'] }}</p>
                            <p class="info ">
                                <span class="icon -ap icon-location3"></span>
                                <span class="name-header texU disb fow">Địa chỉ: </span>
                                <span class="disb">{{ item['address'] }}</span>
                            </p>
                        </div>
                        <div class="contact">
                            <p class="info ">
                                <span class="icon -ap  icon-envelope2"></span>
                                <span class="name-header texU disb fow">Email:</span>
                                <span class="disb">{{ item['email'] }}</span>
                            </p>
                        </div>
                        <div class="contact">
                            <p class="info ">
                                <span class="icon fa fa-phone"></span>
                                <span class="name-header texU disb fow"> Phone: </span>
                                <span class="disb">{{ item['phone'] }}</span>
                            </p>
                        </div>
                    {% endfor %}
                </div>
            </div>
        </div>
    </div>
    <!-- End footer -->
    <!-- Copyright -->
    <div class="bottom">
        <div class="container">
            <div class="pull-left">
                <div class="copyright">
                    Copyright ® 2017 <a href="" class="url">ABC</a>
                </div>
            </div>
        </div>
    </div>
    <!-- end copyright -->

</footer>