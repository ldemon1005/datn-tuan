{% include "layouts/header.volt" %}
{% include "layouts/banner.volt" %}

<h2 class="text-center">{{ this.flash.output() }}</h2>
<div class="sec list-product">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-3 col-left">
                {% include "layouts/sidebar.volt" %}
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9 col-right">
                <div class="box-contentp">
                    <div class="title-name">
                        <h1 class="name">{{ articel['name'] }}</h1>
                        <div class="news__date">
                            <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                            {{ articel['datecreate'] }}
                            <div class="share-social">
                                <a href="javascript:void(0)" target="_blank"><img src=/frontend_res/assets/uploads/socical_fb.jpg alt=""></a>
                                <a href="javascript:void(0)" title="+1">
                                    <img src="/frontend_res/assets/uploads/socical_gg.jpg" alt="Google+ +1 Button">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="content">
                        {{ articel['content'] }}
                    </div>
                </div>
                <!---->
            </div>
        </div>
    </div>
</div>

{% include "layouts/footer.volt" %}

<script>
    function share_fb(url) {
        window.open('https://www.facebook.com/sharer/sharer.php?u=' + url, 'facebook-share-dialog', "width=626, height=436")
    }
    function gPlus(url) {
        window.open(
            'https://plus.google.com/share?url=' + url,
            'popupwindow',
            'scrollbars=yes,width=800,height=400'
        ).focus();
        return false;
    }
</script>

