{% include "/layouts/header.volt" %}
{% include "/layouts/banner.volt" %}

<div class="sec list-product">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12 col-left">
                {% include "layouts/sidebar.volt" %}
            </div>
            <!--/-->
            <div class="col-md-9 col-sm-9 col-xs-12 col-right">
                <!---->
                <div class="box-contentp">
                    <div class="title-name">
                        <p class="name">Bài viết</p>
                    </div>
                    <div class="content list-ct-news">
                        {% if !list_articel %}
                            <span><i>Chưa có bài viết.</i></span>
                        {% endif %}
                        {% for item in list_articel %}
                            <div class="box-news">
                                <div class="avt">
                                    <img src="{{ item['avatar'] }}" alt="">
                                </div>
                                <div class="ct">
                                    <div class="name">
                                        <a href="/articel/detail/{{ item['id'] }}">{{ item['name'] }}</a>
                                    </div>
                                    <div class="date">
                                        <span>{{ item['datecreate'] }}</span>
                                    </div>
                                    <div class="txt">{{ item['title'] }}</div>
                                </div>
                            </div>
                        {% endfor %}
                    </div>
                </div>
                <!---->
                {% include "layouts/paging.volt" %}
                <!---->
            </div>
        </div>
    </div>
</div>

{% include "/layouts/footer.volt" %}
