{% include "/layouts/header.volt" %}
<div class="login-page">
    <div class="container">
        <div class="card card-container">
            {{ flash.output() }}
            <div class="image-user">
                <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"/>
            </div>
            <form class="form-login" method="post">
                <span id="reauth-email" class="reauth-email"></span>
                <div class="form-group">
                    <input type="text" name="username" id="inputEmail" class="form-control"
                           placeholder="Tên tài khoản" required autofocus>
                </div>
                <div class="form-group">
                    <input type="password" name="password" id="inputPassword" class="form-control"
                           placeholder="Mật khẩu" required>
                </div>
                <div id="remember" class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Ghi nhớ tài khoản
                    </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit" style="margin-bottom: 10px">
                    Đăng nhập
                </button>
            </form>
        </div>
    </div>
</div>
<style>
    .login-box{
        margin: 0px auto;
    }

    .wrapper{
        position: inherit;
    }
    .login-page .container{
        min-height: 915px;
    }
    .login-page .card-container{
        width: 400px;
        margin: 15% auto;
        background: #fff;
        height: auto;
        padding: 20px;
    }
    .image-user{
        text-align: center;
        margin-bottom: 20px;
    }
    .image-user img{
        border-radius: 100%;
        height: 122px;
    }
    .login-page .form-login .form-group input{
        height: 40px;
        border-radius: 5px;
    }
    .account_login__social .title {
        font-size: 12px;
        text-transform: uppercase;
        color: #aaaaaa;
        position: relative;
        text-align: center;
        margin-bottom: 10px;
    }
    .account_login__social .title::before {
        content: "";
        position: absolute;
        top: 0px;
        bottom: 0px;
        left: 0px;
        margin: auto;
        height: 1px;
        width: 100%;
        border-top: 1px solid #e6eaf1;
    }
</style>
