{% include "layouts/header.volt" %}
<!-- End menu -->

{% if (step == 1)  %}
    {% include 'shopping/shop_cart_1.volt' %}
{% elseif step == 2 %}
    {% include 'shopping/shop_cart_2.volt' %}
{% elseif step == 3 %}
    {% include 'shopping/shop_cart_3.volt' %}
{% else %}
    {% include'shopping/shop_cart_4.volt' %}
{% endif %}

{% include "layouts/footer.volt" %}