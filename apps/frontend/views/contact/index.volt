{% include "/layouts/header.volt" %}

<div class="sec sec__contact">
    <div class="container">
        <div class="contact-list__head">
            <h2 class="contact-list__title">
                Liên hệ
            </h2>
        </div>
        <div class="row">
            <div class="list-contact col-md-5 col-sm-6 col-xs-12">
                {% for item in list_contact %}
                    <div class="item">
                        <div class="title-comp">
                            {{ item['name'] }}
                        </div>
                        <div class="contact">
                            <p class="info ">
                                <span class="icon -ap icon-old-phone"></span>
                                <span class="name-header">Điện thoại: </span>
                                <span class="disb">{{ item['phone'] }}</span>
                            </p>
                        </div>
                        <div class="contact">
                            <p class="info ">
                                <span class="icon -ap icon-location3"></span>
                                <span class="name-header texU disb fow">Địa chỉ: </span>
                                <span class="disb">{{ item['address'] }}</span>
                            </p>
                        </div>
                        <div class="contact">
                            <p class="info ">
                                <span class="icon -ap  icon-envelope2"></span>
                                <span class="name-header texU disb fow">Email:</span>
                                <span class="disb">{{ item['email'] }}</span>
                            </p>
                        </div>
                        <div class="contact">

                            <p class="info ">
                                <span class="icon -ap icon-access_time"></span>
                                <span class="name-header texU disb fow">Thời gian mở cửa: </span>
                                <span class="disb">Mon - Sat : 9:00 am - 22:00 pm</span>
                            </p>
                        </div>
                    </div>
                {% endfor %}
                <div class="item">
                    <div class="link-map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8858.811083050541!2d105.84461184784595!3d21.005811569826868!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76ccab6dd7%3A0x55e92a5b07a97d03!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBCw6FjaCBraG9hIEjDoCBO4buZaQ!5e0!3m2!1svi!2s!4v1521979280392" width="400" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="form-contact col-md-7 col-sm-6 col-xs-12">
                <form id="contact_info" action="/contact/create" method="post">
                    <div class="form-group">
                        {{ flash.output() }}
                    </div>
                    <div class="form-group">
                        <div class="label">Họ và tên</div>
                        <input name="contact_customer[fullname]" type="text" class="form-control" placeholder="Họ và tên...">
                    </div>
                    <div class="form-group">
                        <div class="label">Email</div>
                        <input name="contact_customer[email]" type="email" class="form-control" placeholder="Email...">
                    </div>
                    <div class="form-group">
                        <div class="label">Số điện thoại</div>
                        <input name="contact_customer[phone]" type="text" class="form-control" placeholder="Số điện thoại...">
                    </div>
                    <div class="form-group">
                        <div class="label">Nội dung:</div>
                        <textarea  name="contact_customer[content]" id="" cols="30" rows="10" class="form-control" placeholder="Nhập nội dung"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-submit pull-right">Gửi</button>
                    </div>
                </form>

            </div>
        </div>

    </div>
</div>