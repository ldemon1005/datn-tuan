{% include "/layouts/header.volt" %}


<div class="sec sec__contact">
    <div class="container">
        <div class="contact-list__head">
            <h2 class="title__main"><span>Giới thiệu</span></h2>
        </div>
        <div class="about-list-wrap">
            <div class="about-wrap__title">
                Cửa hàng vật dụng gia đình
            </div>
            {#<div class="infor-about">#}
                {#<div class="contact">#}
                    {#<p class="info ">#}
                        {#<span class="icon -ap icon-old-phone"></span>#}
                        {#<span class="name-header">Điện thoại: </span>#}
                        {#<span class="disb">08.68.33.1111 - 08.68.99.1111</span>#}
                    {#</p>#}
                {#</div>#}
                {#<div class="contact">#}
                    {#<p class="info ">#}
                        {#<span class="icon -ap icon-location3"></span>#}
                        {#<span class="name-header texU disb fow">Địa chỉ: </span>#}
                        {#<span class="disb">15A Nguyễn Khang, P.Yên Hòa, Q.Cầu Giấy, TP.HN</span>#}
                    {#</p>#}
                {#</div>#}
                {#<div class="contact">#}
                    {#<p class="info ">#}
                        {#<span class="icon -ap  icon-envelope2"></span>#}
                        {#<span class="name-header texU disb fow">Email:</span>#}
                        {#<span class="disb">biznet@8386.vn</span>#}
                    {#</p>#}
                {#</div>#}
                {#<div class="contact">#}
                    {#<p class="info ">#}
                        {#<span class="icon -ap icon-access_time"></span>#}
                        {#<span class="name-header texU disb fow">Thời gian mở cửa: </span>#}
                        {#<span class="disb">Mon - Sat : 9:00 am - 22:00 pm</span>#}
                    {#</p>#}
                {#</div>#}
            {#</div>#}
            <div class="content-wrap">
                <p><img src="/frontend_res/assets/uploads/slide.jpg" alt=""></p>
                <p>Ngày nay, nhiều hãng giày khi bán hàng cho khách thường đựng giày trong một chiếc hộp rất đẹp, có kèm cả túi chống ẩm. Khi mua hàng về, bạn đừng vứt hộp đi. Chẳng hạn, đôi giày đó bạn chỉ đi vào mùa lạnh, thì khi không dùng nữa, hãy nhét giấy vụn vào trong giày để giữ cho giầy không bị biến dạng, rồi bỏ giày vào hộp cùng gói hút ẩm. Như vậy, đôi giày của bạn sẽ yên vị trong hộp nhiều tháng trời mà không ảnh hưởng tới chất lượng của da.<br/>
                    - Làm mềm giày da Đôi giày da để trong xó tủ nào đó, bỗng một ngày nọ bạn muốn dùng đến nó nhưng da đã bị cứng, co lại, khi đi có cảm giác đau chân. Để làm mềm da, hãy thoa một lớp kem vaseline lên giày trước khi đánh xi, giày sẽ mềm trở lại. Hoặc sau khi lấy giày trong tủ ra, bạn dùng giẻ mềm thấm nước vắt khô, lau toàn bộ đôi giày rồi để sau một đêm, da giày sẽ mềm hơn. Để da giày bền lâu, bạn hạn chế làm ướt giày. Khi giày bị ướt không nên hơ trước ngọn lửa hoặc phơi nắng, nó sẽ làm giày bị cứng và co lại, chỉ nên phơi giày ướt ở nơi râm mát, sau khi giày khô thì đánh xi để làm cho da mềm trở lại.<br/>
                    - Khử mùi hôi trong giày Giày dùng cả ngày thường bị mồ hôi làm ẩm ướt, gây mùi hôi. Nên đặt túi đựng viên chống ẩm vào trong giày để hút ẩm và rắc phấn rôm để khử mùi. Để hạn chế mùi hôi và sự ẩm ướt, hãy chọn tất chân loại tốt, có khả năng hút ẩm cao. Dùng lót giày khử mùi cũng là một phương pháp tốt.</p>
            </div>
        </div>
    </div>
</div>

{% include "/layouts/footer.volt" %}
