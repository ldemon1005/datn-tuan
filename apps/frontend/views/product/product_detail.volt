{% include "layouts/header.volt" %}
<!-- End menu -->
{% include "layouts/banner.volt" %}
<div class="sec list-product">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12 col-left">
                {% include "layouts/sidebar.volt" %}
            </div>
            <!--/-->
            <div class="col-md-9 col-sm-9 col-xs-12 col-right">
                <!---->
                <div class="box-category">
                    <div class="ct-detail-pr">
                        <div class="row ct-detail-top">
                            <div class="product__view__image col-md-6 col-sm-6 col-xs-12">
                                <div class="row">
                                    <div class="col-sm-9 col-xs-12">
                                        <div class="product__view__image--list">
                                            <a href="{{ product['avatar'] }}" data-fancybox="gallery" class="fancybox">
                                                <img src="{{ product['avatar'] }}" class="elevate__zoom" data-zoom-image="{{ product['avatar'] }}" id="elevate__zoom"  title="product-2">
                                            </a>
                                            <div class="hidden">
                                                {% for item in product['slide_show'] %}
                                                    <a href="{{ item }}" data-fancybox="gallery" class="fancybox">
                                                        <img src="{{ item }}" alt="">
                                                    </a>
                                                {% endfor %}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="product__view__image--thumb" id="image-additional-carousel">
                                            <div class="item">
                                                {% for item in product['slide_show'] %}
                                                    <a href="{{ item }}" data-fancybox="gallery" class="fancybox">
                                                        <img src="{{ item }}" alt="">
                                                    </a>
                                                {% endfor %}
                                            </div>
                                            <div class="item">
                                                {% for item in product['slide_show'] %}
                                                    <a href="{{ item }}" data-fancybox="gallery" class="fancybox">
                                                        <img src="{{ item }}" alt="">
                                                    </a>
                                                {% endfor %}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!---->
                            <div class="col-md-6 col-sm-6 col-xs-12 product__view__content">
                                <div class="name text-primary">{{ product['name'] }}</div>
                                <div class="product__price product__price__view">
                                    <span class="product__price--txt text-primary">Giá bán</span>
                                    {% if product['type_discount'] == 1  %}
                                        <div class="product__price__old">{{ number_format(product['price']) }}<span class="unit">đ</span></div>
                                        <div class="product__price__regular">{{ number_format(product['price'] - product['discount']) }} <span class="unit">đ</span></div>
                                    {% else %}
                                        <div class="product__price__old">{{ number_format(product['price']) }}<span class="unit">đ</span></div>
                                        <div class="product__price__regular">{{ number_format(product['price']*(100-product['discount'])/100) }} <span class="unit">đ</span></div>
                                    {% endif %}
                                </div>
                                <div class="product__view__info">
                                    <p class="info text-primary">Tình trạng: <span class="name">{{ product['quantity'] > 0 ?'Còn hàng' : 'Liên hệ' }}</span></p>
                                </div>
                                <div class="product__view__info">
                                    <p class="info text-primary">Mô tả: <span>{{ product['desc'] }}</span></p>
                                </div>
                                <div class="product__view__des">
                                    <a id="add_cart" product_id="{{ product['id'] }}"
                                       product_price="{{ product['type_discount'] == 1 ? product['price'] - product['discount'] : product['price']*(100-product['discount'])/100 }}"
                                       class="product__view__button__cart pointer add_to_cart"><span
                                                class="icon icon fa fa-shopping-basket"></span>Thêm vào giỏ</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product__detail__content">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#home">Tổng quan</a></li>
                        </ul>

                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <article class="article">
                                    <div class="std">
                                        {{ product['caption'] }}
                                        <p><img src="{{ product['avatar'] }}"></p>

                                        <p><strong>Đồ gỗ phong thủy hiện mang phong cách hiện đại</strong></p>

                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                    <!---->
                </div>
                <!---->
                <div class="box-ct">
                    <div class="title-name">
                        Sản phẩm ưa chuộng
                    </div>
                    <div class="list-ct">
                        <div class="row">
                            {% for item in product_popularity %}
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="product__item">
                                        <div class="w">
                                            <div class="product__image">
                                                <a href="#" class="product__link"><img src="{{ item['avatar'] }}" alt="name your product"></a>
                                                <div class="product-item__actions">
                                                    <a class="btn btn-detail" data-toggle="tooltip" data-placement="top" href="/product/product_detail/{{ item['id'] }}" title="Chi tiết"><span class="-ap  icon-search2"></span></a>
                                                    <a class="btn btn-quickview" id="quick_view" product="{{ item['id'] }}" data-toggle="modal" data-target="#QuickView" data-placement="top" title="Xem nhanh"><span class="fa fa-eye"></span></a>
                                                </div>
                                                <div class="product__overlay"></div>
                                                {% if item['type_discount'] == 2 %}
                                                    <div class="product__sale">{{ item['discount'] }}%</div>
                                                {% endif %}
                                            </div>
                                            <h3 class="product__name"><a href="#">{{ item['name'] }}</a></h3>
                                            <div class="product__price">
                                                {% if item['type_discount'] == 1  %}
                                                    <div class="product__price__old">{{ item['price'] }}<span class="unit">đ</span></div>
                                                    <div class="product__price__regular">{{ item['price'] - item['discount'] }} <span class="unit">đ</span></div>
                                                {% else %}
                                                    <div class="product__price__old">{{ item['price'] }}<span class="unit">đ</span></div>
                                                    <div class="product__price__regular">{{ item['price']*(100-item['discount'])/100 }} <span class="unit">đ</span></div>
                                                {% endif %}

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {% endfor %}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- footer -->
{% include "layouts/footer.volt" %}