{% include "layouts/header.volt" %}
<!-- End menu -->
{% include "layouts/banner.volt" %}
<div class="sec list-product">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12 col-left">
                {% include "layouts/sidebar.volt" %}
            </div>
            <!--/-->
            <div class="col-md-9 col-sm-9 col-xs-12 col-right">
                <!---->
                <div class="box-ct">
                    <div class="list-ct">
                        <div class="row">
                            {% for item in list_product %}
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="product__item">
                                        <div class="w">
                                            <div class="product__image">
                                                <a href="#" class="product__link"><img style="max-height: 80px" src="{{ item['avatar'] }}" alt="name your product"></a>
                                                <div class="product-item__actions">
                                                    <a class="btn btn-detail" data-toggle="tooltip" data-placement="top" href="/product/product_detail/{{ item['id'] }}" title="Chi tiết"><span class="-ap  icon-search2"></span></a>
                                                    <a class="btn btn-quickview" id="quick_view" product="{{ item['id'] }}" title="Xem nhanh"><span class="fa fa-eye"></span></a>
                                                </div>
                                                <div class="product__overlay"></div>
                                                {% if item['type_discount'] == 2 %}
                                                    <div class="product__sale">{{ number_format(item['discount']) }}%</div>
                                                {% endif %}
                                            </div>
                                            <h3 class="product__name"><a href="#">{{ item['name'] }}</a></h3>
                                            <div class="product__price">
                                                {% if item['type_discount'] == 1  %}
                                                    <div class="product__price__old">{{ number_format(item['price']) }}<span class="unit">đ</span></div>
                                                    <div class="product__price__regular">{{ number_format(item['price'] - item['discount']) }} <span class="unit">đ</span></div>
                                                {% else %}
                                                    <div class="product__price__old">{{ number_format(item['price']) }}<span class="unit">đ</span></div>
                                                    <div class="product__price__regular">{{ number_format(item['price']*(100-item['discount'])/100) }} <span class="unit">đ</span></div>
                                                {% endif %}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {% endfor %}
                            {% include "layouts/paging.volt" %}
                        </div>
                    </div>
                </div>
                <!---->

            </div>
        </div>
    </div>
</div>


<!-- footer -->
{% include "layouts/footer.volt" %}