<div class="sidebar-profile">
    <div class="profiles">
        <div class="avatar" id="avatar_customer">
            <a href="#"><img src="{{ user['avatar'] ? user['avatar'] : '//ssl.gstatic.com/accounts/ui/avatar_2x.png' }}" alt="{{ user['fullname'] }}"></a>
        </div>
        <div class="headline">Tài khoản của</div>
        <div class="username">{{ user['email'] }}</div>
    </div>
    <div class="user-profile__menu">
        <ul class="user-profile__menu__wrap">
            <li class="user-profile__item {{  router.getActionName() == null ? 'active' : '' }}">
                <a href="/customer" class="user-profile__link"><span class="-ap icon-user6 icon"></span> <span class="txt">Thông tin tài khoản</span></a>
            </li>
            <li class="user-profile__item {{  router.getControllerName() == 'customer' and (router.getActionName() == 'order' or router.getActionName() == 'order_detail')  ? 'active' : '' }}">
                <a href="/customer/order" class="user-profile__link"><span class="-ap icon-file-text icon"></span> <span class="txt">Quản lý đơn hàng</span></a>
            </li>
            <li class="user-profile__item {{  router.getControllerName() == 'customer' and router.getActionName() == 'update_password'  ? 'active' : '' }}">
                <a href="/customer/update_password" class="user-profile__link"><span class="fa fa-exchange icon"></span> <span class="txt">Thay đổi mật khẩu</span></a>
            </li>
        </ul>
    </div>
</div>