{% include "layouts/header.volt" %}
<div class="breadcrumb">
    <div class="container">
        <a href="#" class="li">Trang chủ </a>
        <a href="/customer" class="li">Tài khoản</a>
        <a href="/order" class="li">Quản lý đơn hàng</a>
        <span class="li active">Chi tiết đơn hàng</span>
    </div>
</div>
<div class="user-profile__wrap sec">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
                {% include "customer/sidebar-customer.volt" %}
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="wrap__profile__info">
                    <h2 class="title-profile">Đơn hàng: {{ order['order_code'] }}</h2>
                    <div class="profile-order">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th class="xdot">Tên sản phẩm</th>
                                    <th class="xdot">Giá bán</th>
                                    <th>Số lượng</th>
                                    <th class="xdot">Thành tiền</th>
                                </tr>
                                {% for item in list_product %}
                                    <tr>
                                        <td>
                                            <div class="xdot code">{{ item['name'] }}</div>
                                        </td>
                                        <td>{{ number_format(item['price']) }}</td>
                                        <td>{{ item['quantity']}}</td>
                                        <td>
                                            {% if item['discount_type'] == 1 %}
                                                <div class="product__price xdot">{{ number_format(item['price']*item['quantity']*(100-item['discount'])/100) }} <span class="unit">đ</span></div>
                                            {% else %}
                                                <div class="product__price xdot">{{ number_format(item['price']*item['quantity'] - item['discount']) }} <span class="unit">đ</span></div>
                                            {% endif %}

                                        </td>
                                    </tr>
                                {% endfor %}
                                <tr>
                                    <td><b>Phí vận chuyển</b></td>
                                    <td></td>
                                    <td></td>
                                    <td ><b>{{ number_format(order['vtp_info']['vtp_fee']) }}</b></td>
                                </tr>
                                <tr>
                                    <td><b>Tổng</b></td>
                                    <td></td>
                                    <td>{{ total_quantity }}</td>
                                    <td ><b>{{ number_format(order['money']) }}</b></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{% include "layouts/footer.volt" %}