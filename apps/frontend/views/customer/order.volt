{% include "layouts/header.volt" %}
<div class="breadcrumb">
    <div class="container">
        <a href="/" class="li">Trang chủ </a>
        <a href="/customer" class="li">Tài khoản</a>
        <span class="li active">Quản lý đơn hàng</span>
    </div>
</div>
<div class="user-profile__wrap sec">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
                {% include "customer/sidebar-customer.volt" %}
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="wrap__profile__info">
                    <h2 class="title-profile">Quản lý đơn hàng</h2>
                    <div class="profile-order">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th class="xdot">Mã đơn hàng</th>
                                    <th>Ngày tạo đơn</th>
                                    <th>Mã vận đơn</th>
                                    <th class="xdot"> Tổng tiền</th>
                                    <th>Trạng thái</th>
                                </tr>
                                    {% for order in list_order %}
                                        <tr>
                                            <td>
                                                <div class="xdot code"><a href="/customer/order_detail/{{ order['id'] }}" style="color: #00acea">{{ order['order_code'] }}</a></div>
                                            </td>
                                            <td>{{ order['datecreate'] }}</td>
                                            <td>{{ order['vtp_code'] }}</td>
                                            <td>
                                                <div class="product__price xdot">{{ number_format(order['money']) }}<span class="unit">đ</span></div>
                                            </td>
                                            <td>
                                                <div class="status status-new">{{ order['status'] }}</div>
                                            </td>
                                        </tr>
                                    {% endfor %}
                            </table>
                            {% include "layouts/paging.volt" %}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{% include "layouts/footer.volt" %}