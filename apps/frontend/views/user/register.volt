{% include "/layouts/header.volt" %}
{% include "layouts/sidebar.volt" %}
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Thêm mới tài khoản </h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" id="form-register" action="/user/register" method="post" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 text-primary">
                                </label>
                                <div class="col-sm-5">
                                    {{ flash.output() }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Username</label>

                            <div class="col-sm-10">
                                <input type="text" name="user[username]" class="form-control" id="inputEmail3" placeholder="Username">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Password 1</label>

                            <div class="col-sm-10">
                                <input type="password" name="user[password]" class="form-control" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Password 2</label>

                            <div class="col-sm-10">
                                <input type="password" name="user[password2]" class="form-control" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Họ tên</label>

                            <div class="col-sm-10">
                                <input type="text" name="user[fullname]" class="form-control" placeholder="Họ và tên">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Số điện thoại</label>

                            <div class="col-sm-10">
                                <input type="text" name="user[phone]" class="form-control" placeholder="Số điện thoại">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Email</label>

                            <div class="col-sm-10">
                                <input type="email" name="user[email]" class="form-control" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Avatar</label>
                            <div class="col-sm-10 form-group">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <img style="margin-left: 15px;height: 120px;width: 120px" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" id="avatar_img" class="profile-img-card avatar">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="file" id="file" name="user[avatar]">
                                        <em class="color-gray-light">(Hỗ trợ định dạng *.gif, *.png, *.jpg, *.bmp ) </em>
                                        <hr/>
                                        <button type="button" class="btn btn-primary" id="uploadAvatar">Tải ảnh</button>
                                    </div>
                                    <input hidden id="avatar_url" name="user[avatar]">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Ngày sinh</label>

                            <div class="col-sm-10">
                                <input type="text" name="user[dob]" class="form-control" placeholder="dd/mm/yyyy">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Trạng thái</label>

                            <div class="col-sm-10">
                                <div class="row">
                                    <label class="col-sm-2 text-primary">
                                        <input value="2" type="radio" name="user[status]" checked>
                                        Hoạt động
                                    </label>
                                    <label class="col-sm-2 text-primary">
                                        <input value="1" type="radio" name="user[status]">
                                        Không hoạt động
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Địa chỉ</label>

                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <select name="user[city_id]" id="province_id" class="form-control selectpicker" data-live-search="true" style="width: 100%;">
                                            <option selected="selected">--Tỉnh/thành--</option>
                                            {% for item in province %}
                                                <option value="{{ item.id }}">{{ item.name }}</option>
                                            {% endfor %}
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <select name="user[district_id]" id="district_id" class="form-control selectpicker" data-live-search="true" style="width: 100%;">
                                            <option selected="selected">--Quận/huyện--</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <select name="user[ward_id]" id="ward_id" class="form-control selectpicker" data-live-search="true" style="width: 100%;">
                                            <option selected="selected" class="text-center">--Xã/phường--</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Loại</label>

                            <div class="col-sm-10">
                                <div class="row">
                                    <label class="col-sm-2 text-primary">
                                        <input value="2" type="radio" name="user[type]" checked>
                                        Tài khoản thường
                                    </label>
                                    <label class="col-sm-2 text-primary">
                                        <input value="1" type="radio" name="user[type]">
                                        Tài khoản admin
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right" style="margin-right: 10px">Tạo</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
{% include "/layouts/footer.volt" %}

