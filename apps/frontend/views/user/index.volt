{% include "/layouts/header.volt" %}
{% include "layouts/sidebar.volt" %}

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Danh sách tài người dùng</h3>
                </div>
                <!-- /.box-header -->
                <div id="sec__customer" class="sec__page">
                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Username</th>
                            <th>Họ tên</th>
                            <th>Số điện thoại</th>
                            <th>Email</th>
                            <th>Ngày sinh</th>
                            <th>Ngày tạo</th>
                            <th>Địa chỉ</th>
                            <th>Trạng thái</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for item in ListUser %}
                            <tr>
                                <td class="text-center">{{ item['username'] }}</td>
                                <td class="text-center">{{ item['fullname'] }}</td>
                                <td class="text-center">{{ item['phone'] }}</td>
                                <td class="text-center">{{ item['email'] }}</td>
                                <td class="text-center">{{ item['dob'] }}</td>
                                <td class="text-center">{{ item['datecreate'] }}</td>
                                <td class="text-center">{{ item['address'] }}</td>
                                <td class="text-center">
                                    <button class="btn btn-block btn-sm {{ itme['status == 1'] ? 'btn-default' : 'btn-success' }}">{{ itme['status == 1'] ? 'Không hoạt động' : 'Hoạt động' }}</button>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

{% include "/layouts/footer.volt" %}
