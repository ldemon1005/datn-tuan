<?php
namespace DoGo\Frontend\Models;
/**
 * Created by PhpStorm.
 * User: ldemon1005
 * Date: 2/22/2018
 * Time: 9:31 PM
 */

class Wards extends BaseModel
{
    /**
     *
     * @var integer
     */
    public $district_id;

    /**
     *
     * @var integer
     */
    public $ward_id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var integer
     */
    public $fullname;

    /**
     *
     * @var integer
     */
    public $vtp_id;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'wards';
    }

    /**
     * @return int
     */
    public function getDistrictId()
    {
        return $this->district_id;
    }

    /**
     * @param int $district_id
     */
    public function setDistrictId($district_id)
    {
        $this->district_id = $district_id;
    }

    /**
     * @return int
     */
    public function getWardId()
    {
        return $this->ward_id;
    }

    /**
     * @param int $ward_id
     */
    public function setWardId($ward_id)
    {
        $this->ward_id = $ward_id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * @param int $fullname
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    }

    /**
     * @return int
     */
    public function getVtpId()
    {
        return $this->vtp_id;
    }

    /**
     * @param int $vtp_id
     */
    public function setVtpId($vtp_id)
    {
        $this->vtp_id = $vtp_id;
    }

}