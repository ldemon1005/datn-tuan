<?php
/*
 * Modified: prepend directory path of current file, because of this file own different ENV under between Apache and command line.
 * NOTE: please remove this comment.
 */

defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/app');

return new \Phalcon\Config([
    'database' => array(
        'adapter'     => 'Mysql',
        'host'        => 'localhost',
        'username'    => 'root',
        'password'    => 'root',
        'dbname'      => 'tuandv',
        'charset'     => 'utf8',
    ),
    'cache' => [
        'lifetime' => 86400,
        'adapter' => 'File',
        'cacheDir' => BASE_PATH . '/storage/cache/',
    ],
    'application' => [
        'controllersDir' => __DIR__ . '/../controllers/',
        'modelsDir' => __DIR__ . '/../models/',
        'viewsDir' => __DIR__ . '/../views/',
        'cacheDir' => __DIR__ . '/../cache/',
        'libraryDir' => __DIR__ . '/../library/',

        // This allows the baseUri to be understand project paths that are not in the root directory
        // of the webpspace.  This will break if the public/index.php entry point is moved or
        // possibly if the web server rewrite rules are changed. This can also be set to a static path.
        'baseUri' => preg_replace('/public([\/\\\\])index.php$/', '', $_SERVER["PHP_SELF"]),
    ],
    'nganluong' => [
        'URL_API' => 'https://sandbox.nganluong.vn:8088/nl30/checkout.api.nganluong.post.php',
        'RECEIVER' => 'ldemon1005@gmail.com',
        'MERCHANT_ID' => 45529,
        'MERCHANT_PASS' => '74e3d05a598cd7ac8a1013efafa5fc89',
        'URL_REDIRECT' => 'http://localhost:7777/shopping/ngan_luong',
        'URL_CANCEL' => 'http://localhost:7777/shopping/ngan_luong',
    ]
]);
