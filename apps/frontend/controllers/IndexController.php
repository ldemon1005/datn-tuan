<?php
namespace DoGo\Frontend\Controllers;
use DoGo\Frontend\Models\Category;
use DoGo\Frontend\Models\Product;
use DoGo\Frontend\Models\Articel;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
/**
 * Class IndexController
 * @property \Phalcon\Config $config
 */
class IndexController extends ControllerBase
{
    public function indexAction()
    {
        $product_popularity = $this->productPopularity();
        $this->view->product_popularity = $product_popularity;

        $this->listProduct();
    }

    function productPopularity(){
        $product = Product::find([
            'limit' => 4,
            'order' => 'buy_number desc'
        ]);
        return $product->toArray();
    }
    function listProduct(){
        $max = 8;
        $page = $this->request->getQuery('p','int',1);
        $list_product = new PaginatorModel([
            'data' => Product::find([
                'conditions' => 'status = :status: and del_flag = :del_flag:',
                'bind' => [
                    'status' => 2,
                    'del_flag' => Product::NOT_DELETED
                ],
                'order' => 'id desc'
            ]),
            "limit" => $max,
            "page"  => $page,
        ]);
        $product = [];
        foreach ($list_product->getPaginate()->items as $key=>$val){
            $product[] = $val->toArray();
        }
        $product_paging = Product::find([
            'conditions' => 'status = :status: and del_flag = :del_flag:',
            'bind' => [
                'status' => 2,
                'del_flag' => Product::NOT_DELETED
            ],
        ]);
        $pagination = [
            'total' => 0,
            'total_pages' => 1,
            'current' => 1,
            'current_link' => base_uri()."/index?"
        ];

        if($product_paging){
            $product_paging = $product_paging->toArray();
            $pagination = [
                'total' => count($product_paging),
                'total_pages' => ceil(count($product_paging)/$max),
                'current' => $page,
                'current_link' => base_uri()."/index?"
            ];
        }
        $this->view->list_product = $product;
        $this->view->Pagination = $pagination;
    }

    function quick_viewAction(){
        $id = $this->request->getPost('id');
        $product = Product::findById($id);
        if(!$product){
            return json_encode([
                'status' => 0,
                'message' => 'Không tìm thấy sản phẩm!'
            ]);
        }
        $product = $product->toArray();
        if($product['category_id']){
            $category = Category::findById($product['category_id']);
            if($category){
                $product['category'] = [
                    'id' => $category->getId(),
                    'name' => $category->getName()
                ];
            }else {
                $product['category'] = [
                    'id' => null,
                    'name' => ''
                ];
            }
        }
        $product['slide_show'] = json_decode($product['slide_show'],true);
        $product['properties'] = json_decode($product['properties'],true);
        $template = $this->render_template('index','quick_view',['product' => $product]);
        return json_encode([
            'content' => $template
        ]);

    }


}

