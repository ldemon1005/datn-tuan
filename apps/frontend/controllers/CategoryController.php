<?php
namespace DoGo\Frontend\Controllers;

use DoGo\Frontend\Models\Category;

/**
 * Class AuthController
 * @property \Phalcon\Config $config
 */
class CategoryController extends AuthorizedControllerBase
{

    public function indexAction()
    {
        if($this->request->isPost() && $this->request->getPost('category')){
            $category = $this->request->getPost('category');
            $category = Category::newInstance($category);
            if ($category->save() && $category->getName()) {
                $this->flash->success("Thêm mới danh mục thành công");
            } else {
                $this->flash->error("Thêm mới danh mục không thành công");
            }
        }
        $category = Category::find([
            'condition' => 'type = :type:',
            'bind' => [
                'type' => Category::TYPE_PRODUCT
            ],
            'order' => 'id desc'
        ]);
        $this->view->ListCategory = $category;
        $category = $category->toArray();
        $result = [];
        $result[] = [
            'id' => 0,
            'name' => 'root'
        ];
        $this->recusiveMenu($category,0,"",$result);
        foreach ($result as $key=>$value){
            if($value['parent_id'] != 0) $result[$key]['name'] = "|".$result[$key]['name'];
        }
        $this->view->MenuRecusive = $result;
    }
    public static function recusiveMenu($data,$parent_id = 0,$text = "",&$result){
        foreach ($data as $key => $item){
            if($item['parent_id'] == $parent_id){
                $item['name'] = $text.$item['name'];
                $result [] = $item;
                unset($data[$key]);
                self::recusiveMenu($data,$item['id'],$text."--",$result);
            }
        }
    }

    function updateAction($id){
        $category = Category::find([
            'condition' => 'type = :type:',
            'bind' => [
                'type' => Category::TYPE_PRODUCT
            ]
        ]);
        $category = $category->toArray();
        $result = [];
        $this->recusiveMenu($category,0,"",$result);
        foreach ($result as $key=>$value){
            if($value['parent_id'] != 0) $result[$key]['name'] = "|".$result[$key]['name'];
        }
        $this->view->MenuRecusive = $result;

        $category = Category::findById($id);
        if(!$category){
            $this->flash->error("Danh mục không tồn tại");
        }
        $this->view->detail_category = $category;
        if($this->request->isPost() && $this->request->getPost('category')){
            $category_data = $this->request->getPost('category');
            $category->setStatus($category_data['status']);
            $category->setName($category_data['name']);
            $category->setParentId($category_data['parent_id']);
            $category->setDesc($category_data['desc']);
            if ($category->save() && $category->getName()) {
                $this->flash->success("Cập nhật danh mục thành công");
                $this->response->redirect(base_uri().'/category');
            } else {
                $this->flash->error("Cập nhật danh mục không thành công");
                $this->response->redirect(base_uri().'/category/update');
            }
        }
    }

    function deleteAction($id){
        $category = Category::find();
        $category = $category->toArray();
        $result = [];
        $this->recusiveDelete($id,$category,$result);
        $t = 0;
        foreach ($result as $value){
            $category_del = Category::findById($value['id']);
            if($category_del->delete()) $t = 1;
        }
        $category_del_1 = Category::findById($id);
        if($category_del_1->delete()) $t = 1;
        if($t == 0){
            $this->flash->error("Xóa không thành công");
        }else {
            $this->flash->success("Xóa thành công");
        }
        $this->response->redirect(base_uri().'/category');
    }
    function recusiveDelete($id,$menu,&$result){
        foreach ($menu as $key => $val){
            if($val['parent_id'] == $id){
                $result [] = $val;
                unset($menu[$key]);
                self::recusiveMenu($val['id'],$menu,$result);
            }
        }
    }
}

