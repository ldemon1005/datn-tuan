<?php
namespace DoGo\Frontend\Controllers;

use DoGo\Frontend\Models\Districts;
use DoGo\Frontend\Models\Location;
use DoGo\Frontend\Models\User;
use DoGo\Frontend\Models\Wards;

/**
 * Class AuthController
 * @property \Phalcon\Config $config
 */
class UserController extends ControllerBase
{

    public function indexAction()
    {
        $users = User::find();
        if(!$users){
            $users = [];
        }
        $users = $users->toArray();
        foreach ($users as $key => $user){
            $users[$key]['dob'] = date("d/m/Y",$user['dob']);
            $users[$key]['datecreate'] = date("d/m/Y",$user['datecreate']);
            $city = Location::findById($user['city_id']);
            $district = Location::findById($user['district_id']);
            $ward = Location::findById($user['ward_id']);
            $address = $city->getName()."-".$district->getName()."-".$ward->getName();
            $users[$key]['address'] = $address;

        }
        $this->view->ListUser = $users;
    }

    function getListUser(){

    }

    /**
     * Đăng ký
     */
    function registerAction() {
        if($this->request->isPost()){
            $user = $this->request->getPost('user');
            $user = User::newInstance($user);
            $check = 0;
            $check = $this->validateCreate($user);

            $check_email_phone = User::findFirst([
                'conditions' => 'email = :email: or phone = :phone:',
                'bind' => [
                    'email' => $user->getEmail(),
                    'phone' => $user->getPhone()
                ]
            ]);
            if($check_email_phone){
                $check = 1;
                $this->flash->error( 'Email hoặc số điện thoại đã có người sử dụng!' );
            }
            $user->setDob(strtotime(str_replace("/", "-", $user->getDob())));
            if($check == 0){
                $user->create();
                $this->response->redirect(base_uri().'/user');
            }
        }
    }

    /**
     * Đăng nhập
     */
    function updateAction() {
        
    }

    /**
     * Lấy sản danh sách khu vực
     */
    function districtAction()
    {
        $id = $this->request->getPost('id');
        $district = Districts::find([
            'conditions' => 'city_id = :id:',
            'bind' => [
                'id' => $id
            ]
        ]);
       return json_encode($district->toArray());
    }

    /**
     * Lấy sản danh sách khu vực
     */
    function wardAction()
    {
        $id = $this->request->getPost('id');
        $ward = Wards::find([
            'conditions' => 'district_id = :id:',
            'bind' => [
                'id' => $id
            ]
        ]);
        return json_encode($ward);
    }
    function uploadAction(){
        if(isset($_POST) && isset($_FILES['file'])){
            $duoi = explode('.', $_FILES['file']['name']);
            $duoi = strtolower($duoi[(count($duoi)-1)]);
            if($duoi == 'jpg' || $duoi == 'png' || $duoi == 'gif'){
                if(move_uploaded_file($_FILES['file']['tmp_name'], BASE_PATH . '/public/uploads/' . $_FILES['file']['name'])){
                    return json_encode([
                        'code' => 1,
                        'url' => '../uploads/' . $_FILES['file']['name']
                    ]);
                } else{
                    return json_encode([
                        'code' => 0,
                    ]);
                }
            } else{
                die('Chỉ được upload ảnh');
            }
        }
    }

    function validateCreate($user){
        $check = 0;
        if(!$user->getEmail() && $check == 0){
            $check = 1;
            $this->flash->error( 'Thiếu email!' );
        }
        if(!$user->getUsername() && $check == 0){
            $check = 1;
            $this->flash->error( 'Thiếu username!' );
        }
        if(!$user->getPassword() && $check == 0){
            $check = 1;
            $this->flash->error( 'Thiếu mật khẩu!' );
        }
        if(!$user->getPhone() && $check == 0){
            $check = 1;
            $this->flash->error( 'Thiếu số điện thoại!' );
        }
        if(!$user->getFullname() && $check == 0){
            $check = 1;
            $this->flash->error( 'Thiếu họ và tên!' );
        }
        if((!$user->getDistrictId() || !$user->getCityId() || !$user->getWardId()) && $check == 0){
            $check = 1;
            $this->flash->error( 'Thiếu thông tin địa chỉ!' );
        }

        return $check;
    }
}

