<?php
namespace DoGo\Frontend\Controllers;
use DoGo\Frontend\Models\Contact;
use DoGo\Frontend\Models\ContactCustomer;

/**
 * Class IndexController
 * @property \Phalcon\Config $config
 */
class ContactController extends ControllerBase
{
    public function indexAction()
    {
        $contact = Contact::find();
        if($contact) $contact = $contact->toArray();
        $this->view->list_contact = $contact;
    }

    public function createAction(){
        if($this->request->isPost('contact_customer')){
            $contact = $this->request->getPost('contact_customer');
            $contact['datecreate'] = time();
            $contact = ContactCustomer::newInstance($contact);
            if($contact->save()){
                $this->flash->success("Gửi liên hệ thành công.");
            }else $this->flash->error("Gửi liên hệ không thành công");
        }
        $this->response->redirect(base_uri()."/contact");
    }
}

