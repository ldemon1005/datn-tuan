<?php
namespace DoGo\Frontend\Controllers;
use DoGo\Frontend\Models\Category;
use DoGo\Frontend\Models\Product;
use Phalcon\Paginator\Pager;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;

/**
 * Class IndexController
 * @property \Phalcon\Config $config
 */
class ProductController extends ControllerBase
{
    public function indexAction()
    {
        $max = 8;
        $page = $this->request->getQuery('p','int',1);
        $value = null;
        if($this->request->getPost('text_search')){
            $value = $this->request->getPost('text_search');
        }
        if($value){
            $list_product = new PaginatorModel([
                'data' => Product::find([
                    'conditions' => 'name like :value: and status = :status: and del_flag = :del_flag:',
                    'bind' => [
                        'value' => '%'.$value.'%',
                        'status' => 2,
                        'del_flag' => Product::NOT_DELETED
                    ],
                    'order' => 'id desc'
                ]),
                "limit" => $max,
                "page"  => $page,

            ]);
        }else {
            $list_product = new PaginatorModel([
                'data' => Product::find([
                    'conditions' => 'status = :status: and del_flag = :del_flag:',
                    'bind' => [
                        'status' => 2,
                        'del_flag' => Product::NOT_DELETED
                    ],
                    'order' => 'id desc'
                ]),
                "limit" => $max,
                "page"  => $page
            ]);
        }
        $product = [];
        foreach ($list_product->getPaginate()->items as $key=>$val){
            $product[] = $val->toArray();
        }
        if(!$value){
            $product_paging = Product::find([
                'conditions' => 'status = :status: and del_flag = :del_flag:',
                'bind' => [
                    'status' => 2,
                    'del_flag' => Product::NOT_DELETED
                ],
            ]);
        }else {
            $product_paging = Product::find([
                'conditions' => 'name like :value: and status = :status: and del_flag = :del_flag:',
                'bind' => [
                    'value' => '%'.$value.'%',
                    'status' => 2,
                    'del_flag' => Product::NOT_DELETED
                ],
            ]);
        }

        $pagination = [
            'total' => 0,
            'total_pages' => 1,
            'current' => 1,
            'current_link' => base_uri()."/product?"
        ];

        if($product_paging){
            $product_paging = $product_paging->toArray();
            $pagination = [
                'total' => count($product_paging),
                'total_pages' => ceil(count($product_paging)/$max),
                'current' => $page,
                'current_link' => base_uri()."/product?"
            ];
        }

        $this->view->list_product = $product;
        $this->view->Pagination = $pagination;
    }
    public function product_categoryAction($id=null){
        $max = 8;
        $page = $this->request->getQuery('p','int',1);
        $list_product = new PaginatorModel([
            'data' => Product::find([
                'conditions' => 'category_id = :category_id: and status = :status: and del_flag = :del_flag:',
                'bind' => [
                    'category_id' => $id,
                    'status' => 2,
                    'del_flag' => Product::NOT_DELETED
                ],
            ]),
            "limit" => $max,
            "page"  => $page,
        ]);
        $product = [];
        foreach ($list_product->getPaginate()->items as $key=>$val){
            $product[] = $val->toArray();
        }
        $product_paging = Product::find([
            'conditions' => 'category_id = :category_id: and status = :status: and del_flag = :del_flag:',
            'bind' => [
                'category_id' => $id,
                'status' => 2,
                'del_flag' => Product::NOT_DELETED
            ],
        ]);
        $pagination = [
            'total' => 0,
            'total_pages' => 1,
            'current' => 1,
            'current_link' => base_uri()."/product/product_category/$id?"
        ];

        if($product_paging){
            $product_paging = $product_paging->toArray();
            $pagination = [
                'total' => count($product_paging),
                'total_pages' => ceil(count($product_paging)/$max),
                'current' => $page,
                'current_link' => base_uri()."/product/product_category/$id?"
            ];
        }
        $this->view->list_product = $product;
        $this->view->Pagination = $pagination;


    }
    public function product_detailAction($id){
        $product = Product::findById($id);
        if(!$product){
            return json_encode([
                'status' => 0,
                'message' => 'Không tìm thấy sản phẩm!'
            ]);
        }
        $product = $product->toArray();
        if($product['category_id']){
            $category = Category::findById($product['category_id']);
            if($category){
                $product['category'] = [
                    'id' => $category->getId(),
                    'name' => $category->getName()
                ];
            }else {
                $product['category'] = [
                    'id' => null,
                    'name' => ''
                ];
            }
        }
        $product['slide_show'] = json_decode($product['slide_show'],true);
        $product['properties'] = json_decode($product['properties'],true);

        $this->view->product = $product;
    }

    public function searchAction($value){
        $value = $this->request->getPost('value');
        if($value){
            $product = Product::find([
                'conditions' => "(name LIKE :value:) and del_flag = :del_flag: and status = 2",
                'bind' => [
                    'value' => '%'.$value.'%',
                    'del_flag' => Product::NOT_DELETED
                ],
                'order' => 'id desc'
            ]);
            if($product){
                $this->view->list_product = $product->toArray();
                $this->view->pick('index');
                return json_encode([
                    'status' => 1
                ]);
            }else{
                return json_encode([
                    'status' => 0,
                    'message' => 'Không tìm thấy sản phẩm nào'
                ]);
            }
        }else return json_encode([
            'status' => 0,
            'message' => 'Không tìm thấy sản phẩm nào'
        ]);

    }
}

