<?php
namespace DoGo\Frontend\Controllers;

use DoGo\Frontend\Models\Articel;
use DoGo\Frontend\Models\Districts;
use DoGo\Frontend\Models\Product;
use DoGo\Frontend\Models\Province;
use DoGo\Frontend\Models\Wards;
use Phalcon\Cache\Backend;
use Phalcon\Mvc\Controller;
use DoGo\Frontend\Models\Category;
use DoGo\Frontend\Models\Contact;
use DoGo\Frontend\Models\Location;
use Viettelpost;

/**
 * Class ControllerBase
 * @property Backend $cache
 */
class ControllerBase extends Controller
{
    use \AuthExt;
    const STATUS_SUCCESS = 1;
    const STATUS_ERROR = 0;
    const STATUS_WARNING = 2;
    public $helper;
    public $vtp;

    public function initialize() {
        $this->view->resourcePath = base_uri();
        if($this->isLogin()) $this->view->auth = $this->getLoggedUserInfo();
        $this->category();
        $this->contact();
        $this->cart_header();
        $this->product_new();
        $this->getArticel();
    }

    public function setAuth($auth_token)
    {
        $this->session->set("auth", $auth_token);
    }

    /***
     * @param Header $header
     */
    public function setHeader(Header $header)
    {
        $this->view->header = $header;
    }


    function category(){
        $category = Category::find([
            'conditions' => 'del_flag = :del_flag:',
            'bind' => [
                'del_flag' => Category::NOT_DELETED
            ]
        ]);

        $category = $category->toArray();

        $this->view->category_sidebar = $category;
    }

    function contact(){
        $contact = Contact::find([
            'limit' => 2
        ]);
        $contact = $contact->toArray();
        $this->view->contact = $contact;
    }

    public function render_template($controller, $action, $data = null)
    {
        $view = $this->view;
        $content = $view->getRender($controller, $action, ["object" => $data], function ($view) {
            $view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_LAYOUT);
        });
        return $content;
    }

    function product_new(){
        $product = Product::findFirst([
            'conditions' => 'status = :status: and del_flag = :del_flag:',
            'bind' => [
                'status' => 2,
                'del_flag' => Product::NOT_DELETED
            ],
            'order' => 'id desc'
        ]);
        $this->view->product_new = $product->toArray();
    }

    function cart_header(){
        $products = $this->session->get('cart_product');
        usort($products,function ($a,$b){
            return $a['time'] < $b['time'] ? 1 : -1;
        });
        $total_product = count($products);
        $total_money = 0;
        if(count($products)){
            foreach ($products as $product){
                $total_money = $total_money + $product['price']*$product['quantity'];
            }
            if(count($products)>2){
                $products = array_chunk($products,2);
                $products = $products[0];
            }
            $product_ids =array_values(array_column($products,'id'));
            $list_product = Product::find([
                'conditions' => 'id in ({id:array})',
                'bind' => [
                    'id' => $product_ids
                ]
            ]);
            if($list_product){
                $list_product = $list_product->toArray();
                foreach ($list_product as $key => $value){
                    if($products[$value['id']]) {
                        $list_product[$key]['quantity'] = $products[$value['id']]['quantity'];
                        $list_product[$key]['price_disconut'] = $products[$value['id']]['price'];
                    }
                }
            } else $list_product = [];
            $this->view->cart_product = $list_product;
            $this->view->total_product = $total_product;
            $this->view->total_money = $total_money;
        }
    }

    public function getAddress($city_id=null,$district_id=null,$ward_id=null){
        $district = Districts::findFirst([
            'conditions' => 'district_id = :district_id:',
            'bind' => [
                'district_id' => $district_id
            ]
        ]);
        if($ward_id){
            $ward = Wards::findFirst([
                'conditions' => 'ward_id = :ward_id:',
                'bind' => [
                    'ward_id' => $ward_id
                ]
            ]);
            return $ward->getFullname();
        }
        return $district->getFullname();
    }

    public function getProvinceVtp($id){
        $city = Province::findFirst([
            'conditions' => 'city_id = :city_id:',
            'bind' => [
                'city_id' => $id
            ]
        ]);
        if($city) return intval($city->getVtpId());
        else return 0;
    }

    public function getDistrictVtp($id){
        $district = Districts::findFirst([
            'conditions' => 'district_id = :district_id:',
            'bind' => [
                'district_id' => $id
            ]
        ]);
        if($district) return intval($district->getVtpId());
        else return 0;
    }

    public function getWardVtp($id){
        $ward = Wards::findFirst([
            'conditions' => 'ward_id = :ward_id:',
            'bind' => [
                'ward_id' => $id
            ]
        ]);
        if($ward) return intval($ward->getVtpId());
        else return 0;
    }

    public function getArticel(){
        $articel = Articel::find([
            'conditions' => 'del_flag = :del_flag: and status =:status:',
            'bind' => [
                'del_flag' => Articel::NOT_DELETED,
                'status' => 2
            ],
            'order' => 'id desc'
        ]);
        $articel = $articel->toArray();
        foreach ($articel as $key=>$val){
            $articel[$key]['datecreate'] = date("d/m/Y H:i",$val['datecreate']);
        }
        $articel_footer = array_chunk($articel,2)[0];
        $articel_new = $articel[0];
        unset($articel[0]);
        $this->view->articel_new = $articel_new;
        $this->view->list_articel = $articel;
        $this->view->articel_footer = $articel_footer;
    }
}
