<?php
namespace DoGo\Frontend\Controllers;

use AuthenticationService;
use DoGo\Frontend\Models\User;
use PasswordService;

/**
 * Class AuthController
 * @property \Phalcon\Config $config
 */
class AuthController extends ControllerBase
{
    use \AuthExt;

    public function initialize() {
        parent::initialize();
    }

    public function indexAction()
    {

    }

    /**
     * Đăng nhập
     */
    function loginAction() {
        if ( $this->isLogin() ) {
            return $this->response->redirect( base_uri() . '/index' );
        }
        if ( $this->request->isPost() ) {
            $user = $this->request->getPost( 'username' );
            $password = $this->request->getPost( 'password' );
            $authService = new AuthenticationService();
            if ( $user = $authService->login( $user, $password ) ) {
                $this->saveUserInfoToSession( $user );
                $this->response->redirect( base_uri() . '/index' );
            }
        }
    }

    /**
     * Đăng xuất
     */
    function logoutAction() {
        $this->session->destroy( true );
        $this->response->redirect( base_uri() );
    }

    function registerAction(){
        if($this->getLoggedUserInfo()){
            $this->response->redirect(base_uri());
            return;
        }

        if($this->request->getPost('register')){
            $user = $this->request->getPost('register');

            if(User::searchByEmailOrPhoneOrUsername($user['email'])){
                $this->flash->error("Email đã được sử dụng");
                $this->response->redirect(base_uri()."/auth/register");
                return;
            }
            if(User::searchByEmailOrPhoneOrUsername($user['phone'])){
                $this->flash->error("Số điện thoại đã được sử dụng");
                $this->response->redirect(base_uri()."/auth/register");
                return;
            }

            $password = $user['password'];
            $dob = $this->request->getPost('register_birth');
            $dob = $dob['day']."-".$dob['month']."-".$dob['year'];
            $user = User::newInstance($user);
            $user->setDatecreate(time());
            $user->setDob(strtotime($dob) +86400);
            if($user->save()){
                $authService = new AuthenticationService();
                if($user = $authService->login($user->getEmail(),$password)){
                    $this->saveUserInfoToSession( $user );
                    $this->response->redirect( base_uri() . '/index' );
                }
            }else{
                $this->flash->error("Đăng ký tài khoản không thành công.");
                $this->response->redirect(base_uri()."/auth/register");
            }
        }
    }

    function update_passwordAction(){
        if($this->request->getPost('password')){
            $password = $this->request->getPost('password');
            $user = $this->getLoggedUserInfo();
            $user = User::searchByEmailOrPhoneOrUsername($user['email']);
            if(PasswordService::checkPassword($user->getEmail(),$password['current_password'])){
                $user->setPassword($password['password']);
                if($user->save()){
                    $this->flash->success('Cập nhật mật khẩu thành công');
                }else $this->flash->error('Cập nhật mật khẩu không thành công');
            }else $this->flash->error('Cập nhật mật khẩu không thành công');
        }
        $this->response->redirect(base_uri().'/customer/update_password');
    }

    function update_customerAction(){
        if($this->request->getPost('user')){
            $user_data = $this->request->getPost('user');
            $dob = $this->request->getPost('birth');
            $dob = strtotime($dob[0]."-".$dob[1]."-".$dob[2]);
            $user = $this->getLoggedUserInfo();
            $user = User::findById($user['id']);

            if($user_data['email'] != $user->getEmail() && User::searchByEmailOrPhoneOrUsername($user_data['email'])){
                $this->flash->error("Email đã được sử dụng");
                $this->response->redirect(base_uri()."/auth/register");
            }
            if($user_data['phone'] != $user->getPhone() && User::searchByEmailOrPhoneOrUsername($user_data['phone'])){
                $this->flash->error("Số điện thoại đã được sử dụng");
                $this->response->redirect(base_uri()."/auth/register");
            }

            if($user_data['username'] != $user->getUsername() && User::searchByEmailOrPhoneOrUsername($user_data['username'])){
                $this->flash->error("Username đã được sử dụng.");
                $this->response->redirect(base_uri()."/auth/register");
            }


            $user->setFullname($user_data['fullname']);
            $user->setEmail($user->getEmail());
            $user->setGender($user_data['gender']);
            $user->setDob($dob);
            if($user->save()){
                $this->flash->success('Cập nhật thành công');
            }else $this->flash->error('Cập nhật không thành công');
        }
        $this->response->redirect(base_uri().'/customer');
    }
}

