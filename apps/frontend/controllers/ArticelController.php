<?php
namespace DoGo\Frontend\Controllers;
use DoGo\Frontend\Models\Articel;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

/**
 * Class IndexController
 * @property \Phalcon\Config $config
 */
class ArticelController extends ControllerBase
{
    public function indexAction()
    {
        $max = 4;
        $page = $this->request->getQuery('p','int',1);
        $list_articel = new PaginatorModel([
            'data' => Articel::find([
                'conditions' => 'status = :status: and del_flag = :del_flag:',
                'bind' => [
                    'status' => 2,
                    'del_flag' => Articel::NOT_DELETED
                ],
            ]),
            "limit" => $max,
            "page"  => $page,
        ]);
        $articel = [];
        foreach ($list_articel->getPaginate()->items as $key=>$val){
            $data = $val->toArray();
            $data['datecreate'] = date('d/m/Y H:i',$data['datecreate']);
            $articel[] = $data;
        }
        $articel_paging = Articel::find([
            'conditions' => 'status = :status: and del_flag = :del_flag:',
            'bind' => [
                'status' => 2,
                'del_flag' => Articel::NOT_DELETED
            ],
        ]);
        $pagination = [
            'total' => 0,
            'total_pages' => 1,
            'current' => 1,
            'current_link' => base_uri()."/articel?"
        ];

        if($articel_paging){
            $articel_paging = $articel_paging->toArray();
            $pagination = [
                'total' => count($articel_paging),
                'total_pages' => ceil(count($articel_paging)/$max),
                'current' => $page,
                'current_link' => base_uri()."/articel?"
            ];
        }
        $this->view->list_articel = $articel;
        $this->view->Pagination = $pagination;
    }

    public function detailAction($id=null){
        if($id){
            $articel = Articel::findById($id);
            if($articel) $articel = $articel->toArray();
            else $articel = [];
            $articel['datecreate'] = date('d/m/Y H:i',$articel['datecreate']);
            $this->view->articel = $articel;
        }
    }
}

