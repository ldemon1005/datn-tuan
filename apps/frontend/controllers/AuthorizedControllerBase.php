<?php
namespace DoGo\Frontend\Controllers;
/**
 * Class AuthController
 * @property \Phalcon\Config $config
 */
class AuthorizedControllerBase extends ControllerBase
{
    use \AuthExt;

    public function initialize()
    {
        parent::initialize();


        if (!$this->isLogin()) {
            $this->response->redirect(base_uri() . '/backend/auth/login');
        }

	    $user_info = $this->getLoggedUserInfo();
        $this->view->auth = $user_info;

        /*if ($user_info['role'] == 0) {
            $this->response->redirect(base_uri() . '/index/maintenance');
        }*/
    }
}

