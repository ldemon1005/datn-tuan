<?php

use Phalcon\Session\Adapter;

/**
 * Created by PhpStorm.
 * User: balol
 * Date: 1/24/2018
 * Time: 11:36 AM
 */
trait AuthExt
{
    function isLogin()
    {
        /** @var Adapter $session */
        $session = provider('session');
        return $session->has('auth');
    }


    function saveUserInfoToSession($userInfo)
    {
        /** @var Adapter $session */
        $session = provider('session');
        $session->set('auth', ArrayUtils::selectKeys($userInfo, ['id', 'fullname', 'email', 'phone', 'gender', 'type', 'config']));
    }


    function getLoggedUserInfo()
    {
        /** @var Adapter $session */
        $session = provider('session');
        return $session->get('auth');
    }
}