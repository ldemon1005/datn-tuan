<?php

use GuzzleHttp\Client;
use Phalcon\Di\Injectable;
use Phalcon\Forms\Element\Password;

use DoGo\Frontend\Models\User;

/**
 * Created by PhpStorm.
 * User: balol
 * Date: 1/22/2018
 * Time: 5:18 PM
 */
class AuthenticationService extends Injectable
{

    function login($user, $password)
    {
        $this->eventsManager->fire('login:before', $this, [$user, $password]);

        $loginResult = $this->checkUserPassword($user, $password); // TODO get login result
        if ($loginResult) {
            $this->eventsManager->fire('login:success', $this, [$user, $password]);
            return $this->generateAuthentication($loginResult);
        }

        $this->eventsManager->fire('login:fail', $this, [$user, $password]);
        return false;
    }


    function generateAuthentication($loginResult)
    {
        return $loginResult;
    }


    function checkUserPassword($user, $password)
    {
        $user = User::searchByEmailOrPhoneOrUsername($user);
        if ($user && PasswordService::checkPassword($password, $user->password)) {
            $user->last_login = time();
            $user->update();
            return $user->toArray();
        }

        $this->flash->error("Thông tin đăng nhập không đúng!!!");
        return false;

    }
}