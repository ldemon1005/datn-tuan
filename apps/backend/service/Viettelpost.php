<?php
use Phalcon\Session\Adapter;
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 3/8/2018
 * Time: 10:28 AM
 */
class Viettelpost
{
    const BASE_API_URI = 'https://api.viettelpost.vn/';

    const CREATE_ACCOUNT_PATH = '/api/user/Register';

    const CREATE_ORDER_PATH = '/api/tmdt/InsertOrder';
    const UPDATE_ORDER_PATH = '/api/tmdt/UpdateOrder';
    const CALCULATE_FEE_PATH = '/api/tmdt/getPrice';
    const TRACKING_ORDER_PATH = '/api/setting/listOrderTracking';

    const AUTH_API_URI = 'https://api.viettelpost.vn/api/user/Login';


    const CHECK_ORDER_STATUS = 1;
    const CHECK_FALLBACK_ORDER_STATUS = 2;
    const FORWARD_ORDER_STATUS = 3;
    const CANCEL_ORDER_STATUS = 4;
    const GETBACK_ORDER_STATUS = 5;

    /**
     * @var \Phalcon\Http\Client\Provider\Curl
     */
    protected $_curl;

    /**
     * Init
     */
    function initialize()
    {
        $this->_curl = new \Phalcon\Http\Client\Provider\Curl();
        $this->_curl->setOptions([
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_TIMEOUT => 3
        ]);
        $this->_curl->header->set('Content-type', 'application/json');
        $this->_curl->setBaseUri(static::BASE_API_URI);
    }

    public function requestNewApiKey()
    {
        /** @var Adapter $session */
        $session = provider('session');

        $curl = new \Phalcon\Http\Client\Provider\Curl();
        $curl->setOptions([
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_TIMEOUT => 3
        ]);

        $curl->header->set('Content-type', 'application/json');
        $response = $curl->post(static::AUTH_API_URI, json_encode([
            "USERNAME" => 'daovantuan1005@gmail.com',
            "PASSWORD" => 123456,
            "SOURCE" => 0
        ]));
        if ($response->body) {
            $body = json_decode($response->body, true);
            $session->set('VtpToken',$body['TokenKey']);
            return json_encode([
                'status' => 1,
                'message' => $body
            ]);
        }

        return json_encode([
            'status' => 0,
            'message' => 'Không có phản hồi từ viettelpost'
        ]);
    }

    function calculateFee($data)
    {
        $access_token = $this->getApiKey();

        $curl = new \Phalcon\Http\Client\Provider\Curl();
        $curl->setOptions([
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_TIMEOUT => 3
        ]);

        $curl->header->set('Content-type', 'application/json');

        $curl->header->set('token',$access_token);
        $response = $curl->post(
            static::BASE_API_URI . static::CALCULATE_FEE_PATH,
            json_encode($data));
        $res = $response->body;
        return json_decode($res,true);
    }

    function createOrder($data){
        $access_token = $this->getApiKey();
        $curl = new \Phalcon\Http\Client\Provider\Curl();
        $curl->setOptions([
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_TIMEOUT => 3
        ]);
        $curl->header->set('Content-type', 'application/json');
        $curl->header->set('token',$access_token);
        $response = $curl->post(
            static::BASE_API_URI .static::CREATE_ORDER_PATH,
            json_encode($data));
        return json_decode($response->body,true);
    }

    function cancelOrder($tracking_code){
        try {
            $data = [
                "TYPE" => static::CANCEL_ORDER_STATUS,
                "ORDER_NUMBER" => $tracking_code,
                "NOTE" => "Hủy đơn",
                "DATE" => date('d/m/Y H:i:s ')
            ];

            $access_token = $this->getApiKey();

            $curl = new \Phalcon\Http\Client\Provider\Curl();
            $curl->setOptions([
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_TIMEOUT => 3
            ]);

            $curl->header->set('Content-type', 'application/json');

            $curl->header->set('token',$access_token);

            $response = $curl->post(
                static::BASE_API_URI . static::CALCULATE_FEE_PATH,
                json_encode($data));
            $res = $response->body;

        } catch (Exception $e) {
            throw $e;
        }

        return $res;

    }

    public function createAccount($userData)
    {
        $data = [
            "FIRSTNAME" => $userData['firstname'],
            "LASTNAME" => $userData['lastname'],
            "EMAIL" => $userData['email'],
            "PASSWORD" => $userData['password'],
            "PHONE" => $userData['phone'],
            "DISPLAYNAME" => $userData['displayname'],
            "SEX" => intval($userData['sex']) ? 1 : 0, //0:nam,1:nữ
            "INTRODUCTION" => $userData['introduction'] ?: "",
            "DISTRICT_ID" => $userData['district_id'],
            "WARDS_ID" => $userData['ward_id'],
            "ADDRESS" => $userData['address'] ?: "",
            "SOURCE" => 0
        ];

        $res = $this->_curl->post(static::CREATE_ACCOUNT_PATH, json_encode($data));

        $res = json_decode($res->body);
        $res = array_from($res);
        if ($res['error'] == true) {
            return [
                'status' => 0,
                'message' => 'đăng ký tài khoản không thành công'
            ];
        }
        return $res;
    }

    function getApiKey(){
        /** @var Adapter $session */
        $session = provider('session');
        $token = $session->get('VtpToken');
        return $token;
    }
}