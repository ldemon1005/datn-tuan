<?php
/**
 * Created by PhpStorm.
 * User: ldemon1005
 * Date: 3/28/2018
 * Time: 9:59 PM
 */
use Zalo\Zalo;
use Zalo\ZaloConfig;
use Zalo\ZaloEndpoint;
use Zalo\FileUpload\ZaloFile;

class ZaloService
{
    public $zalo;
    public $helper;
    function initialize(){
        $this->zalo = new Zalo(ZaloConfig::getInstance()->getConfig());
        $this->helper = $this->zalo -> getRedirectLoginHelper();
    }

    public function create_category($category){
        $zalo = new Zalo(ZaloConfig::getInstance()->getConfig());
        $zalo->getRedirectLoginHelper();
        $file_name = explode("/",$category['avatar']);
        $filePath = $file_name[4];
        $params = ['file' => new ZaloFile($filePath)];
        $response = $zalo->post(ZaloEndpoint::API_OA_STORE_UPLOAD_CATEGORY_PHOTO, $params);
        $result_img = $response->getDecodedBody(); // result

        if($result_img['errorCode'] == 1){
            $data = array(
                'name' => $category['name'],
                'desc' => $category['desc'],
                'photo' => $result_img['data']['imageId'],
                'status' => 0 // 0 - show | 1 - hide
            );
            $params = ['data' => $data];
            $response = $zalo->post(ZaloEndpoint::API_OA_STORE_CREATE_CATEGORY, $params);
            $result = $response->getDecodedBody(); // result
            return $result;
        }
        return [
            'errorCode' => 0
        ];
    }

    public function update_category($category){
        $zalo = new Zalo(ZaloConfig::getInstance()->getConfig());
        $helper = $zalo->getRedirectLoginHelper();
        $file_name = explode("/",$category['avatar']);
        $filePath = $file_name[4];
        $params = ['file' => new ZaloFile($filePath)];
        $response = $zalo->post(ZaloEndpoint::API_OA_STORE_UPLOAD_CATEGORY_PHOTO, $params);
        $result_img = $response->getDecodedBody(); // result

        if($result_img['errorCode'] == 1){
            $categoryUpdate = array(
                'name' => $category['name'],
                'desc' => $category['desc'],
                'photo' => $result_img['data']['imageId'],
                'status' => $category['status'] == 2 ? 0:1 // 0 - show | 1 - hide
            );
            $data = array(
                'categoryid' => $category['id_zalo'],
                'category' => $categoryUpdate
            );
            $params = ['data' => $data];
            $response = $zalo->post(ZaloEndpoint::API_OA_STORE_UPDATE_CATEGORY, $params);
            $result = $response->getDecodedBody(); // result
            return $result;
        }
        return [
            'errorCode' => 0
        ];
    }

    public function create_product($product){
        $zalo = new Zalo(ZaloConfig::getInstance()->getConfig());
        $zalo->getRedirectLoginHelper();

        $file_name = explode("/",$product['avatar']);
        $filePath = $file_name[4];
        $params = ['file' => new ZaloFile($filePath)];
        $response = $zalo->post(ZaloEndpoint::API_OA_STORE_UPLOAD_PRODUCT_PHOTO, $params);
        $result_img = $response->getDecodedBody(); // result
        if($result_img['errorCode'] == 1){
            $cate = array('cateid' => $product['cate_zalo']);
            $cates = [$cate];
            $photo = array('id' => $result_img['data']['imageId']);
            $photos = [$photo];
            $data = array(
                'cateids' => $cates,
                'name' => $product['name'],
                'desc' => $product['desc'],
                'code' => $product['code'],
                'price' => $product['price'],
                'photos' => $photos,
                'display' => 'show', // show | hide
                'payment' => 2 // 2 - enable | 3 - disable
            );
            $params = ['data' => $data];
            $response = $zalo->post(ZaloEndpoint::API_OA_STORE_CREATE_PRODUCT, $params);
            $result = $response->getDecodedBody(); // result
            return $result;
        }
        return [
            'errorCode' => 0
        ];
    }

    public function update_product($product){
        $zalo = new Zalo(ZaloConfig::getInstance()->getConfig());
        $helper = $zalo->getRedirectLoginHelper();

        $file_name = explode("/",$product['avatar']);
        $filePath = $file_name[4];
        $params = ['file' => new ZaloFile($filePath)];
        $response = $zalo->post(ZaloEndpoint::API_OA_STORE_UPLOAD_PRODUCT_PHOTO, $params);
        $result_img = $response->getDecodedBody(); // result

        if($result_img['errorCode'] == 1){
            $cate = array('cateid' => $product['cate_zalo']);
            $cates = [$cate];
            $photo = array('id' => $result_img['data']['imageId']);
            $photos = [$photo];
            $productUpdate = array(
                'cateids' => $cates,
                'name' => $product['name'],
                'desc' => $product['desc'],
                'code' => $product['code'],
                'price' => $product['price'],
                'photos' => $photos,
                'display' => 'show', // show | hide
                'payment' => 2 // 2 - enable | 3 - disable
            );
            $data = array(
                'productid' => $product['id_zalo'],
                'product' => $productUpdate
            );
            $params = ['data' => $data];
            $response = $zalo->post(ZaloEndpoint::API_OA_STORE_UPDATE_PRODUCT, $params);
            $result = $response->getDecodedBody(); // result
            return $result;
        }
        return [
            'errorCode' => 0
        ];
    }

    public function delete_product($product){
        $zalo = new Zalo(ZaloConfig::getInstance()->getConfig());
        $helper = $zalo->getRedirectLoginHelper();
        $params = ['productid' => $product['id_zalo']];
        $response = $zalo->post(ZaloEndpoint::API_OA_STORE_REMOVE_PRODUCT, $params);
        $result = $response->getDecodedBody(); // result
        return $result;
    }

    public function get_order($offset = 0){
        $zalo = new Zalo(ZaloConfig::getInstance()->getConfig());
        $zalo->getRedirectLoginHelper();

        $data = array(
            'offset' => $offset,
            'count' => 10,
            'filter' => 0
        );
        $params = ['data' => $data];
        $response = $zalo->get(ZaloEndpoint::API_OA_STORE_GET_SLICE_ORDER, $params);
        $result = $response->getDecodedBody(); // result
        return $result;
    }

    public function update_order($id,$status){
        $zalo = new Zalo(ZaloConfig::getInstance()->getConfig());
        $zalo->getRedirectLoginHelper();
        $data = array(
            'orderid' =>$id,
            'status' => $status,
            'reason' => 'Cập nhật từ bán đồ dân dụng',
            'cancelReason' => 'Cập nhật từ bán đồ dân dụng'
        );
        $params = ['data' => $data];
        $response = $zalo->post(ZaloEndpoint::API_OA_STORE_UPDATE_ORDER, $params);
        $result = $response->getDecodedBody(); // result
        return $result;
    }

    public function getOrderById($id = null){
        $zalo = new Zalo(ZaloConfig::getInstance()->getConfig());
        $zalo->getRedirectLoginHelper();
        $params = ['orderid' => $id];
        $response = $zalo->get(ZaloEndpoint::API_OA_STORE_GET_ORDER, $params);
        $result = $response->getDecodedBody(); // result
        return $result;
    }
}