<?php

use Phalcon\Security;

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/15/2017
 * Time: 9:59 AM
 */
class PasswordService
{

    /**
     * @param $pass
     *
     * @return mixed
     */
    public static function encryptPassword($pass)
    {
        $security = provider('security');
        return $security->hash($pass);
    }


    /**
     * @param $pass
     * @param $hashed
     *
     * @return mixed
     */
    public static function checkPassword($pass, $hashed)
    {
        if(md5($pass) == $hashed) return true;
        else return false;
    }
}