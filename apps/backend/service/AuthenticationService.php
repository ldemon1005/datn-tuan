<?php

use GuzzleHttp\Client;
use Phalcon\Di\Injectable;
use Phalcon\Forms\Element\Password;

use DoGo\Backend\Models\User;

/**
 * Created by PhpStorm.
 * User: balol
 * Date: 1/22/2018
 * Time: 5:18 PM
 */
class AuthenticationService extends Injectable
{

    function login($user, $password)
    {
        $loginResult = $this->checkUserPassword($user, $password); // TODO get login result
        if ($loginResult) {
            $this->eventsManager->fire('login:success', $this, [$user, $password]);
            return $loginResult;
        }
        return false;
    }


    function checkUserPassword($user, $password)
    {
        $user = User::searchByEmailOrPhoneOrUsername($user);

        if ($user && PasswordService::checkPassword($password, $user->password)) {
            if($user->getType() != 1){
                $this->flash->error("Bạn không có quyền truy cập");
                return false;
            }
            $user->last_login = time();
            $user->update();
            return $user->toArray();
        }

        $this->flash->error("Thông tin đăng nhập không đúng!!!");
        return false;
    }
}