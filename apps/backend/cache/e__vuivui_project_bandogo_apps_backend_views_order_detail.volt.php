<header class="main-header">
    <!-- Logo -->
    <a href="/backend" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>Z</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Đồ gỗ</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/../backend_res/assets/img/tree.png" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?= $auth['fullname'] ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="/../backend_res/assets/img/tree.png" class="img-circle" alt="User Image">

                            <p>
                                <?= $auth['fullname'] ?>
                                <?= $auth['email'] ?>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Hồ Sơ</a>
                            </div>
                            <div class="pull-right">
                                <a href="/backend/auth/logout" class="btn btn-default btn-flat">Đăng Xuất</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->

    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/../backend_res/assets/img/tree.png" class="img-circle" alt="logo đại lý">
            </div>
            <div class="pull-left info">
                <p><?= $auth['fullname'] ?></p>
                <a href="#"><i class="fa fa-user-circle"></i> <b>Trang quản lý</b></a>
            </div>
        </div>


        <ul class="sidebar-menu" data-widget="tree">
            <li >
                <a href="/backend/user" class="treeview">
                    <i class="fa fa-users"></i>
                    <span>Tài khoản</span>
                </a>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-product-hunt"></i>
                    <span>Sản phẩm</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/category"><i class="fa fa-circle-o"></i> Danh mục sản phẩm</a></li>
                    <li><a href="/backend/product"><i class="fa fa-circle-o"></i> Sản phẩm</a></li>
                </ul>
            </li>
            <li>
                <a href="/backend/order" class="treeview">
                    <i class="fa fa-cart-arrow-down"></i>
                    <span>Đơn hàng</span>
                </a>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-product-hunt"></i>
                    <span>Thông tin</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/contact"><i class="fa fa-circle-o"></i> Danh sách cửa hàng</a></li>
                    <li><a href="/backend/contactCustomer"><i class="fa fa-circle-o"></i> Danh sách liên hệ</a></li>
                </ul>
            </li>

            <li>
                <a href="/backend/articel" class="treeview">
                    <i class="fa fa-book"></i>
                    <span>Bài viết</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><a href="/backend/order" class="btn btn-primary"><i class="fa fa-long-arrow-left"></i></a>Chi tiết đơn hàng <?= ($order['order_code'] ? $order['order_code'] : $order['orderCode']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div id="sec__customer" class="sec__page">
                    <?php if ($order['order_code'] != null) { ?>
                        <table class="table">
                            <tr>
                                <th class="xdot">Tên sản phẩm</th>
                                <th class="xdot">Giá bán</th>
                                <th>Số lượng</th>
                                <th class="xdot">Thành tiền</th>
                            </tr>
                            <?php foreach ($list_product as $item) { ?>
                                <tr>
                                    <td>
                                        <div class="xdot code"><?= $item['name'] ?></div>
                                    </td>
                                    <td><?= number_format($item['price']) ?></td>
                                    <td><?= $item['quantity'] ?></td>
                                    <td>
                                        <?php if ($item['discount_type'] == 1) { ?>
                                            <div class="product__price xdot"><?= number_format($item['price'] * $item['quantity'] * (100 - $item['discount']) / 100) ?> <span class="unit">đ</span></div>
                                        <?php } else { ?>
                                            <div class="product__price xdot"><?= number_format($item['price'] * $item['quantity'] - $item['discount']) ?> <span class="unit">đ</span></div>
                                        <?php } ?>

                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><b>Phí vận chuyển</b></td>
                                <td></td>
                                <td></td>
                                <td ><b><?= number_format($order['vtp_info']['vtp_fee']) ?></b></td>
                            </tr>
                            <tr>
                                <td><b>Tổng</b></td>
                                <td></td>
                                <td><?= $total_quantity ?></td>
                                <td ><b><?= number_format($order['money']) ?></b></td>
                            </tr>
                        </table>
                    <?php } else { ?>
                        <table class="table">
                            <tr>
                                <th class="xdot">Tên sản phẩm</th>
                                <th class="xdot">Giá bán</th>
                                <th>Số lượng</th>
                                <th class="xdot">Thành tiền</th>
                            </tr>

                            <tr>
                                <td>
                                    <div class="xdot code"><?= $order['productName'] ?></div>
                                </td>
                                <td><?= number_format($order['price']) ?></td>
                                <td>1</td>
                                <td><?= number_format($order['price']) ?></td>
                            </tr>

                            <tr>
                                <td><b>Phí vận chuyển</b></td>
                                <td></td>
                                <td></td>
                                <td ><b><?= number_format($order['shippingInfo']['shippingFee']) ?></b></td>
                            </tr>
                            <tr>
                                <td><b>Tổng</b></td>
                                <td></td>
                                <td><?= $total_quantity ?></td>
                                <td ><b><?= number_format($order['price'] + $order['shippingInfo']['shippingFee']) ?></b></td>
                            </tr>
                        </table>
                    <?php } ?>

                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

<footer class="main-footer">

</footer>
