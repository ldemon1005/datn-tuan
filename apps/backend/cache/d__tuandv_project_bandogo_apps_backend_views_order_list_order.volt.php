<header class="main-header">
    <!-- Logo -->
    <a href="/" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>Z</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Đồ gỗ</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/../backend_res/assets/img/tree.png" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?= $auth['fullname'] ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="/../backend_res/assets/img/tree.png" class="img-circle" alt="User Image">

                            <p>
                                <?= $auth['fullname'] ?>
                                <?= $auth['email'] ?>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Hồ Sơ</a>
                            </div>
                            <div class="pull-right">
                                <a href="/backend/auth/logout" class="btn btn-default btn-flat">Đăng Xuất</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->

    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/../backend_res/assets/img/tree.png" class="img-circle" alt="logo đại lý">
            </div>
            <div class="pull-left info">
                <p><?= $auth['fullname'] ?></p>
                <a href="#"><i class="fa fa-user-circle"></i> <b>Trang quản lý</b></a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Tìm kiếm...">
                <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i
                                    class="fa fa-search"></i>
                        </button>
                    </span>
            </div>
        </form>


        <ul class="sidebar-menu" data-widget="tree">
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Tài khoản</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/user/register"><i class="fa fa-circle-o"></i> Thêm tài khoản</a></li>
                    <li><a href="/backend/user"><i class="fa fa-circle-o"></i> Danh sách tài khoản</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-product-hunt"></i>
                    <span>Sản phẩm</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/category"><i class="fa fa-circle-o"></i> Danh mục sản phẩm</a></li>
                    <li><a href="/backend/product"><i class="fa fa-circle-o"></i> Sản phẩm</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-book"></i>
                    <span>Bài viết</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/articel/create_articel"><i class="fa fa-circle-o"></i> Tạo mới bài viết</a></li>
                    <li><a href="/backend/articel"><i class="fa fa-circle-o"></i> Danh mục bài viết</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Danh mục bài viết</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cart-arrow-down"></i>
                    <span>Đơn hàng</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/order"><i class="fa fa-circle-o"></i> Danh sách đơn hàng</a></li>
                </ul>
            </li>
            <li>
                <a href="/backend/contact" class="treeview">
                    <i class="fa fa-id-badge"></i>
                    <span>Liên hệ</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Danh sách đơn hàng</h3>
                </div>
                <!-- /.box-header -->
                <div id="sec__customer" class="sec__page">
                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Ngày tạo</th>
                            <th>Mã đơn hàng</th>
                            <th>Tổng tiền</th>
                            <th>Phí v/c</th>
                            <th>Nguồn</th>
                            <th>Trạng thái</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($list_order as $item) { ?>
                            <tr>
                                <td class="text-center"><?= $item['datecreate'] ?></td>
                                <td class="text-center"><?= $item['order_code'] ?></td>
                                <td class="text-center"><?= $item['money'] ?></td>
                                <td class="text-center"><?= $item->['vtp_info']['vtp_fee'] ?></td>
                                <td class="text-center"></td>
                                <td class="text-center"><?= $item['status_name'] ?></td>

                                <td class="text-center">
                                    <div class="row form-group">
                                        <a data-toggle="tooltip" title="Chỉnh sửa" href="#" class="col-sm-2 text-primary"><i class="fa fa-wrench"></i></a>
                                        <a data-toggle="tooltip" title="Xóa" href="#" class="col-sm-2 text-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>

<footer class="main-footer">

</footer>
