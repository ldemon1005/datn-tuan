<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title text-primary">Thêm mới danh mục</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" action="" method="post">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-right">Tên danh mục</label>

                        <div class="col-sm-9">
                            <input type="text" value="<?= $category->name ?>" name="category[name]" class="form-control" placeholder="Tên danh mục">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-right">Danh mục cha</label>

                        <div class="col-sm-9">
                            <select class="form-control" name="category[parent_id]">
                                <?php foreach ($MenuRecusive as $item) { ?>
                                    <option value="<?= $item['id'] ?>"><?= $item['name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Avatar</label>
                        <div class="col-sm-9 form-group">
                            <div class="row">
                                <div class="col-sm-2">
                                    <img style="margin-left: 15px;height: 120px;width: 120px" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" id="avatar_img" class="profile-img-card avatar">
                                </div>
                                <div class="col-sm-4">
                                    <input type="file" id="file" name="user[avatar]">
                                    <em class="color-gray-light">(Hỗ trợ định dạng *.gif, *.png, *.jpg, *.bmp ) </em>
                                    <hr/>
                                    <button type="button" class="btn btn-primary" id="uploadAvatar">Tải ảnh</button>
                                </div>
                                <input hidden id="avatar_url" name="category[avatar]">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label text-right">Mô tả</label>

                        <div class="col-sm-9" name="category[desc]">
                            <textarea class="form-control" name="category[desc]" rows="3" placeholder="Mô tả ..."><?= $category->desc ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-right">Trạng thái</label>

                        <div class="col-sm-9">
                            <div class="row">
                                <label class="col-sm-6 text-primary">
                                    <input value="2" type="radio" name="category[status]" checked >
                                    Hoạt động
                                </label>
                                <label class="col-sm-6 text-primary">
                                    <input value="1" type="radio" name="category[status]">
                                    Không hoạt động
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">Thêm mới</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </div>
    <!-- /.modal-content -->
</div>