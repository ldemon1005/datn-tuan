<header class="main-header">
    <!-- Logo -->
    <a href="/" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>Z</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Đồ gỗ</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/../backend_res/assets/img/tree.png" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?= $auth['fullname'] ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="/../backend_res/assets/img/tree.png" class="img-circle" alt="User Image">

                            <p>
                                <?= $auth['fullname'] ?>
                                <?= $auth['email'] ?>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Hồ Sơ</a>
                            </div>
                            <div class="pull-right">
                                <a href="/backend/auth/logout" class="btn btn-default btn-flat">Đăng Xuất</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->

    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/../backend_res/assets/img/tree.png" class="img-circle" alt="logo đại lý">
            </div>
            <div class="pull-left info">
                <p><?= $auth['fullname'] ?></p>
                <a href="#"><i class="fa fa-user-circle"></i> <b>Trang quản lý</b></a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Tìm kiếm...">
                <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i
                                    class="fa fa-search"></i>
                        </button>
                    </span>
            </div>
        </form>


        <ul class="sidebar-menu" data-widget="tree">
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Tài khoản</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/user/register"><i class="fa fa-circle-o"></i> Thêm tài khoản</a></li>
                    <li><a href="/backend/user"><i class="fa fa-circle-o"></i> Danh sách tài khoản</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-product-hunt"></i>
                    <span>Sản phẩm</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/category"><i class="fa fa-circle-o"></i> Danh mục sản phẩm</a></li>
                    <li><a href="/backend/product"><i class="fa fa-circle-o"></i> Sản phẩm</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-book"></i>
                    <span>Bài viết</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/articel/create_articel"><i class="fa fa-circle-o"></i> Tạo mới bài viết</a></li>
                    <li><a href="/backend/articel"><i class="fa fa-circle-o"></i> Danh mục bài viết</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Danh mục bài viết</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cart-arrow-down"></i>
                    <span>Đơn hàng</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/order"><i class="fa fa-circle-o"></i> Danh sách đơn hàng</a></li>
                </ul>
            </li>
            <li>
                <a href="/backend/contact" class="treeview">
                    <i class="fa fa-id-badge"></i>
                    <span>Liên hệ</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Thêm mới sản phẩm </h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" id="form-register" action="/backend/product/create" method="post" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 text-primary">
                                </label>
                                <div class="col-sm-5">
                                    <?= $this->flash->output() ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tên sản phẩm</label>

                            <div class="col-sm-10">
                                <input type="text" name="product[name]" class="form-control" id="inputEmail3" placeholder="tên sản phẩm">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Danh mục</label>

                            <div class="col-sm-10">
                                <select class="form-control" name="product[category_id]">
                                    <?php foreach ($MenuRecusive as $item) { ?>
                                        <option value="<?= $item['id'] ?>"><?= $item['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Giá</label>

                            <div class="col-sm-10">
                                <input type="text" name="product[price]" class="form-control" placeholder="giá">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Loại giảm giá</label>

                            <div class="col-sm-10">
                                <div class="col-sm-10">
                                    <div class="row">
                                        <label class="col-sm-4 text-primary">
                                            <input value="2" type="radio" name="product[type_discount]" checked>
                                            Giảm theo phần trăm
                                        </label>
                                        <label class="col-sm-4 text-primary">
                                            <input value="1" type="radio" name="product[type_discount]">
                                            Giảm theo giá trị
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Giá trị giảm giá</label>

                            <div class="col-sm-10">
                                <input type="text" name="product[discount]" class="form-control" placeholder="giá trị giảm giá">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Số lượng</label>
                            <div class="col-sm-10">
                                <input type="text" name="product[price]" class="form-control" placeholder="số lượng">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Thuộc tính</label>

                            <div class="col-sm-10">
                                <div class="row form-group">
                                    <div class="col-sm-6">
                                        <label class="col-sm-4 control-label">Chiều dài</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="product[properties][length]" class="form-control" placeholder="chiều dài">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-sm-4 control-label">Chiều rộng</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="product[properties][width]" class="form-control" placeholder="chiều rộng">
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-6">
                                        <label class="col-sm-4 control-label">Chiều cao</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="product[properties][heigth]" class="form-control" placeholder="chiều cao">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-sm-4 control-label">Màu săc</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="product[properties][color]" class="form-control" placeholder="màu sắc">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Mô tả</label>

                            <div class="col-sm-10">
                                <textarea class="form-control" name="product[desc]" rows="3" placeholder="Mô tả ..."></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Avatar</label>
                            <div class="col-sm-10 form-group">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <img style="margin-left: 15px;height: 120px;width: 120px" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" id="avatar_img" class="profile-img-card avatar">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="file" id="file" name="product[avatar]">
                                        <em class="color-gray-light">(Hỗ trợ định dạng *.gif, *.png, *.jpg, *.bmp ) </em>
                                        <hr/>
                                        <button type="button" class="btn btn-primary" id="uploadAvatar">Tải ảnh</button>
                                    </div>
                                    <input hidden id="avatar_url" name="product[avatar]">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Slide</label>

                            <div class="col-sm-10">
                                <div class="row form-group">
                                    <div class="col-sm-6">
                                        <div class="row" >
                                            <div class="col-sm-4">
                                                <img style="margin-left: 15px;height: 120px;width: 120px" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" id="slide_img_0" class="profile-img-card avatar">
                                            </div>
                                            <div class="col-sm-8">
                                                <input type="file" id="file" name="product[avatar]">
                                                <em class="color-gray-light">(Hỗ trợ định dạng *.gif, *.png, *.jpg, *.bmp ) </em>
                                                <hr/>
                                                <button type="button" class="btn btn-primary" id="uploadSlide0">Tải ảnh</button>
                                            </div>
                                            <input hidden id="slide_url_0" name="product[slide][0]">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <img style="margin-left: 15px;height: 120px;width: 120px" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" id="slide_img_1" class="profile-img-card avatar">
                                            </div>
                                            <div class="col-sm-8">
                                                <input type="file" id="file" name="product[avatar]">
                                                <em class="color-gray-light">(Hỗ trợ định dạng *.gif, *.png, *.jpg, *.bmp ) </em>
                                                <hr/>
                                                <button type="button" class="btn btn-primary" id="uploadSlide1">Tải ảnh</button>
                                            </div>
                                            <input hidden id="slide_url_1" name="product[slide][1]">
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <img style="margin-left: 15px;height: 120px;width: 120px" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" id="slide_img_2" class="profile-img-card avatar">
                                            </div>
                                            <div class="col-sm-8">
                                                <input type="file" id="file" name="product[avatar]">
                                                <em class="color-gray-light">(Hỗ trợ định dạng *.gif, *.png, *.jpg, *.bmp ) </em>
                                                <hr/>
                                                <button type="button" class="btn btn-primary" id="uploadSlide2">Tải ảnh</button>
                                            </div>
                                            <input hidden id="slide_url_2" name="product[slide][2]">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Trạng thái</label>

                            <div class="col-sm-10">
                                <div class="row">
                                    <label class="col-sm-2 text-primary">
                                        <input value="2" type="radio" name="product[status]" checked>
                                        Hoạt động
                                    </label>
                                    <label class="col-sm-2 text-primary">
                                        <input value="1" type="radio" name="product[status]">
                                        Không hoạt động
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 text-right">Zalo</label>
                            <label class="col-sm-10">
                                <input type="checkbox" name="product[zalo]" value="1">
                            </label>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right" style="margin-right: 10px">Tạo</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<footer class="main-footer">

</footer>

