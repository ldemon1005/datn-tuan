<header class="main-header">
    <!-- Logo -->
    <a href="/backend" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>Z</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Đồ gỗ</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/../backend_res/assets/img/tree.png" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?= $auth['fullname'] ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="/../backend_res/assets/img/tree.png" class="img-circle" alt="User Image">

                            <p>
                                <?= $auth['fullname'] ?>
                                <?= $auth['email'] ?>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Hồ Sơ</a>
                            </div>
                            <div class="pull-right">
                                <a href="/backend/auth/logout" class="btn btn-default btn-flat">Đăng Xuất</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->

    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/../backend_res/assets/img/tree.png" class="img-circle" alt="logo đại lý">
            </div>
            <div class="pull-left info">
                <p><?= $auth['fullname'] ?></p>
                <a href="#"><i class="fa fa-user-circle"></i> <b>Trang quản lý</b></a>
            </div>
        </div>


        <ul class="sidebar-menu" data-widget="tree">
            <li >
                <a href="/backend/user" class="treeview">
                    <i class="fa fa-users"></i>
                    <span>Tài khoản</span>
                </a>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-product-hunt"></i>
                    <span>Sản phẩm</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/category"><i class="fa fa-circle-o"></i> Danh mục sản phẩm</a></li>
                    <li><a href="/backend/product"><i class="fa fa-circle-o"></i> Sản phẩm</a></li>
                </ul>
            </li>
            <li>
                <a href="/backend/order" class="treeview">
                    <i class="fa fa-cart-arrow-down"></i>
                    <span>Đơn hàng</span>
                </a>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-product-hunt"></i>
                    <span>Thông tin</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/contact"><i class="fa fa-circle-o"></i> Danh sách cửa hàng</a></li>
                    <li><a href="/backend/contactCustomer"><i class="fa fa-circle-o"></i> Danh sách liên hệ</a></li>
                </ul>
            </li>

            <li>
                <a href="/backend/articel" class="treeview">
                    <i class="fa fa-book"></i>
                    <span>Bài viết</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="col-sm-8 box-title">Danh sách đơn hàng</h3>
                    <div class="col-sm-4">
                        <?= $this->flash->output() ?>
                    </div>
                </div>
                <!-- /.box-header -->

                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="<?= ($Pagination['current'] > 1 ? '' : 'active') ?>"><a href="#activity" data-toggle="tab">Đơn trên website</a></li>
                        <li class="<?= ($Pagination['current'] > 1 ? 'active' : '') ?>"><a href="#timeline" data-toggle="tab">Đơn hàng từ zalo</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="<?= ($Pagination['current'] > 1 ? '' : 'active') ?> tab-pane" id="activity">
                            <div id="sec__customer" class="sec__page">
                                
                                    
                                
                                <table id="data-table" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Ngày tạo</th>
                                        <th>Mã đơn hàng</th>
                                        <th>Mã đơn VTP</th>
                                        <th>Tổng tiền</th>
                                        <th>Phí v/c</th>
                                        <th>Trạng thái</th>
                                        <th>Thao tác</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($list_order as $item) { ?>
                                        <tr>
                                            <td class="text-center"><?= $item['datecreate'] ?></td>
                                            <td class="text-center"><a href="/backend/order/detail/<?= $item['id'] ?>"><?= $item['order_code'] ?></a></td>
                                            <td class="text-center"><?= $item['vtp_code'] ?></td>
                                            <td class="text-center"><?= number_format($item['money']) ?></td>
                                            <td class="text-center"><?= number_format($item['vtp_info']['vtp_fee']) ?></td>
                                            <td class="text-center">
                                                <div class="input-group-btn">
                                                    <button type="button"
                                                            class="btn btn-block btn-sm  dropdown-toggle <?= ($item['status'] == 1 ? 'btn-warning' : '') ?> <?= ($item['status'] == 2 ? 'btn-primary' : '') ?> <?= ($item['status'] == 3 ? 'btn-success' : '') ?>"
                                                            data-toggle="dropdown"><?= $item['status_name'] ?></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="/backend/order/update/<?= $item['id'] ?>?value=1">Đơn mới</a></li>
                                                        <li><a href="/backend/order/update/<?= $item['id'] ?>?value=2">Đã thanh toán</a></li>
                                                        <li><a href="/backend/order/update/<?= $item['id'] ?>?value=3">Hoàn thành</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="row form-group">
                                                    <a data-toggle="tooltip" title="Xóa" href="#" class="col-sm-2 text-danger"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="<?= ($Pagination['current'] > 1 ? 'active' : '') ?> tab-pane" id="timeline">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr class="text-center">
                                    <th>Ngày tạo</th>
                                    <th>Mã đơn hàng</th>
                                    <th>Tổng tiền</th>
                                    <th>Phí v/c</th>
                                    <th>Trạng thái</th>
                                    <th>Thao tác</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($list_order_zalo as $item) { ?>
                                    <tr class="text-center">
                                        <td><?= $item['createdTime'] ?></td>
                                        <td><a href="/backend/order/detail/<?= $item['id'] ?>"><?= $item['orderCode'] ?></a></td>
                                        <td><?= number_format($item['price']) ?></td>
                                        <td><?= ($item['shippingInfo'] ? number_format($item['shippingInfo']['shippingFee']) : '') ?></td>
                                        <td class="text-center">
                                            <div class="input-group-btn">
                                                <button type="button"
                                                        class="btn btn-block btn-sm  dropdown-toggle btn-default"
                                                        data-toggle="dropdown">
                                                    <?= ($item['status'] == 1 ? 'Đơn mới' : '') ?>
                                                    <?= ($item['status'] == 2 ? 'Đơn đang xử lý' : '') ?>
                                                    <?= ($item['status'] == 3 ? 'Đơn xác nhận' : '') ?>
                                                    <?= ($item['status'] == 4 ? 'Đơn đang được giao' : '') ?>
                                                    <?= ($item['status'] == 5 ? 'Đơn hàng đã thành công' : '') ?>
                                                    <?= ($item['status'] == 6 ? 'Đơn đã bị hủy' : '') ?>
                                                    <?= ($item['status'] == 7 ? 'Đơn giao hàng thất bại' : '') ?>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="/backend/order/update/<?= $item['id'] ?>?value=1">Đơn mới</a></li>
                                                    <li><a href="/backend/order/update/<?= $item['id'] ?>?value=2">Đơn đang xử lý</a></li>
                                                    <li><a href="/backend/order/update/<?= $item['id'] ?>?value=3">Đơn xác nhận</a></li>
                                                    <li><a href="/backend/order/update/<?= $item['id'] ?>?value=4">Đơn đang được giao</a></li>
                                                    <li><a href="/backend/order/update/<?= $item['id'] ?>?value=5">Đơn hàng đã thành công</a></li>
                                                    <li><a href="/backend/order/update/<?= $item['id'] ?>?value=6">Đơn đã bị hủy</a></li>
                                                    <li><a href="/backend/order/update/<?= $item['id'] ?>?value=7">Đơn giao hàng thất bại</a></li>

                                                </ul>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <a data-toggle="tooltip" title="Xóa" href="#" class="text-danger"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>

                                <?php } ?>
                                </tbody>
                            </table>
                            <?php if ($Pagination['total'] > 0 && $Pagination['total_pages'] > 1 && $Pagination['current'] >= 1 && $Pagination['current'] <= $Pagination['total_pages']) { ?>
    <nav aria-label="Page navigation" class="page-pagination">
        <ul class="pagination">
            <?php if ($Pagination['current'] > 1) { ?>
                <li>
                    <a href="<?= $Pagination['current_link'] ?>p=<?= ($Pagination['current'] - 1) ?>" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
            <?php } ?>
            <?php foreach (range(1, $Pagination['total_pages']) as $i) { ?>
                <li><a href="<?= $Pagination['current_link'] ?>p=<?= $i ?>" class="<?= ($i == $Pagination['current'] ? 'active' : '') ?>"><?= $i ?></a></li>
            <?php } ?>
            <?php if ($Pagination['current'] < $Pagination['total_pages']) { ?>
                <li>
                    <a href="<?= $Pagination['current_link'] ?>p=<?= ($Pagination['current'] + 1) ?>" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </nav>
<?php } ?>
<?php if ($Pagination['total'] == 0) { ?>
    <p class="text-success text-center" style="font-size: 15px;padding-top: 10px">Không tìm thấy kết quả nào ở trang này</p>
<?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

<footer class="main-footer">

</footer>
