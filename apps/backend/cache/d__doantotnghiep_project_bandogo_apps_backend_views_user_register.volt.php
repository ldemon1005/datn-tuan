<header class="main-header">
    <!-- Logo -->
    <a href="/" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>Z</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Đồ gỗ</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/../backend_res/assets/img/tree.png" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?= $auth['fullname'] ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="/../backend_res/assets/img/tree.png" class="img-circle" alt="User Image">

                            <p>
                                <?= $auth['fullname'] ?>
                                <?= $auth['email'] ?>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Hồ Sơ</a>
                            </div>
                            <div class="pull-right">
                                <a href="/backend/auth/logout" class="btn btn-default btn-flat">Đăng Xuất</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->

    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/../backend_res/assets/img/tree.png" class="img-circle" alt="logo đại lý">
            </div>
            <div class="pull-left info">
                <p><?= $auth['fullname'] ?></p>
                <a href="#"><i class="fa fa-user-circle"></i> <b>Trang quản lý</b></a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Tìm kiếm...">
                <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i
                                    class="fa fa-search"></i>
                        </button>
                    </span>
            </div>
        </form>


        <ul class="sidebar-menu" data-widget="tree">
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Tài khoản</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/user/register"><i class="fa fa-circle-o"></i> Thêm tài khoản</a></li>
                    <li><a href="/backend/user"><i class="fa fa-circle-o"></i> Danh sách tài khoản</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-product-hunt"></i>
                    <span>Sản phẩm</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/category"><i class="fa fa-circle-o"></i> Danh mục sản phẩm</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Sản phẩm</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-book"></i>
                    <span>Bài viết</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/articel/create_articel"><i class="fa fa-circle-o"></i> Tạo mới bài viết</a></li>
                    <li><a href="/backend/articel"><i class="fa fa-circle-o"></i> Danh mục bài viết</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Danh mục bài viết</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Thêm mới tài khoản </h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" id="form-register" action="/user/register" method="post" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 text-primary">
                                </label>
                                <div class="col-sm-5">
                                    <?= $this->flash->output() ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Username</label>

                            <div class="col-sm-10">
                                <input type="text" name="user[username]" class="form-control" id="inputEmail3" placeholder="Username">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Password 1</label>

                            <div class="col-sm-10">
                                <input type="password" name="user[password]" class="form-control" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Password 2</label>

                            <div class="col-sm-10">
                                <input type="password" name="user[password2]" class="form-control" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Họ tên</label>

                            <div class="col-sm-10">
                                <input type="text" name="user[fullname]" class="form-control" placeholder="Họ và tên">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Số điện thoại</label>

                            <div class="col-sm-10">
                                <input type="text" name="user[phone]" class="form-control" placeholder="Số điện thoại">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Email</label>

                            <div class="col-sm-10">
                                <input type="email" name="user[email]" class="form-control" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Avatar</label>
                            <div class="col-sm-10 form-group">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <img style="margin-left: 15px;height: 120px;width: 120px" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" id="avatar_img" class="profile-img-card avatar">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="file" id="file" name="user[avatar]">
                                        <em class="color-gray-light">(Hỗ trợ định dạng *.gif, *.png, *.jpg, *.bmp ) </em>
                                        <hr/>
                                        <button type="button" class="btn btn-primary" id="uploadAvatar">Tải ảnh</button>
                                    </div>
                                    <input hidden id="avatar_url" name="user[avatar]">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Ngày sinh</label>

                            <div class="col-sm-10">
                                <input type="text" name="user[dob]" class="form-control" placeholder="dd/mm/yyyy">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Trạng thái</label>

                            <div class="col-sm-10">
                                <div class="row">
                                    <label class="col-sm-2 text-primary">
                                        <input value="2" type="radio" name="user[status]" checked>
                                        Hoạt động
                                    </label>
                                    <label class="col-sm-2 text-primary">
                                        <input value="1" type="radio" name="user[status]">
                                        Không hoạt động
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Địa chỉ</label>

                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <select name="user[city_id]" id="province_id" class="form-control selectpicker" data-live-search="true" style="width: 100%;">
                                            <option selected="selected">--Tỉnh/thành--</option>
                                            <?php foreach ($province as $item) { ?>
                                                <option value="<?= $item->id ?>"><?= $item->name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <select name="user[district_id]" id="district_id" class="form-control selectpicker" data-live-search="true" style="width: 100%;">
                                            <option selected="selected">--Quận/huyện--</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <select name="user[ward_id]" id="ward_id" class="form-control selectpicker" data-live-search="true" style="width: 100%;">
                                            <option selected="selected" class="text-center">--Xã/phường--</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Loại</label>

                            <div class="col-sm-10">
                                <div class="row">
                                    <label class="col-sm-2 text-primary">
                                        <input value="2" type="radio" name="user[type]" checked>
                                        Tài khoản thường
                                    </label>
                                    <label class="col-sm-2 text-primary">
                                        <input value="1" type="radio" name="user[type]">
                                        Tài khoản admin
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right" style="margin-right: 10px">Tạo</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<footer class="main-footer">

</footer>

