<header class="main-header">
    <!-- Logo -->
    <a href="/backend" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>Z</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Đồ gỗ</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/../backend_res/assets/img/tree.png" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?= $auth['fullname'] ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="/../backend_res/assets/img/tree.png" class="img-circle" alt="User Image">

                            <p>
                                <?= $auth['fullname'] ?>
                                <?= $auth['email'] ?>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Hồ Sơ</a>
                            </div>
                            <div class="pull-right">
                                <a href="/backend/auth/logout" class="btn btn-default btn-flat">Đăng Xuất</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->

    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/../backend_res/assets/img/tree.png" class="img-circle" alt="logo đại lý">
            </div>
            <div class="pull-left info">
                <p><?= $auth['fullname'] ?></p>
                <a href="#"><i class="fa fa-user-circle"></i> <b>Trang quản lý</b></a>
            </div>
        </div>


        <ul class="sidebar-menu" data-widget="tree">
            <li >
                <a href="/backend/user" class="treeview">
                    <i class="fa fa-users"></i>
                    <span>Tài khoản</span>
                </a>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-product-hunt"></i>
                    <span>Sản phẩm</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/category"><i class="fa fa-circle-o"></i> Danh mục sản phẩm</a></li>
                    <li><a href="/backend/product"><i class="fa fa-circle-o"></i> Sản phẩm</a></li>
                </ul>
            </li>
            <li>
                <a href="/backend/order" class="treeview">
                    <i class="fa fa-cart-arrow-down"></i>
                    <span>Đơn hàng</span>
                </a>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-product-hunt"></i>
                    <span>Thông tin</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/contact"><i class="fa fa-circle-o"></i> Danh sách cửa hàng</a></li>
                    <li><a href="/backend/contactCustomer"><i class="fa fa-circle-o"></i> Danh sách liên hệ</a></li>
                </ul>
            </li>

            <li>
                <a href="/backend/articel" class="treeview">
                    <i class="fa fa-book"></i>
                    <span>Bài viết</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Danh sách cửa hàng</h3>
                </div>
                <!-- /.box-header -->
                <div id="sec__customer" class="sec__page">
                    <div id="data_filter" class="data_filter">
                        <a class="btn btn-block btn-primary btn-sm" data-toggle="modal" data-target="#modal-default">Thêm mới</a>
                    </div>
                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Tên</th>
                            <th>Hotline</th>
                            <th>Số điện thoại</th>
                            <th>Email</th>
                            <th>Địa chỉ</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($contact as $item) { ?>
                            <tr>
                                <td class="text-center"><?= $item['name'] ?></td>
                                <td class="text-center"><?= $item['hotline'] ?></td>
                                <td class="text-center"><?= $item['phone'] ?></td>
                                <td class="text-center"><?= $item['email'] ?></td>
                                <td class="text-center"><?= $item['address'] ?></td>
                                <td class="text-center">
                                    <div class="row form-group">
                                        <a data-toggle="tooltip modal" title="Chỉnh sửa" data-target="#modal-update" href="#" id="update_contact" contact="<?= $item['id'] ?>" class="col-sm-2 text-primary"><i class="fa fa-wrench"></i></a>
                                        <a data-toggle="tooltip" title="Xóa" href="/backend/contact/delete/<?= $item['id'] ?>" class="col-sm-2 text-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <div class="modal modal-default fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-primary">Thêm mới liên hệ</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="/backend/contact/create" method="post">
                        <?= $this->flash->output() ?>
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-right">Tên liên hệ</label>

                                <div class="col-sm-9">
                                    <input type="text" name="contact[name]" class="form-control" placeholder="tên liên hệ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-right">Hotline</label>
                                <div class="col-sm-9">
                                    <input type="text" name="contact[hotline]" class="form-control" placeholder="hotline">
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-right">Số điện thoại</label>

                                <div class="col-sm-9">
                                    <input type="text" name="contact[phone]" class="form-control" placeholder="số điện thoại">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-right">Địa chỉ</label>

                                <div class="col-sm-9">
                                    <input type="text" name="contact[address]" class="form-control" placeholder="địa chỉ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-right"></label>

                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <select name="user[city_id]" id="province_id" class="form-control selectpicker" data-live-search="true" style="width: 100%;">
                                                <option selected="selected">--Tỉnh/thành--</option>
                                                <?php foreach ($province as $item) { ?>
                                                    <option value="<?= $item->id ?>"><?= $item->name ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <select name="user[district_id]" id="district_id" class="form-control selectpicker" data-live-search="true" style="width: 100%;">
                                                <option selected="selected">--Quận/huyện--</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <select name="user[ward_id]" id="ward_id" class="form-control selectpicker" data-live-search="true" style="width: 100%;">
                                                <option selected="selected" class="text-center">--Xã/phường--</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-right">Email</label>

                                <div class="col-sm-9">
                                    <input type="text" name="contact[email]" class="form-control" placeholder="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-right">Icon</label>

                                <div class="col-sm-9">
                                    <input type="text" name="contact[icon]" class="form-control" placeholder="icon">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">Thêm mới</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-default fade" id="modal-update">
    </div>
    <!-- /.content -->
</div>

<footer class="main-footer">

</footer>
