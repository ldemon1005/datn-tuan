<header class="main-header">
    <!-- Logo -->
    <a href="/backend" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>Z</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Đồ gỗ</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/../backend_res/assets/img/tree.png" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?= $auth['fullname'] ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="/../backend_res/assets/img/tree.png" class="img-circle" alt="User Image">

                            <p>
                                <?= $auth['fullname'] ?>
                                <?= $auth['email'] ?>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Hồ Sơ</a>
                            </div>
                            <div class="pull-right">
                                <a href="/backend/auth/logout" class="btn btn-default btn-flat">Đăng Xuất</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->

    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/../backend_res/assets/img/tree.png" class="img-circle" alt="logo đại lý">
            </div>
            <div class="pull-left info">
                <p><?= $auth['fullname'] ?></p>
                <a href="#"><i class="fa fa-user-circle"></i> <b>Trang quản lý</b></a>
            </div>
        </div>


        <ul class="sidebar-menu" data-widget="tree">
            <li >
                <a href="/backend/user" class="treeview">
                    <i class="fa fa-users"></i>
                    <span>Tài khoản</span>
                </a>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-product-hunt"></i>
                    <span>Sản phẩm</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/category"><i class="fa fa-circle-o"></i> Danh mục sản phẩm</a></li>
                    <li><a href="/backend/product"><i class="fa fa-circle-o"></i> Sản phẩm</a></li>
                </ul>
            </li>
            <li>
                <a href="/backend/order" class="treeview">
                    <i class="fa fa-cart-arrow-down"></i>
                    <span>Đơn hàng</span>
                </a>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-product-hunt"></i>
                    <span>Thông tin</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/contact"><i class="fa fa-circle-o"></i> Danh sách cửa hàng</a></li>
                    <li><a href="/backend/contactCustomer"><i class="fa fa-circle-o"></i> Danh sách liên hệ</a></li>
                </ul>
            </li>

            <li>
                <a href="/backend/articel" class="treeview">
                    <i class="fa fa-book"></i>
                    <span>Bài viết</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <div class="row">
                        <h3 class="box-title col-sm-8">Danh sách liên hệ</h3>
                        <div class="col-sm-4">
                            <?= $this->flash->output() ?>
                        </div>
                    </div>

                </div>
                <!-- /.box-header -->
                <div id="sec__customer" class="sec__page">
                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Họ và tên</th>
                            <th>Số điện thoại</th>
                            <th>Email</th>
                            <th>Ngày tạo</th>
                            <th>Nội dung</th>
                            <th>Trạng thái</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($list_contact as $item) { ?>
                            <tr>
                                <td class="text-center"><?= $item['fullname'] ?></td>
                                <td class="text-center"><?= $item['phone'] ?></td>
                                <td class="text-center"><?= $item['email'] ?></td>
                                <td class="text-center"><?= $item['datecreate'] ?></td>
                                <td class="text-center">
                                    <textarea><?= $item['content'] ?></textarea>
                                </td>
                                <td class="text-center">
                                    <button class="btn <?= ($item['status'] == 1 ? 'btn-primary' : 'btn-success') ?> text-center"><?= ($item['status'] == 1 ? 'Liên hệ mới' : 'Đã xử lý') ?></button>
                                </td>
                                <td class="text-center">
                                    <div class="row form-group">
                                        <a data-toggle="modal" data-target="#modal-sendmail" title="Gửi email" id="sendmail" contact="<?= $item['id'] ?>" email="<?= $item['email'] ?>"  class="pointer"><i class="fa fa-envelope"></i></a>
                                        <a title="Xóa" href="/backend/contactcustomer/delete/<?= $item['id'] ?>" class="col-sm-2 text-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <div class="modal modal-default fade" id="modal-sendmail">
        <!-- /.modal-dialog -->
    </div>
    <!-- /.content -->
</div>

<footer class="main-footer">

</footer>
