<header class="main-header">
    <!-- Logo -->
    <a href="/backend" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>Z</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Đồ gỗ</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/../backend_res/assets/img/tree.png" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?= $auth['fullname'] ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="/../backend_res/assets/img/tree.png" class="img-circle" alt="User Image">

                            <p>
                                <?= $auth['fullname'] ?>
                                <?= $auth['email'] ?>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Hồ Sơ</a>
                            </div>
                            <div class="pull-right">
                                <a href="/backend/auth/logout" class="btn btn-default btn-flat">Đăng Xuất</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->

    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/../backend_res/assets/img/tree.png" class="img-circle" alt="logo đại lý">
            </div>
            <div class="pull-left info">
                <p><?= $auth['fullname'] ?></p>
                <a href="#"><i class="fa fa-user-circle"></i> <b>Trang quản lý</b></a>
            </div>
        </div>


        <ul class="sidebar-menu" data-widget="tree">
            <li >
                <a href="/backend/user" class="treeview">
                    <i class="fa fa-users"></i>
                    <span>Tài khoản</span>
                </a>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-product-hunt"></i>
                    <span>Sản phẩm</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/category"><i class="fa fa-circle-o"></i> Danh mục sản phẩm</a></li>
                    <li><a href="/backend/product"><i class="fa fa-circle-o"></i> Sản phẩm</a></li>
                </ul>
            </li>
            <li>
                <a href="/backend/order" class="treeview">
                    <i class="fa fa-cart-arrow-down"></i>
                    <span>Đơn hàng</span>
                </a>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-product-hunt"></i>
                    <span>Thông tin</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/contact"><i class="fa fa-circle-o"></i> Danh sách cửa hàng</a></li>
                    <li><a href="/backend/contactCustomer"><i class="fa fa-circle-o"></i> Danh sách liên hệ</a></li>
                </ul>
            </li>

            <li>
                <a href="/backend/articel" class="treeview">
                    <i class="fa fa-book"></i>
                    <span>Bài viết</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Cập nhật danh mục </h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="" method="post">
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-right">Tên danh mục</label>

                            <div class="col-sm-9">
                                <input type="text" value="<?= $detail_category->name ?>" name="category[name]" class="form-control" placeholder="Tên danh mục">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-right">Avatar</label>
                            <div class="col-sm-9 form-group">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <img style="margin-left: 15px;height: 120px;width: 120px" src="<?= ($detail_category->avatar ? $detail_category->avatar : '//ssl.gstatic.com/accounts/ui/avatar_2x.png') ?>" id="avatar_img" class="profile-img-card avatar">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="file" id="file" name="user[avatar]">
                                        <em class="color-gray-light">(Hỗ trợ định dạng *.gif, *.png, *.jpg, *.bmp ) </em>
                                        <hr/>
                                        <button type="button" class="btn btn-primary" id="uploadAvatar">Tải ảnh</button>
                                    </div>
                                    <input hidden id="avatar_url" name="category[avatar]">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-right">Danh mục cha</label>

                            <div class="col-sm-9">
                                <select class="form-control" name="category[parent_id]">
                                    <?php foreach ($MenuRecusive as $item) { ?>
                                        <option <?= ($detail_category->parent_id == $item['id'] ? 'selected' : '') ?> value="<?= $item['id'] ?>"><?= $item['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-right">Mô tả</label>

                            <div class="col-sm-9" name="category[desc]">
                                <textarea class="form-control" name="category[desc]" rows="3" placeholder="Mô tả ..."><?= $detail_category->desc ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-right">Vị trí</label>

                            <div class="col-sm-9">
                                <input value="<?= $detail_category->order ?>" type="text" name="category[order]" class="form-control" placeholder="Vị trí hiển thị">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-right">Trạng thái</label>

                            <div class="col-sm-9">
                                <div class="row">
                                    <label class="col-sm-6 text-primary">
                                        <input value="2" type="radio" name="category[status]" <?= ($detail_category->status == 2 ? 'checked' : '') ?>  >
                                        Hoạt động
                                    </label>
                                    <label class="col-sm-6 text-primary">
                                        <input value="1" type="radio" name="category[status]" <?= ($detail_category->status == 1 ? 'checked' : '') ?>>
                                        Không hoạt động
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right">Cập nhật</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<footer class="main-footer">

</footer>
