<header class="main-header">
    <!-- Logo -->
    <a href="/" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>Z</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Đồ gỗ</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/../backend_res/assets/img/tree.png" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?= $auth['fullname'] ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="/../backend_res/assets/img/tree.png" class="img-circle" alt="User Image">

                            <p>
                                <?= $auth['fullname'] ?>
                                <?= $auth['email'] ?>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Hồ Sơ</a>
                            </div>
                            <div class="pull-right">
                                <a href="/backend/auth/logout" class="btn btn-default btn-flat">Đăng Xuất</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->

    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/../backend_res/assets/img/tree.png" class="img-circle" alt="logo đại lý">
            </div>
            <div class="pull-left info">
                <p><?= $auth['fullname'] ?></p>
                <a href="#"><i class="fa fa-user-circle"></i> <b>Trang quản lý</b></a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Tìm kiếm...">
                <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i
                                    class="fa fa-search"></i>
                        </button>
                    </span>
            </div>
        </form>


        <ul class="sidebar-menu" data-widget="tree">
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Tài khoản</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/user/register"><i class="fa fa-circle-o"></i> Thêm tài khoản</a></li>
                    <li><a href="/backend/user"><i class="fa fa-circle-o"></i> Danh sách tài khoản</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-product-hunt"></i>
                    <span>Sản phẩm</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/category"><i class="fa fa-circle-o"></i> Danh mục sản phẩm</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Sản phẩm</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-book"></i>
                    <span>Bài viết</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/articel/create_articel"><i class="fa fa-circle-o"></i> Tạo mới bài viết</a></li>
                    <li><a href="/backend/articel"><i class="fa fa-circle-o"></i> Danh mục bài viết</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Danh mục bài viết</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <div class="row">
                        <h3 class="box-title col-sm-8">Danh mục hàng hóa</h3>
                        <div class="col-sm-4">
                            <?= $this->flash->output() ?>
                        </div>
                    </div>

                </div>
                <!-- /.box-header -->
                <div id="sec__customer" class="sec__page">
                    <div id="data_filter" class="data_filter">
                        <a class="btn btn-block btn-primary btn-sm" data-toggle="modal" data-target="#modal-default">Thêm mới</a>
                    </div>
                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Tên</th>
                            <th>Ngày tạo</th>
                            <th>Trạng thái</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($ListCategory as $item) { ?>
                            <tr>
                                <td class="text-center"><?= $item->name ?></td>
                                <td class="text-center"><?= $item->datecreate ?></td>
                                <td class="text-center">
                                    <button class="btn btn-block btn-sm <?= ($itme->status == 1 ? 'btn-default' : 'btn-success') ?>"><?= ($itme->status == 1 ? 'Không hoạt động' : 'Hoạt động') ?></button>
                                </td>
                                <td class="text-center">
                                    <div class="row form-group">
                                        <a data-toggle="tooltip modal" title="Chỉnh sửa" href="#" id="update_category" category_id="<?= $item->id ?>" class="col-sm-2 text-primary"  data-target="#modal-update"><i class="fa fa-wrench"></i></a>
                                        <a data-toggle="tooltip" title="Xóa" href="/backend/category/delete/<?= $item->id ?>" class="col-sm-2 text-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="modal modal-default fade" id="modal-default">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-primary">Thêm mới danh mục</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" action="" method="post">
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label text-right">Tên danh mục</label>

                                    <div class="col-sm-9">
                                        <input type="text" name="category[name]" class="form-control" placeholder="Tên danh mục">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label text-right">Danh mục cha</label>

                                    <div class="col-sm-9">
                                        <select class="form-control" name="category[parent_id]">
                                            <?php foreach ($MenuRecusive as $item) { ?>
                                                <option value="<?= $item['id'] ?>"><?= $item['name'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label text-right">Mô tả</label>

                                    <div class="col-sm-9" name="category[desc]">
                                        <textarea class="form-control" name="category[desc]" rows="3" placeholder="Mô tả ..."></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label text-right">Trạng thái</label>

                                    <div class="col-sm-9">
                                        <div class="row">
                                            <label class="col-sm-6 text-primary">
                                                <input value="2" type="radio" name="category[status]" checked >
                                                Hoạt động
                                            </label>
                                            <label class="col-sm-6 text-primary">
                                                <input value="1" type="radio" name="category[status]">
                                                Không hoạt động
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Thêm mới</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal modal-default fade update_category" id="modal-update">
            <!-- /.modal-dialog -->
        </div>
    </section>
    <!-- /.content -->
</div>
<footer class="main-footer">

</footer>
