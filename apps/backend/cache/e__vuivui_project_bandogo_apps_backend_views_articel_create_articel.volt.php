<header class="main-header">
    <!-- Logo -->
    <a href="/backend" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>Z</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Đồ gỗ</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/../backend_res/assets/img/tree.png" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?= $auth['fullname'] ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="/../backend_res/assets/img/tree.png" class="img-circle" alt="User Image">

                            <p>
                                <?= $auth['fullname'] ?>
                                <?= $auth['email'] ?>
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Hồ Sơ</a>
                            </div>
                            <div class="pull-right">
                                <a href="/backend/auth/logout" class="btn btn-default btn-flat">Đăng Xuất</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->

    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/../backend_res/assets/img/tree.png" class="img-circle" alt="logo đại lý">
            </div>
            <div class="pull-left info">
                <p><?= $auth['fullname'] ?></p>
                <a href="#"><i class="fa fa-user-circle"></i> <b>Trang quản lý</b></a>
            </div>
        </div>


        <ul class="sidebar-menu" data-widget="tree">
            <li >
                <a href="/backend/user" class="treeview">
                    <i class="fa fa-users"></i>
                    <span>Tài khoản</span>
                </a>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-product-hunt"></i>
                    <span>Sản phẩm</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/category"><i class="fa fa-circle-o"></i> Danh mục sản phẩm</a></li>
                    <li><a href="/backend/product"><i class="fa fa-circle-o"></i> Sản phẩm</a></li>
                </ul>
            </li>
            <li>
                <a href="/backend/order" class="treeview">
                    <i class="fa fa-cart-arrow-down"></i>
                    <span>Đơn hàng</span>
                </a>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-product-hunt"></i>
                    <span>Thông tin</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/backend/contact"><i class="fa fa-circle-o"></i> Danh sách cửa hàng</a></li>
                    <li><a href="/backend/contactCustomer"><i class="fa fa-circle-o"></i> Danh sách liên hệ</a></li>
                </ul>
            </li>

            <li>
                <a href="/backend/articel" class="treeview">
                    <i class="fa fa-book"></i>
                    <span>Bài viết</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<div class="content-wrapper">
    <div class="box-header with-border">
        <h3 class="box-title">Thêm mới Bài viết </h3>
    </div>
    <section class="content">
        <form action="/backend/articel/create_articel/<?= $Articel['id'] ?>" method="post">
            <div class="row form-group">
                <label class="col-sm-2 control-label">Tên bài viết</label>
                <div class="col-sm-10">
                    <input type="text" name="articel[name]" value="<?= $Articel['name'] ?>" class="form-control" placeholder="Tên bài viết">
                </div>
            </div>
            <div class="row form-group">
                <label class="col-sm-2 control-label">Tiêu đề bài viết</label>
                <div class="col-sm-10">
                    <input type="text" name="articel[title]" value="<?= $Articel['title'] ?>" class="form-control" placeholder="Tiêu đề bài viết">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Avatar</label>
                <div class="col-sm-10 form-group">
                    <div class="row">
                        <div class="col-sm-2">
                            <img style="margin-left: 15px;height: 120px;width: 120px" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" id="avatar_img" class="profile-img-card avatar">
                        </div>
                        <div class="col-sm-4">
                            <input type="file" id="file" name="articel[avatar]">
                            <em class="color-gray-light">(Hỗ trợ định dạng *.gif, *.png, *.jpg, *.bmp ) </em>
                            <hr/>
                            <button type="button" class="btn btn-primary" id="uploadAvatar">Tải ảnh</button>
                        </div>
                        <input hidden id="avatar_url" value="<?= $Articel['avatar'] ?>" name="articel[avatar]">
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-sm-2 control-label">Trạng thái</label>
                <div class="col-sm-10">
                    <div class="row">
                        <label class="col-sm-2 text-primary">
                            <input value="2" type="radio" name="articel[status]" <?= ($Articel['category_id'] == 2 ? 'checked' : '') ?>>
                            Hoạt động
                        </label>
                        <label class="col-sm-2 text-primary">
                            <input value="1" type="radio" name="articel[status]" <?= ($Articel['category_id'] == 1 ? 'checked' : '') ?>>
                            Không hoạt động
                        </label>
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-sm-2 control-label">Nội dung bài viết</label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-info">
                                <!-- /.box-header -->
                                <div class="box-body pad">
                                    <textarea id="editor1" name="editor1" rows="10" cols="80">
                                        <?= ($Articel['content'] != '' ? $Articel['content'] : 'Nội dung bài viết') ?>
                                    </textarea>
                                </div>
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col-->
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right" style="margin-right: 10px"><?= ($Articel['id'] ? 'Cập nhật' : 'Tạo mới') ?></button>
            </div>
        </form>
        <!-- ./row -->
    </section>
</div>

<footer class="main-footer">

</footer>

