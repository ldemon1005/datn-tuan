<?php
namespace DoGo\Backend\Models;
/**
 * Created by PhpStorm.
 * User: ldemon1005
 * Date: 2/22/2018
 * Time: 9:31 PM
 */

class OrderProduct extends BaseModel
{
    /**
     *
     * @var string
     */
    public $order_id;

    /**
     *
     * @var integer
     */
    public $product_id;
    /**
     *
     * @var integer
     */
    public $quantity;


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'order_product';
    }

    /**
     * @return string
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * @param string $order_id
     */
    public function setOrderId($order_id)
    {
        $this->order_id = $order_id;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param int $product_id
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }
}