<?php
namespace DoGo\Backend\Models;
/**
 * Created by PhpStorm.
 * User: ldemon1005
 * Date: 2/22/2018
 * Time: 9:31 PM
 */

class Province extends BaseModel
{

    /**
     *
     * @var integer
     */
    public $city_id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var integer
     */
    public $zip_code;

    /**
     *
     * @var integer
     */
    public $vtp_id;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cities';
    }

    /**
     * @return int
     */
    public function getCityId()
    {
        return $this->city_id;
    }

    /**
     * @param int $city_id
     */
    public function setCityId($city_id)
    {
        $this->city_id = $city_id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getZipCode()
    {
        return $this->zip_code;
    }

    /**
     * @param int $zip_code
     */
    public function setZipCode($zip_code)
    {
        $this->zip_code = $zip_code;
    }

    /**
     * @return int
     */
    public function getVtpId()
    {
        return $this->vtp_id;
    }

    /**
     * @param int $vtp_id
     */
    public function setVtpId($vtp_id)
    {
        $this->vtp_id = $vtp_id;
    }






}