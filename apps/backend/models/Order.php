<?php
namespace DoGo\Backend\Models;
/**
 * Created by PhpStorm.
 * User: ldemon1005
 * Date: 2/22/2018
 * Time: 9:31 PM
 */

class Order extends BaseModel
{
    const PREFIX = 'DH-';
    const ORDER_NEW = 1;
    const ORDER_PAYMENT = 8;
    const ORDER_COMPLATE = 5;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $order_code;

    /**
     *
     * @var integer
     */
    public $store_id;
    /**
     *
     * @var integer
     */
    public $province_id;

    /**
     *
     * @var integer
     */
    public $district_id;

    /**
     *
     * @var integer
     */
    public $ward_id;
    /**
     *
     * @var integer
     */
    public $vtp_code;

    /**
     *
     * @var string
     */
    public $vtp_info;

    /**
     *
     * @var string
     */
    public $nl_info;

    /**
     *
     * @var double
     */
    public $money;

    /**
     *
     * @var integer
     */
    public $type_pay;
    /**
     *
     * @var integer
     */
    public $status;

    /**
     *
     * @var integer
     */
    public $datecreate;

    /**
     *
     * @var double
     */
    public $payment_money;

    /**
     *
     * @var double
     */
    public $del_flag;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'order';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getOrderCode()
    {
        return $this->order_code;
    }

    /**
     * @param string $order_code
     */
    public function setOrderCode($order_code)
    {
        $this->order_code = $order_code;
    }


    /**
     * @return int
     */
    public function getStoreId()
    {
        return $this->store_id;
    }

    /**
     * @param int $store_id
     */
    public function setStoreId($store_id)
    {
        $this->store_id = $store_id;
    }

    /**
     * @return int
     */
    public function getProvinceId()
    {
        return $this->province_id;
    }

    /**
     * @param int $province_id
     */
    public function setProvinceId($province_id)
    {
        $this->province_id = $province_id;
    }

    /**
     * @return int
     */
    public function getDistrictId()
    {
        return $this->district_id;
    }

    /**
     * @param int $district_id
     */
    public function setDistrictId($district_id)
    {
        $this->district_id = $district_id;
    }

    /**
     * @return int
     */
    public function getVtpCode()
    {
        return $this->vtp_code;
    }

    /**
     * @param int $vtp_code
     */
    public function setVtpCode($vtp_code)
    {
        $this->vtp_code = $vtp_code;
    }

    /**
     * @return float
     */
    public function getMoney()
    {
        return $this->money;
    }

    /**
     * @param float $money
     */
    public function setMoney($money)
    {
        $this->money = $money;
    }

    /**
     * @return int
     */
    public function getTypePay()
    {
        return $this->type_pay;
    }

    /**
     * @param int $type_pay
     */
    public function setTypePay($type_pay)
    {
        $this->type_pay = $type_pay;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getDatecreate()
    {
        return $this->datecreate;
    }

    /**
     * @param int $datecreate
     */
    public function setDatecreate($datecreate)
    {
        $this->datecreate = $datecreate;
    }

    /**
     * @return float
     */
    public function getPaymentMoney()
    {
        return $this->payment_money;
    }

    /**
     * @param float $payment_money
     */
    public function setPaymentMoney($payment_money)
    {
        $this->payment_money = $payment_money;
    }

    /**
     * @return float
     */
    public function getDelFlag()
    {
        return $this->del_flag;
    }

    /**
     * @param float $del_flag
     */
    public function setDelFlag($del_flag)
    {
        $this->del_flag = $del_flag;
    }

    /**
     * @return int
     */
    public function getWardId()
    {
        return $this->ward_id;
    }

    /**
     * @param int $ward_id
     */
    public function setWardId($ward_id)
    {
        $this->ward_id = $ward_id;
    }

    /**
     * @return string
     */
    public function getVtpInfo()
    {
        return $this->vtp_info;
    }

    /**
     * @param string $vtp_info
     */
    public function setVtpInfo($vtp_info)
    {
        $this->vtp_info = $vtp_info;
    }

    /**
     * @return string
     */
    public function getNlInfo()
    {
        return $this->nl_info;
    }

    /**
     * @param string $nl_info
     */
    public function setNlInfo($nl_info)
    {
        $this->nl_info = $nl_info;
    }

}