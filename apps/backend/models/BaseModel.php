<?php
namespace DoGo\Backend\Models;
/**
 * Created by PhpStorm.
 * User: ldemon1005
 * Date: 2/22/2018
 * Time: 7:25 PM
 */
use Phalcon\Mvc\Model\Behavior\SoftDelete;
use Phalcon\Mvc\Model\Query\Builder;

use Phalcon\Mvc\Model;

class BaseModel extends Model
{
    use \ObjectInstanceExt;

    const DELETED = 1;
    const NOT_DELETED = 0;
    const DEL_FLAG = 'del_flag';




    static function getSourceStatic()
    {
        return (new static())->getSource();
    }

    /**
     * @param      $id
     * @param null $fields
     *
     * @return static
     * @internal param null $parameters
     *
     */
    public static function findById($id, $fields = null)
    {
        if ($id == null) {
            return null;
        }
        $model = static::findFirst([
            'conditions' => 'id = :id:',
            'bind' => ['id' => $id],
            'columns' => $fields
        ]);
        return $model ? static::newInstance($model) : null;
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @param null $columns
     *
     * @return array
     */
    public function toArray($columns = null)
    {
        is_string($columns) && ($columns = preg_replace('/\s/', '', $columns)) && $columns = explode(',', $columns);
        $fields = $columns == null ? static::getFieldProperties() : array_intersect(static::getFieldProperties(), (array)$columns);
        $res = [];
        foreach ($fields as $field) {
            $getter = 'get' . str_replace('_', '', $field);
            if (method_exists($this, $getter)) {
                $value = call_user_func([
                    $this,
                    $getter
                ]);
                $res[$field] = $value;
            } else {
                $res[$field] = $this->$field;
            }
        }
        return $res;
    }

    /**
     * @param array|string $ignore
     * @return array
     */
    public static function getFieldProperties($ignore = '')
    {

        $props = array_keys(get_class_vars(static::class));
        $props = array_filter($props, function ($item) {
            return $item[0] === '_' ? null : $item;
        });

        is_string($ignore) && $ignore = explode(',', str_replace(' ', '', $ignore));
        $ignore != null && $props = array_diff($props, $ignore);

        return $props;
    }

    /**
     * Enable soft delete
     */
    public function beforeDelete()
    {
        if (property_exists(static::class, BaseModel::DEL_FLAG)) {
            $exist = static::findFirst([
                'conditions' => 'id = :id:',
                'bind' => [
                    'id' => $this->id
                ]
            ]);
            if ($exist == null) {
                return false;
            }
            $this->addBehavior(new SoftDelete([
                'field' => BaseModel::DEL_FLAG,
                'value' => BaseModel::DELETED
            ]));
        }
    }

    public static function post($url, $data, $token = null)
    {
        try {
            if ($token) {
                $response = \Httpful\Request::post($url)->body($data, Httpful\Mime::JSON)->addHeader('Authorization', $token)->send();
            } else {
                $response = \Httpful\Request::post($url)->body($data, Httpful\Mime::JSON)->send();
            }
        } catch (Exception $e) {
            echo '<pre>';
            echo "Message: " . $e->getMessage() . "\n</br>";
            echo "URL: " . $url . "\n</br>";
            echo "Data: <textarea rows='15' cols='100'>" . json_encode($data) . "</textarea>\n</br>";
            echo $e->getTraceAsString() . '</pre>';
            die;
        }
        return $response;
    }

    public static function put($url, $data)
    {
        try {
            $response = \Httpful\Request::put($url)->body($data, Httpful\Mime::JSON)->send();
        } catch (Exception $e) {
            echo '<pre>';
            echo "Message: " . $e->getMessage() . "\n</br>";
            echo "URL: " . $url . "\n</br>";
            echo "Data: <textarea rows='15' cols='100'>" . json_encode($data) . "</textarea>\n</br>";
            echo $e->getTraceAsString() . '</pre>';
            die;
        }
        return $response;
    }

    public static function get($url)
    {
        $response = \Httpful\Request::get($url)->expectsJson()->send();
        return $response->body;
    }
}