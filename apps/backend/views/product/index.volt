{% include "/layouts/header.volt" %}
{% include "layouts/sidebar.volt" %}

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <div class="row">
                        <h3 class="box-title col-sm-8">Danh sách hàng hóa</h3>
                        <div class="col-sm-4">
                            {{ flash.output() }}
                        </div>
                    </div>

                </div>
                <!-- /.box-header -->
                <div id="sec__customer" class="sec__page">
                    <div id="data_filter" class="data_filter">
                        <a class="btn btn-block btn-primary btn-sm" href="/backend/product/create">Thêm mới</a>
                    </div>
                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Tên</th>
                            <th>Danh mục</th>
                            <th>Giá</th>
                            <th>Giảm giá</th>
                            <th>Số lượng</th>
                            <th>Cân nặng</th>
                            <th>Ngày tạo</th>
                            <th>Thuộc tính</th>
                            <th>Trạng thái</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for item in products %}
                            <tr>
                                <td class="text-center">{{ item['name'] }}</td>
                                <td class="text-center">{{ item['category']['name'] }}</td>
                                <td class="text-center">{{ number_format(item['price']) }}</td>
                                <td class="text-center">{{ number_format(item['discount']) }}</td>
                                <td class="text-center">{{ number_format(item['quantity']) }}</td>
                                <td class="text-center">{{ number_format(item['weight']) }}</td>
                                <td class="text-center">{{ item['datecreate'] }}</td>
                                <td class="text-center">
                                    <p>Chiều dài : {{ item['properties']['length'] }}</p>
                                    <p>Chiều rộng : {{ item['properties']['width'] }}</p>
                                    <p>Chiều cao : {{ item['properties']['heigth'] }}</p>
                                    <p>Màu sắc : {{ item['properties']['color'] }}</p>
                                </td>
                                <td class="text-center">
                                    <button class="btn btn-block btn-sm {{ item['status'] == 1 ? 'btn-default' : 'btn-success' }}">{{ item['status'] == 1 ? 'Không hoạt động' : 'Hoạt động' }}</button>
                                </td>
                                <td class="text-center">
                                    <div class="row form-group">
                                        <a data-toggle="tooltip" title="Chỉnh sửa" href="/backend/product/update/{{ item['id'] }}" class="col-sm-2 text-primary"><i class="fa fa-wrench"></i></a>
                                        <a data-toggle="tooltip" title="Xóa" href="/backend/product/delete/{{ item['id'] }}" class="col-sm-2 text-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
{% include "/layouts/footer.volt" %}
