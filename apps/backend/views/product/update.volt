{% include "/layouts/header.volt" %}
{% include "/layouts/sidebar.volt" %}
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Cập nhật sản phẩm </h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" id="form-register" action="/backend/product/update/{{ product['id'] }}" method="post" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-sm-2 text-primary">
                                </label>
                                <div class="col-sm-5">
                                    {{ flash.output() }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tên sản phẩm</label>

                            <div class="col-sm-10">
                                <input type="text" name="product[name]" value="{{ product['name'] }}" class="form-control" id="inputEmail3" placeholder="tên sản phẩm">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Danh mục</label>

                            <div class="col-sm-10">
                                <select class="form-control" name="product[category_id]">
                                    {% for item in MenuRecusive %}
                                        <option {{ product['category_id'] == item.id ? 'selected' : '' }} value="{{ item['id'] }}">{{ item['name'] }}</option>
                                    {% endfor %}
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Giá</label>

                            <div class="col-sm-10">
                                <input type="text" name="product[price]" value="{{ number_format(product['price']) }}" class="form-control" placeholder="giá">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Loại giảm giá</label>

                            <div class="col-sm-10">
                                <div class="col-sm-10">
                                    <div class="row">
                                        <label class="col-sm-4 text-primary">
                                            <input value="2" type="radio" name="product[type_discount]" {{ product['type_discount'] == 2 ? 'checked' : '' }}>
                                            Giảm theo phần trăm
                                        </label>
                                        <label class="col-sm-4 text-primary">
                                            <input value="1" type="radio" name="product[type_discount]" {{ product['type_discount'] == 1 ? 'checked' : '' }}>
                                            Giảm theo giá trị
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Giá trị giảm giá</label>

                            <div class="col-sm-10">
                                <input type="text" name="product[discount]" value="{{ number_format(product['discount']) }}" class="form-control" placeholder="giá trị giảm giá">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Số lượng</label>
                            <div class="col-sm-10">
                                <input type="text" name="product[quantity]" value="{{ product['quantity'] }}" class="form-control" placeholder="số lượng">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Cân nặng</label>
                            <div class="col-sm-10">
                                <input type="text" name="product[weight]" value="{{ product['weight'] }}" class="form-control" placeholder="số lượng">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Thuộc tính</label>

                            <div class="col-sm-10">
                                <div class="row form-group">
                                    <div class="col-sm-6">
                                        <label class="col-sm-4 control-label">Chiều dài</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="product[properties][length]" value="{{ product['properties']['length'] }}" class="form-control" placeholder="chiều dài">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-sm-4 control-label">Chiều rộng</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="product[properties][width]" value="{{ product['properties']['width'] }}" class="form-control" placeholder="chiều rộng">
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-6">
                                        <label class="col-sm-4 control-label">Chiều cao</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="product[properties][heigth]" value="{{ product['properties']['heigth'] }}" class="form-control" placeholder="chiều cao">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="col-sm-4 control-label">Màu săc</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="product[properties][color]" value="{{ product['properties']['color'] }}" class="form-control" placeholder="màu sắc">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Mô tả</label>

                            <div class="col-sm-10">
                                <textarea class="form-control" name="product[desc]" rows="3" placeholder="Mô tả ...">{{ product['desc'] }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Avatar</label>
                            <div class="col-sm-10 form-group">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <img style="margin-left: 15px;height: 120px;width: 120px" src="{{ product['avatar'] ? product['avatar'] : '//ssl.gstatic.com/accounts/ui/avatar_2x.png' }}" id="avatar_img" class="profile-img-card avatar">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="file" id="file" name="product[avatar]">
                                        <em class="color-gray-light">(Hỗ trợ định dạng *.gif, *.png, *.jpg, *.bmp ) </em>
                                        <hr/>
                                        <button type="button" class="btn btn-primary" id="uploadAvatar">Tải ảnh</button>
                                    </div>
                                    <input hidden id="avatar_url" name="product[avatar]">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Slide</label>

                            <div class="col-sm-10">
                                <div class="row form-group">
                                    <div class="col-sm-6">
                                        <div class="row" >
                                            <div class="col-sm-4">
                                                <img style="margin-left: 15px;height: 120px;width: 120px" src="{{ product['slide_show'][0] ? product['slide_show'][0] : '//ssl.gstatic.com/accounts/ui/avatar_2x.png' }}" id="slide_img_0" class="profile-img-card avatar">
                                            </div>
                                            <div class="col-sm-8">
                                                <input type="file" id="file" name="product[avatar]">
                                                <em class="color-gray-light">(Hỗ trợ định dạng *.gif, *.png, *.jpg, *.bmp ) </em>
                                                <hr/>
                                                <button type="button" class="btn btn-primary" id="uploadSlide0">Tải ảnh</button>
                                            </div>
                                            <input value="{{ product['slide_show'][0] }}" hidden id="slide_url_0" name="product[slide][0]">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <img style="margin-left: 15px;height: 120px;width: 120px" src="{{ product['slide_show'][1] ? product['slide_show'][1] : '//ssl.gstatic.com/accounts/ui/avatar_2x.png' }}" id="slide_img_1" class="profile-img-card avatar">
                                            </div>
                                            <div class="col-sm-8">
                                                <input type="file" id="file" name="product[avatar]">
                                                <em class="color-gray-light">(Hỗ trợ định dạng *.gif, *.png, *.jpg, *.bmp ) </em>
                                                <hr/>
                                                <button type="button" class="btn btn-primary" id="uploadSlide1">Tải ảnh</button>
                                            </div>
                                            <input value="{{ product['slide_show'][1] }}" hidden id="slide_url_1" name="product[slide][1]">
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <img style="margin-left: 15px;height: 120px;width: 120px" src="{{ product['slide_show'][2] ? product['slide_show'][2] : '//ssl.gstatic.com/accounts/ui/avatar_2x.png' }}" id="slide_img_2" class="profile-img-card avatar">
                                            </div>
                                            <div class="col-sm-8">
                                                <input type="file" id="file" name="product[avatar]">
                                                <em class="color-gray-light">(Hỗ trợ định dạng *.gif, *.png, *.jpg, *.bmp ) </em>
                                                <hr/>
                                                <button type="button" class="btn btn-primary" id="uploadSlide2">Tải ảnh</button>
                                            </div>
                                            <input value="{{ product['slide_show'][2] }}" hidden id="slide_url_2" name="product[slide][2]">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Trạng thái</label>

                            <div class="col-sm-10">
                                <div class="row">
                                    <label class="col-sm-2 text-primary">
                                        <input value="2" type="radio" name="product[status]" checked>
                                        Hoạt động
                                    </label>
                                    <label class="col-sm-2 text-primary">
                                        <input value="1" type="radio" name="product[status]">
                                        Không hoạt động
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right" style="margin-right: 10px">Cập nhật</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
{% include "/layouts/footer.volt" %}

