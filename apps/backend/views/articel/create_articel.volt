{% include "/layouts/header.volt" %}
{% include "layouts/sidebar.volt" %}
<div class="content-wrapper">
    <div class="box-header with-border">
        <h3 class="box-title">Thêm mới Bài viết </h3>
    </div>
    <section class="content">
        <form action="/backend/articel/create_articel/{{ Articel['id'] }}" method="post">
            <div class="row form-group">
                <label class="col-sm-2 control-label">Tên bài viết</label>
                <div class="col-sm-10">
                    <input type="text" name="articel[name]" value="{{ Articel['name'] }}" class="form-control" placeholder="Tên bài viết">
                </div>
            </div>
            <div class="row form-group">
                <label class="col-sm-2 control-label">Tiêu đề bài viết</label>
                <div class="col-sm-10">
                    <input type="text" name="articel[title]" value="{{ Articel['title'] }}" class="form-control" placeholder="Tiêu đề bài viết">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Avatar</label>
                <div class="col-sm-10 form-group">
                    <div class="row">
                        <div class="col-sm-2">
                            <img style="margin-left: 15px;height: 120px;width: 120px" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" id="avatar_img" class="profile-img-card avatar">
                        </div>
                        <div class="col-sm-4">
                            <input type="file" id="file" name="articel[avatar]">
                            <em class="color-gray-light">(Hỗ trợ định dạng *.gif, *.png, *.jpg, *.bmp ) </em>
                            <hr/>
                            <button type="button" class="btn btn-primary" id="uploadAvatar">Tải ảnh</button>
                        </div>
                        <input hidden id="avatar_url" value="{{ Articel['avatar'] }}" name="articel[avatar]">
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-sm-2 control-label">Trạng thái</label>
                <div class="col-sm-10">
                    <div class="row">
                        <label class="col-sm-2 text-primary">
                            <input value="2" type="radio" name="articel[status]" {{ Articel['category_id'] == 2 ? 'checked' : '' }}>
                            Hoạt động
                        </label>
                        <label class="col-sm-2 text-primary">
                            <input value="1" type="radio" name="articel[status]" {{ Articel['category_id'] == 1 ? 'checked' : '' }}>
                            Không hoạt động
                        </label>
                    </div>
                </div>
            </div>
            <div class="row form-group">
                <label class="col-sm-2 control-label">Nội dung bài viết</label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-info">
                                <!-- /.box-header -->
                                <div class="box-body pad">
                                    <textarea id="editor1" name="editor1" rows="10" cols="80">
                                        {{ Articel['content'] != '' ? Articel['content'] : 'Nội dung bài viết' }}
                                    </textarea>
                                </div>
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col-->
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right" style="margin-right: 10px">{{ Articel['id'] ? 'Cập nhật' : 'Tạo mới' }}</button>
            </div>
        </form>
        <!-- ./row -->
    </section>
</div>

{% include "/layouts/footer.volt" %}

