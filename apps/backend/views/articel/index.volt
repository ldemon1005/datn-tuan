{% include "/layouts/header.volt" %}
{% include "layouts/sidebar.volt" %}

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Danh sách bài viết</h3>
                </div>
                <div>
                    {{ flash.output() }}
                </div>
                <!-- /.box-header -->
                <div id="sec__customer" class="sec__page">
                    <div id="data_filter" class="data_filter">
                        <a class="btn btn-block btn-primary btn-sm" href="/backend/articel/create_articel">Thêm mới</a>
                    </div>
                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Tên bài viết</th>
                            <th>avatar</th>
                            <th>Ngày tạo</th>
                            <th>Trạng thái</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for item in ListArticel %}
                            <tr>
                                <td class="text-center">{{ item['name'] }}</td>
                                <td class="text-center"><img style="height: 50px" src="{{ item['avatar'] }}"></td>
                                <td class="text-center">{{ item['datecreate'] }}</td>
                                <td class="text-center">
                                    <button class="btn btn-block btn-sm {{ itme['status == 1'] ? 'btn-default' : 'btn-success' }}">{{ itme['status == 1'] ? 'Không hoạt động' : 'Hoạt động' }}</button>
                                </td>
                                <td class="text-center">
                                    <div class="row form-group">
                                        <a href="/backend/articel/create_articel/{{ item['id'] }}" class="col-sm-2 text-primary"><i class="fa fa-wrench"></i></a>
                                        <a data-toggle="tooltip" title="Xóa" href="/backend/articel/delete/{{ item['id'] }}" class="col-sm-2 text-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

{% include "/layouts/footer.volt" %}
