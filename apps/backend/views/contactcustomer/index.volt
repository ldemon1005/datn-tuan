{% include "/layouts/header.volt" %}
{% include "layouts/sidebar.volt" %}

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <div class="row">
                        <h3 class="box-title col-sm-8">Danh sách liên hệ</h3>
                        <div class="col-sm-4">
                            {{ flash.output() }}
                        </div>
                    </div>

                </div>
                <!-- /.box-header -->
                <div id="sec__customer" class="sec__page">
                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Họ và tên</th>
                            <th>Số điện thoại</th>
                            <th>Email</th>
                            <th>Ngày tạo</th>
                            <th>Nội dung</th>
                            <th>Trạng thái</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for item in list_contact %}
                            <tr>
                                <td class="text-center">{{ item['fullname'] }}</td>
                                <td class="text-center">{{ item['phone'] }}</td>
                                <td class="text-center">{{ item['email'] }}</td>
                                <td class="text-center">{{ item['datecreate'] }}</td>
                                <td class="text-center">
                                    <textarea>{{ item['content'] }}</textarea>
                                </td>
                                <td class="text-center">
                                    <button class="btn {{ item['status'] == 1 ? 'btn-primary' : 'btn-success' }} text-center">{{ item['status'] == 1 ? 'Liên hệ mới' : 'Đã xử lý' }}</button>
                                </td>
                                <td class="text-center">
                                    <div class="row form-group">
                                        <a data-toggle="modal" data-target="#modal-sendmail" title="Gửi email" id="sendmail" contact="{{ item['id'] }}" email="{{ item['email'] }}"  class="pointer"><i class="fa fa-envelope"></i></a>
                                        <a title="Xóa" href="/backend/contactcustomer/delete/{{ item['id'] }}" class="col-sm-2 text-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <div class="modal modal-default fade" id="modal-sendmail">
        <!-- /.modal-dialog -->
    </div>
    <!-- /.content -->
</div>

{% include "/layouts/footer.volt" %}
