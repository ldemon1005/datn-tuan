{% set contact = object['contact'] %}
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title text-primary">Gửi email</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" action="/backend/contactCustomer/sendmail/{{ contact['id'] }}" method="post">
                {{ flash.output() }}
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-right">Email</label>

                        <div class="col-sm-9">
                            <input type="text" value="{{ contact['email'] }}" name="email[email]" class="form-control" placeholder="email">
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-right">Nội dung email</label>

                        <div class="col-sm-9">
                            <textarea type="text" name="email[content]" rows="5" class="form-control" placeholder="Nội dung email"></textarea>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">Gửi email</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </div>
    <!-- /.modal-content -->
</div>