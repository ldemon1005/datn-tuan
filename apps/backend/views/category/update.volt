{% include "/layouts/header.volt" %}
{% include "/layouts/sidebar.volt" %}
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Cập nhật danh mục </h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="" method="post">
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-right">Tên danh mục</label>

                            <div class="col-sm-9">
                                <input type="text" value="{{ detail_category.name }}" name="category[name]" class="form-control" placeholder="Tên danh mục">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-right">Avatar</label>
                            <div class="col-sm-9 form-group">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <img style="margin-left: 15px;height: 120px;width: 120px" src="{{ detail_category.avatar ? detail_category.avatar : '//ssl.gstatic.com/accounts/ui/avatar_2x.png' }}" id="avatar_img" class="profile-img-card avatar">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="file" id="file" name="user[avatar]">
                                        <em class="color-gray-light">(Hỗ trợ định dạng *.gif, *.png, *.jpg, *.bmp ) </em>
                                        <hr/>
                                        <button type="button" class="btn btn-primary" id="uploadAvatar">Tải ảnh</button>
                                    </div>
                                    <input hidden id="avatar_url" name="category[avatar]">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-right">Danh mục cha</label>

                            <div class="col-sm-9">
                                <select class="form-control" name="category[parent_id]">
                                    {% for item in MenuRecusive %}
                                        <option {{ detail_category.parent_id == item['id'] ? 'selected' : '' }} value="{{ item['id'] }}">{{ item['name'] }}</option>
                                    {% endfor %}
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-right">Mô tả</label>

                            <div class="col-sm-9" name="category[desc]">
                                <textarea class="form-control" name="category[desc]" rows="3" placeholder="Mô tả ...">{{ detail_category.desc }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-right">Vị trí</label>

                            <div class="col-sm-9">
                                <input value="{{ detail_category.order }}" type="text" name="category[order]" class="form-control" placeholder="Vị trí hiển thị">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-right">Trạng thái</label>

                            <div class="col-sm-9">
                                <div class="row">
                                    <label class="col-sm-6 text-primary">
                                        <input value="2" type="radio" name="category[status]" {{ detail_category.status == 2 ? 'checked' : '' }}  >
                                        Hoạt động
                                    </label>
                                    <label class="col-sm-6 text-primary">
                                        <input value="1" type="radio" name="category[status]" {{ detail_category.status == 1 ? 'checked' : '' }}>
                                        Không hoạt động
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right">Cập nhật</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
{% include "/layouts/footer.volt" %}
