{% include "/layouts/header.volt" %}
{% include "layouts/sidebar.volt" %}

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <div class="row">
                        <h3 class="box-title col-sm-8">Danh mục hàng hóa</h3>
                        <div class="col-sm-4">
                            {{ flash.output() }}
                        </div>
                    </div>

                </div>
                <!-- /.box-header -->
                <div id="sec__customer" class="sec__page">
                    <div id="data_filter" class="data_filter">
                        <a class="btn btn-block btn-primary btn-sm" data-toggle="modal" data-target="#modal-default">Thêm mới</a>
                    </div>
                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Tên</th>
                            <th>Ngày tạo</th>
                            <th>Trạng thái</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for item in ListCategory %}
                            <tr>
                                <td class="text-center">{{ item['name'] }}</td>
                                <td class="text-center">{{ item['datecreate'] }}</td>
                                <td class="text-center">
                                    <button class="btn btn-block btn-sm {{ item['status'] == 1 ? 'btn-default' : 'btn-success' }}">{{ item['status'] == 1 ? 'Không hoạt động' : 'Hoạt động' }}</button>
                                </td>
                                <td class="text-center">
                                    <div class="row form-group">
                                        <a data-toggle="tooltip" title="Chỉnh sửa" href="/backend/category/update/{{ item['id'] }}" class="col-sm-2 text-primary"><i class="fa fa-wrench"></i></a>
                                        <a data-toggle="tooltip" title="Xóa" href="/backend/category/delete/{{ item['id'] }}" class="col-sm-2 text-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="modal modal-default fade" id="modal-default">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-primary">Thêm mới danh mục</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" action="" method="post">
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label text-right">Tên danh mục</label>

                                    <div class="col-sm-9">
                                        <input type="text" name="category[name]" class="form-control" placeholder="Tên danh mục">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Ảnh đại diện</label>
                                    <div class="col-sm-9 form-group">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <img style="margin-left: 15px;height: 120px;width: 120px" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" id="avatar_img" class="profile-img-card avatar">
                                            </div>
                                            <div class="col-sm-7">
                                                <input type="file" id="file" name="user[avatar]">
                                                <em>(Hỗ trợ định dạng *.gif, *.png, *.jpg) </em>
                                                <hr/>
                                                <button type="button" class="btn btn-primary" id="uploadAvatar">Tải ảnh</button>
                                            </div>
                                            <input hidden id="avatar_url" name="category[avatar]">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 text-right">Đồng bộ Zalo</label>
                                    <label class="col-sm-9">
                                        <input type="checkbox" name="category[zalo]" value="1">
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label text-right">Danh mục cha</label>

                                    <div class="col-sm-9">
                                        <select class="form-control" name="category[parent_id]">
                                            {% for item in MenuRecusive %}
                                                <option value="{{ item['id'] }}">{{ item['name'] }}</option>
                                            {% endfor %}
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label text-right">Mô tả</label>

                                    <div class="col-sm-9" name="category[desc]">
                                        <textarea class="form-control" name="category[desc]" rows="3" placeholder="Mô tả ..."></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label text-right">Vị trí</label>

                                    <div class="col-sm-9">
                                        <input type="text" name="category[order]" class="form-control" placeholder="Vị trí hiển thị">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label text-right">Trạng thái</label>

                                    <div class="col-sm-9">
                                        <div class="row">
                                            <label class="col-sm-6 text-primary">
                                                <input value="2" type="radio" name="category[status]" checked >
                                                Hoạt động
                                            </label>
                                            <label class="col-sm-6 text-primary">
                                                <input value="1" type="radio" name="category[status]">
                                                Không hoạt động
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Thêm mới</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </section>
    <!-- /.content -->
</div>
{% include "/layouts/footer.volt" %}
