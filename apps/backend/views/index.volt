<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Đồ gỗ hiện đại</title>
    <!-- Tell the browser to be responsive to screen width -->
    <link rel="shortcut icon" href="/backend_res/assets/img/favicon.ico">

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="/backend_res/source/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/backend_res/source/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="/backend_res/source/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="/backend_res/source/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/backend_res/source/dist/css/AdminLTE.min.css">
    <!-- Custom CSS Tuandv -->
    {#<link rel="stylesheet" href="/backend_res/source/dist/css/custom.css">#}
    <link rel="stylesheet" href="/backend_res/assets/css/custom.css">
    <link rel="stylesheet" href="/backend_res/assets/css/bootstrap-select.min.css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/backend_res/source/dist/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="/backend_res/assets/css/skin-bizsale.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="/backend_res/source/bower_components/morris.js/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="/backend_res/source/bower_components/jvectormap/jquery-jvectormap.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="/backend_res/source/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="/backend_res/source/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="/backend_res/source/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-bizsale sidebar-mini">
<div class="wrapper">
    {{ content() }}
    {% include "layouts/control-sidebar.volt" %}
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>

<script src="/backend_res/source/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="/backend_res/source/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="/backend_res/source/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="/backend_res/source/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/backend_res/source/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="/backend_res/source/bower_components/raphael/raphael.min.js"></script>
<script src="/backend_res/source/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="/backend_res/source/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->

<!-- jQuery Knob Chart -->
<script src="/backend_res/source/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- FLOT CHARTS -->
<script src="/backend_res/source/bower_components/Flot/jquery.flot.js"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="/backend_res/source/bower_components/Flot/jquery.flot.resize.js"></script>
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
<script src="/backend_res/source/bower_components/Flot/jquery.flot.pie.js"></script>
<!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
<script src="/backend_res/source/bower_components/Flot/jquery.flot.categories.js"></script>
<!-- ChartJS -->
<script src="/backend_res/source/bower_components/Chart.js/Chart.js"></script>
<!-- daterangepicker -->
<script src="/backend_res/source/bower_components/moment/min/moment.min.js"></script>
<script src="/backend_res/source/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="/backend_res/source/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="/backend_res/source/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="/backend_res/source/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/backend_res/source/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/backend_res/source/dist/js/adminlte.min.js"></script>
<!-- CK Editor -->
<script src="/backend_res/source/bower_components/ckeditor/ckeditor.js"></script>

<!-- Custom JS of Daily-Bizsale -->
<script src="/backend_res/assets/js/jquery.validate.min.js"></script>
<script src="/backend_res/assets/js/custom.js"></script>
<script src="/backend_res/assets/js/bootstrap-select.js"></script>
<script>
    $(function () {
        CKEDITOR.replace( 'editor1', {
            filebrowserBrowseUrl: '/backend_res/source/bower_components/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl: '/backend_res/source/bower_components/ckfinder/ckfinder.html?type=Images',
            filebrowserFlashBrowseUrl: '/backend_res/source/bower_components/ckfinder/ckfinder.html?type=Flash',
            filebrowserUploadUrl: '/backend_res/source/bower_components/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: '/backend_res/source/bower_components/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            filebrowserFlashUploadUrl: '/backend_res/source/bower_components/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
        } );
    })
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".filter-option-inner").html("Chọn danh mục bài viết");
        $('#uploadAvatar').on('click', function() {
            var file_data = $('#file').prop('files')[0];
            var type = file_data.type;
            var match= ["image/gif","image/png","image/jpg","image/jpeg"];
            if(type == match[0] || type == match[1] || type == match[2] || type == match[3])
            {
                var form_data = new FormData();
                form_data.append('file', file_data);
                $.ajax({
                    url: '/backend/user/upload',
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success:function (result) {
                        var data = JSON.parse(result);
                        console.log(data);
                        if(data.code == 1){
                            $('#avatar_img').attr('src',data.url);
                            $('#avatar_url').attr('value',data.url);
                        }
                    }
                });
            } else{
                $('.status').text('Chỉ được upload file ảnh');
                $('#file').val('');
            }
            return false;
        });
        $('#uploadSlide0').on('click', function() {
            var file_data = $('#file0').prop('files')[0];
            var type = file_data.type;
            var match= ["image/gif","image/png","image/jpg","image/jpeg"];
            if(type == match[0] || type == match[1] || type == match[2] || type == match[3])
            {
                var form_data = new FormData();
                form_data.append('file', file_data);
                $.ajax({
                    url: '/backend/user/upload',
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success:function (result) {
                        var data = JSON.parse(result);
                        if(data.code == 1){
                            $('#slide_img_0').attr('src',data.url);
                            $('#slide_url_0').attr('value',data.url);
                        }
                    }
                });
            } else{
                $('.status').text('Chỉ được upload file ảnh');
                $('#file').val('');
            }
            return false;
        });
        $('#uploadSlide1').on('click', function() {
            var file_data = $('#file1').prop('files')[0];
            var type = file_data.type;
            var match= ["image/gif","image/png","image/jpg","image/jpeg"];
            if(type == match[0] || type == match[1] || type == match[2] || type == match[3])
            {
                var form_data = new FormData();
                form_data.append('file', file_data);
                $.ajax({
                    url: '/backend/user/upload',
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success:function (result) {
                        var data = JSON.parse(result);
                        if(data.code == 1){
                            $('#slide_img_1').attr('src',data.url);
                            $('#slide_url_1').attr('value',data.url);
                        }
                    }
                });
            } else{
                $('.status').text('Chỉ được upload file ảnh');
                $('#file').val('');
            }
            return false;
        });
        $('#uploadSlide2').on('click', function() {
            var file_data = $('#file2').prop('files')[0];
            var type = file_data.type;
            var match= ["image/gif","image/png","image/jpg","image/jpeg"];
            if(type == match[0] || type == match[1] || type == match[2] || type == match[3])
            {
                var form_data = new FormData();
                form_data.append('file', file_data);
                $.ajax({
                    url: '/backend/user/upload',
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success:function (result) {
                        var data = JSON.parse(result);
                        if(data.code == 1){
                            $('#slide_img_2').attr('src',data.url);
                            $('#slide_url_2').attr('value',data.url);
                        }
                    }
                });
            } else{
                $('.status').text('Chỉ được upload file ảnh');
                $('#file').val('');
            }
            return false;
        });
        $('#uploadSlide3').on('click', function() {
            var file_data = $('#file3').prop('files')[0];
            var type = file_data.type;
            var match= ["image/gif","image/png","image/jpg","image/jpeg"];
            if(type == match[0] || type == match[1] || type == match[2] || type == match[3])
            {
                var form_data = new FormData();
                form_data.append('file', file_data);
                $.ajax({
                    url: '/backend/user/upload',
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success:function (result) {
                        var data = JSON.parse(result);
                        if(data.code == 1){
                            $('#slide_img_3').attr('src',data.url);
                            $('#slide_url_3').attr('value',data.url);
                        }
                    }
                });
            } else{
                $('.status').text('Chỉ được upload file ảnh');
                $('#file').val('');
            }
            return false;
        });

        $(document).on('change', '#province_id', function (e) {
            e.preventDefault();
            $.ajax({
                url: '/backend/user/district',
                method: 'post',
                data: { id: $(this).val() },
                success:function (result) {
                    var option = "<option selected=\"selected\">--Quận/huyện--</option>";
                    var data = JSON.parse(result);
                    data.forEach(function (item) {
                        option = option + "<option value=\"" + item.district_id + "\">" + item.name +"</option>";
                    });
                    var html_option = $.parseHTML(option);
                    $("#district_id").html(html_option);
                    $('#district_id').selectpicker('refresh');

                }
            })
        });

        $(document).on('change', '#district_id', function (e) {
            e.preventDefault();
            $.ajax({
                url: '/backend/user/ward',
                method: 'post',
                data: { id: $(this).val() },
                success:function (result) {
                    var option = "<option selected=\"selected\">--Quận/huyện--</option>";
                    var data = JSON.parse(result);
                    data.forEach(function (item) {
                        option = option + "<option value=\"" + item.ward_id + "\">" + item.name +"</option>";
                    });
                    var html_option = $.parseHTML(option);
                    $("#ward_id").html(html_option);
                    $('#ward_id').selectpicker('refresh');

                }
            })
        });

        $(document).on('click', '#update_category', function (e) {
            e.preventDefault();
            $.ajax({
                url: '/backend/category/create_form',
                method: 'post',
                data: { id: $(this).attr("category_id") },
            }).fail(function (ui, status) {
                alert('ERROR: ' + status);
            }).done(function (data, status) {
                window.setInterval(function () {
                    $("#modal-update").html(data);
                })
            });
        });

        $(document).on('click', '#update_contact', function (e) {
            e.preventDefault();
            $.ajax({
                url: '/backend/contact/update',
                method: 'post',
                data: { id: $(this).attr('contact') },
                success:function (result) {
                    var res = JSON.parse(result);
                    $("#modal-update").html(res.content);
                    $("#modal-update").modal('show');
                }
            })
        });

        $(document).on('click', '#sendmail', function (e) {
            console.log($(this).attr('contact'));
            e.preventDefault();
            $.ajax({
                url: '/backend/contactCustomer/sendmail',
                method: 'post',
                data: { id: $(this).attr('contact') },
                success:function (result) {
                    var res = JSON.parse(result);
                    $("#modal-sendmail").html(res.content);
                    $("#modal-sendmail").modal('show');
                }
            })
        });

        $("#data_filter").appendTo(".dataTables_wrapper .row:first-child .col-sm-6:first-child");

        {% if (isFilter) %}
        $('#daterange-btn span').html('{{ QueryArr['time_start'] }} đến {{ QueryArr['time_end'] }}');
        {% endif %}

        $('#daterange-btn').daterangepicker(
            {
                opens: "right",
                /*autoApply: true,*/
                locale: {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Chọn",
                    "cancelLabel": "Hủy",
                    "fromLabel": "Từ",
                    "toLabel": "Đến",
                    "customRangeLabel": "Tùy chọn",
                    "weekLabel": "W",
                    "daysOfWeek": [
                        "CN",
                        "T2",
                        "T3",
                        "T4",
                        "T5",
                        "T6",
                        "T7"
                    ],
                    "monthNames": [
                        "Tháng 1",
                        "Tháng 2",
                        "Tháng 3",
                        "Tháng 4",
                        "Tháng 5",
                        "Tháng 6",
                        "Tháng 7",
                        "Tháng 8",
                        "Tháng 9",
                        "Tháng 10",
                        "Tháng 11",
                        "Tháng 12"
                    ],
                    "firstDay": 1
                },
                ranges   : {
                    'Hôm nay'       : [moment(), moment()],
                    'Hôm qua'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '7 ngày trước' : [moment().subtract(6, 'days'), moment()],
                    '30 ngày trước': [moment().subtract(29, 'days'), moment()],
                    'Tháng này'  : [moment().startOf('month'), moment().endOf('month')],
                    'Tháng trước'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                {% if (isFilter) %}
                startDate: "{{ QueryArr['time_start'] }}",
                endDate  : "{{ QueryArr['time_end'] }}",
                {% else %}
                startDate: moment().subtract(1, 'year'),
                endDate  : moment(),
                {% endif %}
            },
            function (start, end) {
                $('#daterange-btn span').html(start.format('DD/MM/YYYY') + ' đến ' + end.format('DD/MM/YYYY'));
            }
        );

        $('#daterange-btn').on('apply.daterangepicker', function(ev, picker) {
            $("#filter_form input#time_start").val(picker.startDate.format('DD/MM/YYYY'));
            $("#filter_form input#time_end").val(picker.endDate.format('DD/MM/YYYY'));
            $('#filter_form').submit();
        });
    });
</script>
</body>
</html>
