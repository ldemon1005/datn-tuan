{% include "/layouts/header.volt" %}
{% include "layouts/sidebar.volt" %}

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="col-sm-8 box-title">Danh sách đơn hàng</h3>
                    <div class="col-sm-4">
                        {{ flash.output() }}
                    </div>
                </div>
                <!-- /.box-header -->

                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="{{ Pagination['current'] > 1 ? '' : 'active' }}"><a href="#activity" data-toggle="tab">Đơn trên website</a></li>
                        <li class="{{ Pagination['current'] > 1 ? 'active' : '' }}"><a href="#timeline" data-toggle="tab">Đơn hàng từ zalo</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="{{ Pagination['current'] > 1 ? '' : 'active' }} tab-pane" id="activity">
                            <div id="sec__customer" class="sec__page">
                                {#<div id="data_filter" class="data_filter">#}
                                    {#<a class="btn btn-block btn-primary btn-sm" href="/backend/order/zalo">Đồng bộ đơn hàn từ zalo</a>#}
                                {#</div>#}
                                <table id="data-table" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Ngày tạo</th>
                                        <th>Mã đơn hàng</th>
                                        <th>Mã đơn VTP</th>
                                        <th>Tổng tiền</th>
                                        <th>Phí v/c</th>
                                        <th>Trạng thái</th>
                                        <th>Thao tác</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {% for item in list_order %}
                                        <tr>
                                            <td class="text-center">{{ item['datecreate'] }}</td>
                                            <td class="text-center"><a href="/backend/order/detail/{{ item['id'] }}">{{ item['order_code'] }}</a></td>
                                            <td class="text-center">{{ item['vtp_code'] }}</td>
                                            <td class="text-center">{{ number_format(item['money']) }}</td>
                                            <td class="text-center">{{ number_format(item['vtp_info']['vtp_fee']) }}</td>
                                            <td class="text-center">
                                                <div class="input-group-btn">
                                                    <button type="button"
                                                            class="btn btn-block btn-sm  dropdown-toggle {{ item['status'] == 1 ? 'btn-warning' : '' }} {{ item['status'] == 2 ? 'btn-primary' : '' }} {{ item['status'] == 3 ? 'btn-success' : '' }}"
                                                            data-toggle="dropdown">{{ item['status_name'] }}</button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="/backend/order/update/{{ item['id'] }}?value=1">Đơn mới</a></li>
                                                        <li><a href="/backend/order/update/{{ item['id'] }}?value=2">Đã thanh toán</a></li>
                                                        <li><a href="/backend/order/update/{{ item['id'] }}?value=3">Hoàn thành</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div class="row form-group">
                                                    <a data-toggle="tooltip" title="Xóa" href="#" class="col-sm-2 text-danger"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    {% endfor %}
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="{{ Pagination['current'] > 1 ? 'active' : '' }} tab-pane" id="timeline">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr class="text-center">
                                    <th>Ngày tạo</th>
                                    <th>Mã đơn hàng</th>
                                    <th>Tổng tiền</th>
                                    <th>Phí v/c</th>
                                    <th>Trạng thái</th>
                                    <th>Thao tác</th>
                                </tr>
                                </thead>
                                <tbody>
                                {% for item in list_order_zalo %}
                                    <tr class="text-center">
                                        <td>{{ item['createdTime'] }}</td>
                                        <td><a href="/backend/order/detail/{{ item['id'] }}">{{ item['orderCode'] }}</a></td>
                                        <td>{{ number_format(item['price']) }}</td>
                                        <td>{{ item['shippingInfo'] ? number_format(item['shippingInfo']['shippingFee']) : '' }}</td>
                                        <td class="text-center">
                                            <div class="input-group-btn">
                                                <button type="button"
                                                        class="btn btn-block btn-sm  dropdown-toggle btn-default"
                                                        data-toggle="dropdown">
                                                    {{ item['status'] == 1 ? 'Đơn mới' : '' }}
                                                    {{ item['status'] == 2 ? 'Đơn đang xử lý' : '' }}
                                                    {{ item['status'] == 3 ? 'Đơn xác nhận' : '' }}
                                                    {{ item['status'] == 4 ? 'Đơn đang được giao' : '' }}
                                                    {{ item['status'] == 5 ? 'Đơn hàng đã thành công' : '' }}
                                                    {{ item['status'] == 6 ? 'Đơn đã bị hủy' : '' }}
                                                    {{ item['status'] == 7 ? 'Đơn giao hàng thất bại' : '' }}
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="/backend/order/update/{{ item['id'] }}?value=1">Đơn mới</a></li>
                                                    <li><a href="/backend/order/update/{{ item['id'] }}?value=2">Đơn đang xử lý</a></li>
                                                    <li><a href="/backend/order/update/{{ item['id'] }}?value=3">Đơn xác nhận</a></li>
                                                    <li><a href="/backend/order/update/{{ item['id'] }}?value=4">Đơn đang được giao</a></li>
                                                    <li><a href="/backend/order/update/{{ item['id'] }}?value=5">Đơn hàng đã thành công</a></li>
                                                    <li><a href="/backend/order/update/{{ item['id'] }}?value=6">Đơn đã bị hủy</a></li>
                                                    <li><a href="/backend/order/update/{{ item['id'] }}?value=7">Đơn giao hàng thất bại</a></li>

                                                </ul>
                                            </div>
                                        </td>
                                        <td class="text-center">
                                            <a data-toggle="tooltip" title="Xóa" href="#" class="text-danger"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>

                                {% endfor %}
                                </tbody>
                            </table>
                            {% include "/layouts/paging.volt" %}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

{% include "/layouts/footer.volt" %}
