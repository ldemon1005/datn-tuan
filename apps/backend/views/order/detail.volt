{% include "/layouts/header.volt" %}
{% include "layouts/sidebar.volt" %}

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><a href="/backend/order" class="btn btn-primary"><i class="fa fa-long-arrow-left"></i></a>Chi tiết đơn hàng {{ order['order_code'] ? order['order_code'] : order['orderCode'] }}</h3>
                </div>
                <!-- /.box-header -->
                <div id="sec__customer" class="sec__page">
                    {% if order['order_code'] != null %}
                        <table class="table">
                            <tr>
                                <th class="xdot">Tên sản phẩm</th>
                                <th class="xdot">Giá bán</th>
                                <th>Số lượng</th>
                                <th class="xdot">Thành tiền</th>
                            </tr>
                            {% for item in list_product %}
                                <tr>
                                    <td>
                                        <div class="xdot code">{{ item['name'] }}</div>
                                    </td>
                                    <td>{{ number_format(item['price']) }}</td>
                                    <td>{{ item['quantity']}}</td>
                                    <td>
                                        {% if item['discount_type'] == 1 %}
                                            <div class="product__price xdot">{{ number_format(item['price']*item['quantity']*(100-item['discount'])/100) }} <span class="unit">đ</span></div>
                                        {% else %}
                                            <div class="product__price xdot">{{ number_format(item['price']*item['quantity'] - item['discount']) }} <span class="unit">đ</span></div>
                                        {% endif %}

                                    </td>
                                </tr>
                            {% endfor %}
                            <tr>
                                <td><b>Phí vận chuyển</b></td>
                                <td></td>
                                <td></td>
                                <td ><b>{{ number_format(order['vtp_info']['vtp_fee']) }}</b></td>
                            </tr>
                            <tr>
                                <td><b>Tổng</b></td>
                                <td></td>
                                <td>{{ total_quantity }}</td>
                                <td ><b>{{ number_format(order['money']) }}</b></td>
                            </tr>
                        </table>
                    {% else %}
                        <table class="table">
                            <tr>
                                <th class="xdot">Tên sản phẩm</th>
                                <th class="xdot">Giá bán</th>
                                <th>Số lượng</th>
                                <th class="xdot">Thành tiền</th>
                            </tr>

                            <tr>
                                <td>
                                    <div class="xdot code">{{ order['productName'] }}</div>
                                </td>
                                <td>{{ number_format(order['price']) }}</td>
                                <td>1</td>
                                <td>{{ number_format(order['price']) }}</td>
                            </tr>

                            <tr>
                                <td><b>Phí vận chuyển</b></td>
                                <td></td>
                                <td></td>
                                <td ><b>{{ number_format(order['shippingInfo']['shippingFee']) }}</b></td>
                            </tr>
                            <tr>
                                <td><b>Tổng</b></td>
                                <td></td>
                                <td>{{ total_quantity }}</td>
                                <td ><b>{{ number_format(order['price'] + order['shippingInfo']['shippingFee']) }}</b></td>
                            </tr>
                        </table>
                    {% endif %}

                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

{% include "/layouts/footer.volt" %}
