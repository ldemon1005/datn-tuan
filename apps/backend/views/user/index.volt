{% include "/layouts/header.volt" %}
{% include "layouts/sidebar.volt" %}

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Danh sách tài người dùng</h3>
                </div>
                <div>
                    {{ flash.output() }}
                </div>
                <!-- /.box-header -->
                <div id="sec__customer" class="sec__page">
                    <div id="data_filter" class="data_filter">
                        <a class="btn btn-block btn-primary btn-sm" href="/backend/user/register">Thêm mới</a>
                    </div>
                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Username</th>
                            <th>Họ tên</th>
                            <th>Số điện thoại</th>
                            <th>Email</th>
                            <th>Ngày sinh</th>
                            <th>Ngày tạo</th>
                            <th>Địa chỉ</th>
                            <th>Trạng thái</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for item in ListUser %}
                            <tr class="{{ item['type'] == 1 ? 'text-danger' : '' }}">
                                <td class="text-center">{{ item['username'] }}</td>
                                <td class="text-center">{{ item['fullname'] }}</td>
                                <td class="text-center">{{ item['phone'] }}</td>
                                <td class="text-center">{{ item['email'] }}</td>
                                <td class="text-center">{{ item['dob'] }}</td>
                                <td class="text-center">{{ item['datecreate'] }}</td>
                                <td class="text-center">{{ item['address'] }}</td>
                                <td class="text-center">
                                    <a href="/backend/user/update/{{ item['id'] }}" class="btn btn-block btn-sm {{ item['status'] == 1 ? 'btn-danger' : 'btn-success' }}">{{ item['status'] == 1 ? 'Không hoạt động' : 'Hoạt động' }}</a>
                                </td>
                                <td class="text-center">
                                    <div class="row form-group">
                                        <a data-toggle="tooltip" title="Xóa" href="/backend/user/delete/{{ item['id'] }}" class="col-sm-2 text-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>

{% include "/layouts/footer.volt" %}
