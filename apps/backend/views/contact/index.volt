{% include "/layouts/header.volt" %}
{% include "layouts/sidebar.volt" %}

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Danh sách cửa hàng</h3>
                </div>
                <!-- /.box-header -->
                <div id="sec__customer" class="sec__page">
                    <div id="data_filter" class="data_filter">
                        <a class="btn btn-block btn-primary btn-sm" data-toggle="modal" data-target="#modal-default">Thêm mới</a>
                    </div>
                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Tên</th>
                            <th>Hotline</th>
                            <th>Số điện thoại</th>
                            <th>Email</th>
                            <th>Địa chỉ</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for item in contact %}
                            <tr>
                                <td class="text-center">{{ item['name'] }}</td>
                                <td class="text-center">{{ item['hotline'] }}</td>
                                <td class="text-center">{{ item['phone'] }}</td>
                                <td class="text-center">{{ item['email'] }}</td>
                                <td class="text-center">{{ item['address'] }}</td>
                                <td class="text-center">
                                    <div class="row form-group">
                                        <a data-toggle="tooltip modal" title="Chỉnh sửa" data-target="#modal-update" href="#" id="update_contact" contact="{{ item['id'] }}" class="col-sm-2 text-primary"><i class="fa fa-wrench"></i></a>
                                        <a data-toggle="tooltip" title="Xóa" href="/backend/contact/delete/{{ item['id'] }}" class="col-sm-2 text-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <div class="modal modal-default fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-primary">Thêm mới liên hệ</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="/backend/contact/create" method="post">
                        {{ flash.output() }}
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-right">Tên liên hệ</label>

                                <div class="col-sm-9">
                                    <input type="text" name="contact[name]" class="form-control" placeholder="tên liên hệ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-right">Hotline</label>
                                <div class="col-sm-9">
                                    <input type="text" name="contact[hotline]" class="form-control" placeholder="hotline">
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-right">Số điện thoại</label>

                                <div class="col-sm-9">
                                    <input type="text" name="contact[phone]" class="form-control" placeholder="số điện thoại">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-right">Địa chỉ</label>

                                <div class="col-sm-9">
                                    <input type="text" name="contact[address]" class="form-control" placeholder="địa chỉ">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-right"></label>

                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <select name="user[city_id]" id="province_id" class="form-control selectpicker" data-live-search="true" style="width: 100%;">
                                                <option selected="selected">--Tỉnh/thành--</option>
                                                {% for item in province %}
                                                    <option value="{{ item.id }}">{{ item.name }}</option>
                                                {% endfor %}
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <select name="user[district_id]" id="district_id" class="form-control selectpicker" data-live-search="true" style="width: 100%;">
                                                <option selected="selected">--Quận/huyện--</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <select name="user[ward_id]" id="ward_id" class="form-control selectpicker" data-live-search="true" style="width: 100%;">
                                                <option selected="selected" class="text-center">--Xã/phường--</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-right">Email</label>

                                <div class="col-sm-9">
                                    <input type="text" name="contact[email]" class="form-control" placeholder="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-right">Icon</label>

                                <div class="col-sm-9">
                                    <input type="text" name="contact[icon]" class="form-control" placeholder="icon">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">Thêm mới</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal modal-default fade" id="modal-update">
    </div>
    <!-- /.content -->
</div>

{% include "/layouts/footer.volt" %}
