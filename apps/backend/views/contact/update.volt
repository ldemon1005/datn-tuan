{% set contact = object['contact'] %}
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title text-primary">Cập nhật liên hệ</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" action="/backend/contact/update/{{ contact.id }}" method="post">
                {{ flash.output() }}
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-right">Tên liên hệ</label>

                        <div class="col-sm-9">
                            <input type="text" value="{{ contact.name }}" name="contact[name]" class="form-control"
                                   placeholder="tên liên hệ">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-right">Hotline</label>
                        <div class="col-sm-9">
                            <input type="text" value="{{ contact.hotline }}" name="contact[hotline]"
                                   class="form-control" placeholder="hotline">
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-right">Số điện thoại</label>

                        <div class="col-sm-9">
                            <input type="text" value="{{ contact.phone }}" name="contact[phone]" class="form-control"
                                   placeholder="số điện thoại">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-right">Địa chỉ</label>

                        <div class="col-sm-9">
                            <input type="text" value="{{ contact.address }}" name="contact[address]"
                                   class="form-control" placeholder="địa chỉ">
                        </div>
                    </div>
                  {#  <div class="form-group">
                        <label class="col-sm-3 control-label text-right"></label>

                        <div class="col-sm-9">
                            <div class="row">
                                <div class="col-sm-4">
                                    <select name="user[city_id]" id="province_id" class="form-control selectpicker" data-live-search="true" style="width: 100%;">
                                        <option>--Tỉnh/thành--</option>
                                        {% for item in province %}
                                            <option {{ item.city_id == contact.city_id ? 'selected' : '' }}
                                                    value="{{ item.city_id }}">{{ item.name }}</option>
                                        {% endfor %}
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="user[district_id]" id="district_id" class="form-control selectpicker" data-live-search="true" style="width: 100%;">
                                        <option selected="selected">--Quận/huyện--</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select name="user[ward_id]" id="ward_id" class="form-control selectpicker" data-live-search="true" style="width: 100%;">
                                        <option selected="selected" class="text-center">--Xã/phường--</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>#}
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-right">Email</label>

                        <div class="col-sm-9">
                            <input type="text" value="{{ contact.email }}" name="contact[email]" class="form-control"
                                   placeholder="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label text-right">Icon</label>

                        <div class="col-sm-9">
                            <input type="text" value="{{ contact.icon }}" name="contact[icon]" class="form-control"
                                   placeholder="icon">
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">Thêm mới</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
