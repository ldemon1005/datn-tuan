<?php
namespace DoGo\Backend\Controllers;
use DoGo\Backend\Models\Product;
use DoGo\Backend\Models\Category;
/**
 * Class IndexController
 * @property \Phalcon\Config $config
 */
class ProductController extends AuthorizedControllerBase
{
    public function indexAction()
    {
        $products = Product::find([
            'conditions' => 'del_flag = :del_flag:',
            'bind' => [
                'del_flag' => Product::NOT_DELETED
            ],
            'order' => 'id desc'
        ]);
        $products = $products->toArray();
        foreach ($products as $key => $val){
            $category = Category::findById($val['category_id']);
            if($category){
                $products[$key]['category'] = [
                    'id' => $category->getId(),
                    'name' => $category->getName(),
                    'parent_id' => $category->getParentId()
                ];
            }
            $products[$key]['properties'] = json_decode($val['properties'],true);
            $products[$key]['datecreate'] = date('d/m/Y',$val['datecreate']);
        }
        $this->view->products = $products;
    }

    public function createAction(){
        $category = Category::find([
            'conditions' => 'type = :type: and del_flag = :del_flag:',
            'bind' => [
                'type' => Category::TYPE_PRODUCT,
                'del_flag' => Category::NOT_DELETED
            ],
            'order' => 'id desc'
        ]);
        $category = $category->toArray();
        $result = [];
        $this->recusiveMenu($category,0,"",$result);
        $this->view->MenuRecusive = $result;

        if($this->request->isPost() && $this->request->getPost('product')){
            $product = $this->request->getPost('product');
            $zalo_check = $product['zalo'];
            $category_zalo = Category::findById($product['category_id']);
            if(!$category_zalo){
                $this->flash->error("Danh mục sản phẩm không tồn tại");
            }
            $product['price'] = filter_var($product['price'],FILTER_SANITIZE_NUMBER_INT);
            $product['discount'] = filter_var($product['discount'],FILTER_SANITIZE_NUMBER_INT);
            $slide_show = $product['slide'];
            $properties = $product['properties'];
            $product = Product::newInstance($product);
            $product->setSlideShow(json_encode($slide_show));
            $product->setProperties(json_encode($properties));
            $product->setDatecreate(time());
            $code = strtoupper(uniqid(Product::PREFIX));
            $product->setCode($code);
            !$product->getDiscount() && $product->setDiscount(0);
            !$product->getDesc() && $product->setDesc('');
            !$product->getQuantity() && $product->setQuantity(0);
            !$product->getWeight() && $product->setWeight(100);
            $tt = 0;
            if($product->create()){
                $tt = 1;
                if($zalo_check == 1){
                    $zalo = new \ZaloService();
                    $product_zalo = $product->toArray();
                    $product_zalo['cate_zalo'] = $category_zalo->getIdZalo();
                    $res_zalo = $zalo->create_product($product_zalo);
                    if($res_zalo['errorCode'] == 1){
                        $product->setIdZalo($res_zalo['data']['productId']);
                        $product->setDelFlag(0);
                        if($product->save() == 1) {
                            $tt = 1;
                        }else $tt = 2;
                    }
                }
            }
            switch ($tt){
                case 0 :
                    $this->flash->error("Tạo mới sản phẩm không thành công");
                    $this->response->redirect(base_uri() . '/backend/product');
                    return;
                case 1 :
                    $this->flash->success("Tạo mới sản phẩm thành công");
                    $this->response->redirect(base_uri() . '/backend/product');
                    return;
                case 3 :
                    $this->flash->success("Đồng bộ sản phẩm với Zalo không thành công");
                    $this->response->redirect(base_uri() . '/backend/product');
                    return;
            }
        }
    }

    public function updateAction($id){
        $category = Category::find([
            'conditions' => 'type = :type: and del_flag = :del_flag:',
            'bind' => [
                'type' => Category::TYPE_PRODUCT,
                'del_flag' => Category::NOT_DELETED
            ],
            'order' => 'id desc'
        ]);
        $category = $category->toArray();
        $result = [];
        $this->recusiveMenu($category,0,"",$result);
        $this->view->MenuRecusive = $result;

        $product = Product::findById($id);
        if(!$product){
            $this->flash->error('Không tìm thấy sản phẩm');
            $this->response->redirect(base_uri().'/backend/product');
        }
        $product_view = $product->toArray();



        $product_view['slide_show'] = json_decode($product_view['slide_show'],true);
        $product_view['properties'] = json_decode($product_view['properties'],true);

        $this->view->product = $product_view;

        if($this->request->isPost() && $this->request->getPost('product')){
            $product_data = $this->request->getPost('product');
            $slide_show = $product_data['slide'];
            $properties = $product_data['properties'];
            $product_data['price'] = filter_var($product_data['price'],FILTER_SANITIZE_NUMBER_INT);
            $product_data['discount'] = filter_var($product_data['discount'],FILTER_SANITIZE_NUMBER_INT);
            $product->setName($product_data['name']);
            $product->setCategoryId($product_data['category_id']);
            $product_data['price'] ? $product->setPrice($product_data['price']) : $product->setPrice(0);
            $product->setTypeDiscount($product_data['type_discount']);
            $product_data['discount'] ? $product->setDiscount($product_data['discount']) : $product->setDiscount(0);
            $product_data['weight'] ? $product->setWeight($product_data['weight']) : $product->setWeight(100);
            $product_data['avatar'] && $product->setAvatar($product_data['avatar']);
            $product->setStatus($product_data['status']);
            $product->setDesc($product_data['desc']);
            $product_data['quantity'] ? $product->setQuantity($product_data['quantity']) : $product->setQuantity(0);
            $product->setSlideShow(json_encode($slide_show));
            $product->setProperties(json_encode($properties));
            if($product->save()){
                if($product->getIdZalo()){
                    $zalo = new \ZaloService();
                    $res_zalo = $zalo->update_product($product->toArray());
                    if($res_zalo['errorCode'] != 1){
                        $this->flash->error("Đồng bộ cập nhật với zalo không thành công");
                    }
                }
                $this->flash->success('Cập nhật thành công');
                $this->response->redirect(base_uri().'/backend/product');
            }else $this->flash->error('Cập nhật không thành công');
        }
    }

    public function deleteAction($id){
        $product = Product::findById($id);
        if($product->delete()){
            $zalo = new \ZaloService();
            $zalo->delete_product($product->toArray());
            $this->flash->success('Xóa sản phẩm thành công');
        }else{
            $this->flash->error('Xóa sản phẩm không thành công');
        }
        $this->response->redirect(base_uri().'/backend/product');
    }

    public static function recusiveMenu($data,$parent_id = 0,$text = "",&$result){
        foreach ($data as $key => $item){
            if($item['parent_id'] == $parent_id){
                $item['name'] = $text.$item['name'];
                $result [] = $item;
                unset($data[$key]);
                self::recusiveMenu($data,$item['id'],$text."--",$result);
            }
        }
    }
}

