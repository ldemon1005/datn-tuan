<?php
namespace DoGo\Backend\Controllers;
/**
 * Class AuthController
 * @property \Phalcon\Config $config
 */
class AuthorizedControllerBase extends ControllerBase
{
    use \AuthExt;

    public function initialize()
    {
        parent::initialize();


        if (!$this->isLogin()) {
            $this->response->redirect(base_uri() . '/backend/auth/login');
        }
	    $user_info = $this->getLoggedUserInfo();
        if($user_info['type'] == 0) {
            $this->response->redirect(base_uri() . '/backend/auth/login');
            $this->session->destroy( true );
        }
        $this->view->auth = $user_info;
    }
}

