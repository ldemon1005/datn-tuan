<?php
namespace DoGo\Backend\Controllers;
use DoGo\Backend\Models\Contact;
use DoGo\Backend\Models\Districts;
use DoGo\Backend\Models\Location;
use DoGo\Backend\Models\Province;

/**
 * Class IndexController
 * @property \Phalcon\Config $config
 */
class ContactController extends AuthorizedControllerBase
{
    public function indexAction()
    {
        $province = Province::find();
        $this->view->province = $province;
        $contact = Contact::find([
            'order' => 'id desc'
        ]);
        $contact = $contact->toArray();
        $this->view->contact = $contact;
    }

    public function createAction(){
        if($this->request->isPost() && $this->request->getPost('contact')){
            $contact = $this->request->getPost('contact');
            $contact = Contact::newInstance($contact);
            if($contact->create()){
                $this->flash->success('Thêm mới liên hệ thành công');
                $this->response->redirect(base_uri().'/backend/contact');
            }else $this->flash->error('Thêm mới liên hệ không thành công');
        }
    }

    function updateAction($id=null){
        $contact_id = $this->request->getPost('id');
        $contact = Contact::findById($contact_id);
        if(!$contact){
            $this->flash->error('Liên hệ không tồn tại');
            $this->response->redirect(base_uri().'/backend/contact');
        }
        if($this->request->isPost() && $this->request->getPost('contact')){
            $contact_data = $this->request->getPost('contact');
            $contact = Contact::findById($id);
            $contact->setName($contact_data['name']);
            $contact->setAddress($contact_data['address']);
            $contact->setHotline($contact_data['hotline']);
            $contact->setEmail($contact_data['email']);
            $contact->setPhone($contact_data['phone']);
            if($contact->save()){
                $this->flash->success('Cập nhật liên hệ thành công');
                $this->response->redirect(base_uri().'/backend/contact');
            }else $this->flash->error('Cập nhật liên hệ không thành công');
        }else {
            $template = $this->render_template('contact','update',['contact' =>$contact]);
            return json_encode([
                'content' => $template
            ]);
        }
    }

    function deleteAction($id){
        $contact = Contact::findById($id);
        if(!$contact){
            $this->flash->error('Xóa liên hệ không thành công');
        }else {
            $contact->delete();
        }
        $this->response->redirect(base_uri().'/backend/contact');
    }


}

