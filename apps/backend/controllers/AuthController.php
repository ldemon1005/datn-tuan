<?php
namespace DoGo\Backend\Controllers;

use AuthenticationService;
/**
 * @property \Phalcon\Config $config
 */
class AuthController extends ControllerBase
{
    use \AuthExt;

    public function initialize() {
        parent::initialize();
    }

    public function indexAction()
    {

    }

    /**
     * Đăng nhập
     */
    function loginAction() {
        if ( $this->isLogin() ) {
            return $this->response->redirect( base_uri() . '/backend/index' );
        }
        if ( $this->request->isPost() ) {
            $user = $this->request->getPost( 'username' );
            $password = $this->request->getPost( 'password' );
            $authService = new AuthenticationService();
            if ( $user = $authService->login( $user, $password ) ) {
                $this->saveUserInfoToSession( $user );
                $this->response->redirect( base_uri() . '/backend/index' );
            }
        }
    }

    /**
     * Đăng xuất
     */
    function logoutAction() {
        $this->session->destroy( true );
        $this->response->redirect( base_uri() . '/backend/auth/login' );
    }


}

