<?php
namespace DoGo\Backend\Controllers;
/**
 * Class IndexController
 * @property \Phalcon\Config $config
 */
class IndexController extends AuthorizedControllerBase
{
    use \MailService;
    public function indexAction()
    {

    }

    function uploadAction(){
        if(isset($_POST) && isset($_FILES['file'])){
            $funcNum = $_GET['CKEditorFuncNum'] ;
            $duoi = explode('.', $_FILES['file']['name']);
            $duoi = strtolower($duoi[(count($duoi)-1)]);
            if($duoi == 'jpg' || $duoi == 'png' || $duoi == 'gif'){
                if(move_uploaded_file($_FILES['file']['tmp_name'], BASE_PATH . '/../public/uploads/' . $_FILES['file']['name'])){
                    $url = '/../../uploads/' . $_FILES['file']['name'];
                    return "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', 'Upload file success');";
                } else{

                    return json_encode([
                        'code' => 0,
                    ]);
                }
            } else{
                die('Chỉ được upload ảnh');
            }
        }
    }
}

