<?php
namespace DoGo\Backend\Controllers;
use DoGo\Backend\Models\Articel;

/**
 * Class IndexController
 * @property \Phalcon\Config $config
 */
class ArticelController extends AuthorizedControllerBase
{

    public function indexAction()
    {
        $articel = Articel::find([
            'conditions' => 'del_flag = :del_flag:',
            'bind' => [
                'del_flag' => Articel::NOT_DELETED
            ],
            'order' => 'id desc'
        ]);
        if($articel) $articel = $articel->toArray();
        else $articel = [];
        foreach ($articel as $key => $value){
            $articel[$key]['datecreate'] = date('d/m/Y H:i',$value['datecreate']);
        }
        $this->view->ListArticel = $articel;
    }
    function create_articelAction($id=null){
        if($id != null && !$this->request->isPost()){
            $articel = Articel::findById($id);
            if($articel) $articel = $articel->toArray();
            else $articel = [];
            $this->view->Articel = $articel;
        }

        if($this->request->getPost('articel')){
            $articel = $this->request->getPost('articel');
            $content = $this->request->getPost('editor1');
            $articel['content'] = $content;
            if($id == null){
                $articel = Articel::newInstance($articel);
                $articel->setDatecreate(time());
                if($articel->save()){
                    $this->flash->success("Tạo mới bài viết thành công");
                    $this->response->redirect(base_uri().'/backend/articel');
                }else $this->flash->error("Tạo mới bài viết không thành công");
            }else {
                $articel_update = Articel::findById($id);
                if(!$articel_update){
                    $this->flash->error("Không tìm thấy bài viết");
                }else {
                    $articel_update->setName($articel['name']);
                    $articel_update->setAvtar($articel['avatar']);
                    $articel_update->setCategoryId($articel['category_id']);
                    $articel_update->setStatus($articel['status']);
                    $articel_update->setContent($articel['content']);
                    if($articel_update->save()){
                        $this->flash->success("Cập nhật bài viết thành công");
                        $this->response->redirect(base_uri().'/backend/articel');
                    }else $this->flash->error("Cập nhật bài viết không thành công");
                }
            }
        }
    }

    function deleteAction($id = null){
        $articel = Articel::findById($id);
        if(!$articel) $this->flash->error("Không tìm thấy bài viết");
        else {
            if($articel->delete()){
                $this->flash->success("Xóa thành công");
            }else $this->flash->error("Xóa không thành công");
        }
        $this->response->redirect(base_uri().'/backend/articel');
    }

    public static function recusiveMenu($data,$parent_id = 0,$text = "",&$result){
        foreach ($data as $key => $item){
            if($item['parent_id'] == $parent_id){
                $item['name'] = $text.$item['name'];
                $result [] = $item;
                unset($data[$key]);
                self::recusiveMenu($data,$item['id'],$text."--",$result);
            }
        }
    }

}

