<?php
namespace DoGo\Backend\Controllers;

use DoGo\Backend\Models\User;
use DoGo\Backend\Models\Location;
use DoGo\Backend\Models\Districts;
use DoGo\Backend\Models\Province;
use DoGo\Backend\Models\Wards;

/**
 * Class AuthController
 * @property \Phalcon\Config $config
 */
class UserController extends AuthorizedControllerBase
{

    public function indexAction()
    {
        $users = User::find([
            'order' => 'id desc'
        ]);
        if(!$users){
            $users = [];
        }
        $users = $users->toArray();
        foreach ($users as $key => $user){
            $users[$key]['dob'] = date("d/m/Y",$user['dob']);
            $users[$key]['datecreate'] = date("d/m/Y",$user['datecreate']);

            if($user['ward_id']){
                $address = $this->getWardVtp($user['ward_id']);
                $address = $address['fullname'];
            }else if($user['district_id']){
                $address = $this->getDistrictVtp($user['district_id']);
                $address = $address['fullname'];
            }else if($user['city_id']){
                $address = $this->getProvinceVtp($user['city_id']);
                $address = $address['name'];
            }else {
                $address = '';
            }
            $users[$key]['address'] = $address;

        }
        $this->view->ListUser = $users;
    }

    function getListUser(){

    }

    /**
     * Đăng ký
     */
    function registerAction() {
        $province = Province::find();
        $this->view->province = $province;
        if($this->request->isPost()){
            $user = $this->request->getPost('user');
            $user = User::newInstance($user);
            $check = 0;
            $check = $this->validateCreate($user);

            $check_email_phone = User::findFirst([
                'conditions' => 'email = :email: or phone = :phone:',
                'bind' => [
                    'email' => $user->getEmail(),
                    'phone' => $user->getPhone()
                ]
            ]);
            if($check_email_phone){
                $check = 1;
                $this->flash->error( 'Email hoặc số điện thoại đã có người sử dụng!' );
            }
            $user->setDatecreate(time());
            $user->setDob(strtotime(str_replace("/", "-", $user->getDob())));
            if($check == 0){
                $user->create();
                $this->response->redirect(base_uri().'/backend/user');
            }
        }
    }

    /**
     * Chỉnh sửa
     */
    function updateAction($id = null) {
        if($id){
            $user = User::findById($id);
            if($user){
                $user->getStatus() == 1 ? $user->setStatus(2) : $user->setStatus(1);
                if($user->save()) {
                    $this->flash->success("Cập nhật thành công");
                }else $this->flash->error("Cập nhật không thành công");
            }else $this->flash->error("Không tìm thấy khách hàng");
        }
        $this->response->redirect(base_uri().'/backend/user');
    }

    function deleteAction($id = null){
        if($id){
            $user = User::findById($id);
            if($user){
                if($user->delete()) $this->flash->success("Xóa thành công");
                else $this->flash->error("Xóa không thành công");
            }else $this->flash->error("Không tìm thấy khách hàng");
        }
        $this->response->redirect(base_uri().'/backend/user');
    }

    /**
     * Lấy sản danh sách khu vực
     */
    function districtAction()
    {
        $id = $this->request->getPost('id');
        $district = Districts::find([
            'conditions' => 'city_id = :id:',
            'bind' => [
                'id' => $id
            ]
        ]);
       return json_encode($district->toArray());
    }

    /**
     * Lấy sản danh sách khu vực
     */
    function wardAction()
    {
        $id = $this->request->getPost('id');
        $ward = Wards::find([
            'conditions' => 'district_id = :id:',
            'bind' => [
                'id' => $id
            ]
        ]);
        return json_encode($ward);
    }
    function uploadAction(){
        if(isset($_POST) && isset($_FILES['file'])){
            $duoi = explode('.', $_FILES['file']['name']);
            $duoi = strtolower($duoi[(count($duoi)-1)]);
            if($duoi == 'jpg' || $duoi == 'png' || $duoi == 'gif'){
                $maxDim = 500;
                list($width, $height, $type, $attr) = getimagesize( $_FILES['myFile']['tmp_name'] );
                if ( $width > $maxDim || $height > $maxDim ) {
                    $target_filename = $_FILES['myFile']['tmp_name'];
                    $fn = $_FILES['myFile']['tmp_name'];
                    $size = getimagesize( $fn );
                    $ratio = $size[0]/$size[1]; // width/height
                    if( $ratio > 1) {
                        $width = $maxDim;
                        $height = $maxDim/$ratio;
                    } else {
                        $width = $maxDim*$ratio;
                        $height = $maxDim;
                    }
                    $src = imagecreatefromstring( file_get_contents( $fn ) );
                    $dst = imagecreatetruecolor( $width, $height );
                    imagecopyresampled( $dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1] );
                    imagedestroy( $src );
                    imagepng( $dst, $target_filename ); // adjust format as needed
                    imagedestroy( $dst );
                }
                if(move_uploaded_file($_FILES['file']['tmp_name'], BASE_PATH . '/../public/uploads/' . $_FILES['file']['name'])){
                    return json_encode([
                        'code' => 1,
                        'url' => '/../../uploads/' . $_FILES['file']['name']
                    ]);
                } else{
                    return json_encode([
                        'code' => 0,
                    ]);
                }
            } else{
                die('Chỉ được upload ảnh');
            }
        }
    }

    function validateCreate($user){
        $check = 0;
        if(!$user->getEmail() && $check == 0){
            $check = 1;
            $this->flash->error( 'Thiếu email!' );
        }
        if(!$user->getUsername() && $check == 0){
            $check = 1;
            $this->flash->error( 'Thiếu username!' );
        }
        if(!$user->getPassword() && $check == 0){
            $check = 1;
            $this->flash->error( 'Thiếu mật khẩu!' );
        }
        if(!$user->getPhone() && $check == 0){
            $check = 1;
            $this->flash->error( 'Thiếu số điện thoại!' );
        }
        if(!$user->getFullname() && $check == 0){
            $check = 1;
            $this->flash->error( 'Thiếu họ và tên!' );
        }
        if((!$user->getDistrictId() || !$user->getCityId() || !$user->getWardId()) && $check == 0){
            $check = 1;
            $this->flash->error( 'Thiếu thông tin địa chỉ!' );
        }

        return $check;
    }

    function resize_image_max($image,$max_width,$max_height) {
        $w = imagesx($image); //current width
        $h = imagesy($image); //current height
        if ((!$w) || (!$h)) { $GLOBALS['errors'][] = 'Image couldn\'t be resized because it wasn\'t a valid image.'; return false; }

        if (($w <= $max_width) && ($h <= $max_height)) { return $image; } //no resizing needed

        //try max width first...
        $ratio = $max_width / $w;
        $new_w = $max_width;
        $new_h = $h * $ratio;

        //if that didn't work
        if ($new_h > $max_height) {
            $ratio = $max_height / $h;
            $new_h = $max_height;
            $new_w = $w * $ratio;
        }
        $new_image = imagecreatetruecolor ($new_w, $new_h);
        imagecopyresampled($new_image,$image, 0, 0, 0, 0, $new_w, $new_h, $w, $h);
        return $new_image;
    }
}

