<?php
namespace DoGo\Backend\Controllers;

use DoGo\Backend\Models\Category;
use ZaloService;
/**
 * @property \Phalcon\Config $config
 */
class CategoryController extends AuthorizedControllerBase
{

    public function indexAction()
    {
        if($this->request->isPost() && $this->request->getPost('category')){
            $category = $this->request->getPost('category');
            $zalo_check = $category['zalo'];
            $category = Category::newInstance($category);
            $category->setDatecreate(time());
            $category->setType(Category::TYPE_PRODUCT);
            !$category->getOrder() && $category->setOrder(0);
            if ($category->getName() && $category->save()) {
                if($zalo_check){
                    $zalo = new ZaloService();
                    $res = $zalo->create_category($category->toArray());
                    if($res['errorCode'] != 1){
                        $this->flash->error("Tạo mới danh mục zalo không thành công");
                    }else {
                        $category->setIdZalo($res['data']['categoryId']);
                        $category->setDelFlag(0);
                        $category->save();
                    }
                }
            } else {
                $this->flash->error("Thêm mới danh mục không thành công");
            }

        }
        $category = Category::find([
            'conditions' => 'type = :type: and del_flag = :del_flag:',
            'bind' => [
                'type' => Category::TYPE_PRODUCT,
                'del_flag' => Category::NOT_DELETED
            ],
            'order' => 'id desc'
        ]);
        $category = $category->toArray();
        foreach ($category as $key => $value){
            $category[$key]['datecreate'] = date('d/m/Y',$value['datecreate']);
        }
        $this->view->ListCategory = $category;
        $result = [];
        $result[] = [
            'id' => 0,
            'name' => 'root'
        ];
        $this->recusiveMenu($category,0,"",$result);
        foreach ($result as $key=>$value){
            if($value['parent_id'] != 0) $result[$key]['name'] = "|".$result[$key]['name'];
        }
        $this->view->MenuRecusive = $result;
    }
    public static function recusiveMenu($data,$parent_id = 0,$text = "",&$result){
        foreach ($data as $key => $item){
            if($item['parent_id'] == $parent_id){
                $item['name'] = $text.$item['name'];
                $result [] = $item;
                unset($data[$key]);
                self::recusiveMenu($data,$item['id'],$text."--",$result);
            }
        }
    }

    function updateAction($id){
        $category = Category::find([
            'condition' => 'type = :type:',
            'bind' => [
                'type' => Category::TYPE_PRODUCT
            ]
        ]);
        $category = $category->toArray();
        $result = [];
        $result[0] = [
            'id' => 0,
            'name' => 'root'
        ];
        $this->recusiveMenu($category,0,"",$result);
        foreach ($result as $key=>$value){
            if($value['parent_id'] != 0) $result[$key]['name'] = "|".$result[$key]['name'];
        }
        $this->view->MenuRecusive = $result;

        $category = Category::findById($id);
        if(!$category){
            $this->flash->error("Danh mục không tồn tại");
        }
        $this->view->detail_category = $category;
        if($this->request->isPost() && $this->request->getPost('category')){
            $category_data = $this->request->getPost('category');
            $category->setStatus($category_data['status']);
            $category->setName($category_data['name']);
            $category->setParentId($category_data['parent_id']);
            $category->setDesc($category_data['desc']);
            if ($category->save() && $category->getName()) {
                if($category->getIdZalo()){
                    $zalo = new ZaloService();
                    $res = $zalo->update_category($category->toArray());
                    d($res);
                    if($res['errorCode'] != 1){
                        $this->flash->error("Cập nhật danh mục zalo không thành công");
                    }
                }
                $this->flash->success("Cập nhật danh mục thành công");
                $this->response->redirect(base_uri().'/backend/category');
            } else {
                $this->flash->error("Cập nhật danh mục không thành công");
                $this->response->redirect(base_uri().'/backend/category/update');
            }
        }
    }



    function deleteAction($id){
        $category = Category::find();
        $category = $category->toArray();
        $result = [];
        $this->recusiveDelete($id,$category,$result);
        $t = 0;
        foreach ($result as $value){
            $category_del = Category::findById($value['id']);
            if($category_del->delete()) $t = 1;
        }
        $category_del_1 = Category::findById($id);
        if($category_del_1->delete()) $t = 1;
        if($t == 0){
            $this->flash->error("Xóa không thành công");
        }else {
            $this->flash->success("Xóa thành công");
        }
        $this->response->redirect(base_uri().'/backend/category');
    }
    function recusiveDelete($id,$menu,&$result){
        foreach ($menu as $key => $val){
            if($val['parent_id'] == $id){
                $result [] = $val;
                unset($menu[$key]);
                self::recusiveMenu($val['id'],$menu,$result);
            }
        }
    }

    function create_formAction(){
        $id = $this->request->getPost('id');
        $category = Category::findById($id);
        if($category){
            $data = $this->render_template("category",'create_form',$category);
            return $data;
        }
    }
}

