<?php
namespace DoGo\Backend\Controllers;
use DoGo\Backend\Models\Contact;
use DoGo\Backend\Models\Location;
use DoGo\Backend\Models\ContactCustomer;

/**
 * Class IndexController
 * @property \Phalcon\Config $config
 */
class ContactCustomerController extends AuthorizedControllerBase
{
    use \MailService;
    public function indexAction()
    {
        $contacts = ContactCustomer::find([
            'conditions' => 'del_flag = :del_flag:',
            'bind' => [
                'del_flag' => ContactCustomer::NOT_DELETED
            ]
        ]);
        if($contacts) $contacts = $contacts->toArray();
        else $contacts = [];
        $this->view->list_contact = $contacts;
    }

    public function emailAction(){

    }

    public function sendmailAction($id=null){
        $contact_id = $this->request->getPost('id');
        $contact = ContactCustomer::findById($contact_id);
        if($contact) $contact = $contact->toArray();
        else {
            $this->flash->error("Không tìm thấy liên hệ");
            $this->response->redirect(base_uri()."/backend/contactCustmer");
        }
        if($this->request->getPost('email')){
            $email = $this->request->getPost('email');
            $this->sendMail($email['email'],$email['content']);
            $contact = ContactCustomer::findById($id);
            $contact->setStatus(2);
            $contact->save();
            $this->response->redirect(base_uri()."/backend/contactCustomer");
        }else{
            $template = $this->render_template('contactCustomer','sendmail',['contact' => $contact]);
            return json_encode([
                'content' => $template
            ]);
        }
    }
    public function deleteAction($id = null){
        if($id){
            $contact = ContactCustomer::findById($id);
            if($contact) {
                if($contact->delete()) $this->flash->success("Xóa thành công");
                else $this->flash->error("Xóa không thành công");
            }
            else $this->flash->error("Xóa không thành công");
        }
        $this->response->redirect(base_uri()."/backend/contactcustomer");
    }
}

