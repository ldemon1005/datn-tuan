<?php
namespace DoGo\Backend\Controllers;

use DoGo\Backend\Models\Order;
use DoGo\Backend\Models\Province;
use DoGo\Backend\Models\User;
use Phalcon\Cache\Backend;
use Phalcon\Mvc\Controller;
use DoGo\Backend\Models\Wards;
use DoGo\Backend\Models\Districts;

use Viettelpost;

/**
 * Class ControllerBase
 * @property Backend $cache
 */
class ControllerBase extends Controller
{
    use \AuthExt;
    const STATUS_SUCCESS = 1;
    const STATUS_ERROR = 0;
    const STATUS_WARNING = 2;
    public $helper;

    public function initialize() {
        $province = Province::find();
        $this->view->province = $province;
        $this->view->resourcePath = base_uri();
        if($this->isLogin()){
            $this->view->auth = $this->getLoggedUserInfo();
            $this->getTotalOrder();
            $this->getTotalUser();
        }
    }

    public function setAuth($auth_token)
    {
        $this->session->set("auth", $auth_token);
    }

    /***
     * @param Header $header
     */
    public function setHeader(Header $header)
    {
        $this->view->header = $header;
    }

    public function render_template($controller, $action, $data = null)
    {
        $view = $this->view;
        $content = $view->getRender($controller, $action, ["object" => $data], function ($view) {
            $view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_LAYOUT);
        });
        return $content;
    }

    public function getProvinceVtp($id){
        $city = Province::findFirst([
            'conditions' => 'city_id = :city_id:',
            'bind' => [
                'city_id' => $id
            ]
        ]);
        if($city) return $city->toArray();
        else return null;
    }

    public function getDistrictVtp($id){
        $district = Districts::findFirst([
            'conditions' => 'district_id = :district_id:',
            'bind' => [
                'district_id' => $id
            ]
        ]);
        if($district) return $district->toArray();
        else return null;
    }

    public function getWardVtp($id){
        $ward = Wards::findFirst([
            'conditions' => 'ward_id = :ward_id:',
            'bind' => [
                'ward_id' => $id
            ]
        ]);
        if($ward) return $ward->toArray();
        else return null;
    }

    public function getTotalUser(){
        $count = User::count([
            'del_flag' => User::NOT_DELETED
        ]);
        $this->view->total_user = $count;
    }
    public function getTotalOrder(){
        $count = Order::count([
            'del_flag' => User::NOT_DELETED
        ]);
        $this->view->total_order = $count;
    }
}
