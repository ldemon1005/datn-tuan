<?php
namespace DoGo\Backend\Controllers;
use DoGo\Backend\Models\Order;
use DoGo\Backend\Models\OrderProduct;
use DoGo\Backend\Models\Product;
use ZaloService;

/**
 * Class OrderController
 * @property \Phalcon\Config $config
 */
class OrderController extends AuthorizedControllerBase
{

    public function indexAction()
    {
        $order = Order::find([
            'conditions' => 'del_flag = :del_flag:',
            'bind' => [
                'del_flag' => Order::NOT_DELETED
            ],
            'order' => 'id desc'
        ]);
        if($order) {
            $order = $order->toArray();
            foreach ($order as $key => $val){
                $order[$key]['vtp_info'] = json_decode($val['vtp_info'],true);
                $order[$key]['datecreate'] = date("d/m/Y H:i:s",$val['datecreate']);
                switch ($val['status']){
                    case 1: $order[$key]['status_name'] = "Đơn mới";break;
                    case 2: $order[$key]['status_name'] = "Đã thanh toán";break;
                    case 3: $order[$key]['status_name'] = "Hoàn thành";break;

                    default:
                        $order[$key]['status_name'] = "Lỗi";break;
                }
            }
        }
        else $order = [];
        $this->view->list_order = $order;

        //Order zalo

        $page = $this->request->getQuery('p','int',1);

        $pagination = [
            'total' => 0,
            'total_pages' => 1,
            'current' => 1,
            'current_link' => base_uri()."/backend/order?"
        ];
        $zalo = new ZaloService();
        $order_zalo = $zalo->get_order(($page-1)*10);
        if($order_zalo['errorCode'] == 1){
            $pagination = [
                'total' => $order_zalo['data']['total'],
                'total_pages' => ceil($order_zalo['data']['total']/10),
                'current' => $page,
                'current_link' => base_uri()."/backend/order?"
            ];
        }
        if($order_zalo['data']['orders']){
            foreach ($order_zalo['data']['orders'] as $key => $val){
                $order_zalo['data']['orders'][$key]['createdTime'] = date("d/m/Y H:i:s",$val['createdTime']/1000);
            }
        }
        $this->view->list_order_zalo = $order_zalo['data']['orders'] ? $order_zalo['data']['orders'] : [];
        $this->view->Pagination = $pagination;
    }

    function updateAction($id = null){
        if($id){
            $val = $this->request->get('value');
            $order = Order::findById($id);
            if($order){
                $order->setStatus($val);
                if($order->save()) $this->flash->success('Cập nhật trạng thái thành công');
                else $this->flash->error("Cập nhật trạng thái không thành công");
            }else {
                $zalo = new ZaloService();
                $order = $zalo->getOrderById($id);
                if($order['errorCode'] == 1){
                    $status = $zalo->update_order($id,$val);
                    if($status['errorCode'] == 1) $this->flash->success('Cập nhật trạng thái thành công');
                    else $this->flash->error("Cập nhật trạng thái không thành công");
                }else $this->flash->error("Cập nhật không thành công");
            }
        }
        $this->response->redirect(base_uri().'/backend/order');
    }

    function deleteAction($id = null){
        if($id){
            $order = Order::findById($id);
            if($order){
                if($order->delete())$this->flash->success("Xóa đơn hàng thành công");
                else $this->flash->error("Xóa đơn hàng không thành công");
            }else $this->flash->error("Không tìm thấy đơn hàng");
        }
        $this->response->redirect(base_uri().'/backend/order');
    }

    function zaloAction(){
        $zalo = new ZaloService();
        $order_zalo = $zalo->get_order();
        d(json_encode($order_zalo));
        if($order_zalo['errorCode'] == 1){
            $order_zalo = $order_zalo['data']['orders'];
            $t=0;
            foreach ($order_zalo as $value){
                $vtp_info = [
                    'vtp_fee' => $value['shippingInfo']['shippingFee']
                ];
                $order_code = strtoupper(uniqid(Order::PREFIX));
                $data = [
                    'id_zalo' => $value['id'],
                    'datecreate' => intval($value['createdTime']/1000),
                    'money' => $value['price'],
                    'vtp_info' => json_encode($vtp_info),
                    'order_code' => $order_code,
                    'status' => $value['status']
                ];
                $order = Order::findFirst([
                    'conditions' => 'id_zalo=:id_zalo:',
                    'bind' => [
                        'id_zalo' => $value['id']
                    ]
                ]);
                if(!$order){
                    $order = Order::newInstance($data);
                }
                if($order->save()){
                    $t++;
                }
                $this->flash->success("Đồng bộ thành công $t đơn hàng");
            }
        }
        $this->response->redirect(base_uri()."/backend/order");
    }

    function detailAction($id = null){
        if($id != null){
            $order = Order::findById($id);
            if($order){
                $order = $order->toArray();
                $order['vtp_info'] = json_decode($order['vtp_info'],true);
                $products = OrderProduct::find([
                    'conditions' => 'order_id = :order_id:',
                    'bind' => [
                        'order_id' => $id
                    ]
                ]);
                if($products){
                    $products = $products->toArray();
                    $total_quantity = array_sum(array_column($products,'quantity'));
                    $product_id = array_column($products,'product_id');
                    if(count($product_id)){
                        $list_product = Product::find([
                            'conditions' => 'id in ({id:array})',
                            'bind' => [
                                'id' => $product_id
                            ]
                        ]);
                        if($list_product){
                            $list_product = $list_product->toArray();
                            foreach ($list_product as $key=>$val){
                                foreach ($products as $value){
                                    if($value['product_id'] == $val['id']){
                                        $list_product[$key]['quantity'] = $value['quantity'];
                                    }
                                }
                            }
                        }else $list_product = [];

                    }else $list_product = [];

                    $this->view->setVars([
                        'list_product' => $list_product,
                        'order' => $order,
                        'total_quantity' => $total_quantity
                    ]);
                }
            } else {
                $zalo = new ZaloService();
                $order = $zalo->getOrderById($id);
                if($order ['errorCode'] == 1){
                    $this->view->order = $order['data'];
                }
            }
        }
    }


}

